﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleTrigger : MonoBehaviour {

	// Author: Glenn Storm
	// This triggers battle wave start on the battle manager in the scene

	public		int 			waveIndex;
	public		float			startDelay;

	private		float			startTimer;
	private		BattleManager	mgr;


	void Start () {
		// validate
		if (waveIndex < 0) {
			Debug.LogError ("--- BattleTrigger [Start] : "+gameObject.name+" wave index "+waveIndex+" invalid. Aborting.");
			enabled = false;
		}
		mgr = GameObject.FindObjectOfType<BattleManager>() as BattleManager;
		if (mgr == null) {
			Debug.LogError ("--- BattleTrigger [Start] : "+gameObject.name+" no battle manager found in scene. Aborting.");
			enabled = false;
		}
		// initialize
		startTimer = startDelay;
	}

	void Update () {
		startTimer -= Time.deltaTime;
		if (startTimer <= 0f) {
			startTimer = 0f;
			if (!mgr.StartWaves (waveIndex))
				Debug.LogError ("--- BattleTrigger [Update] : "+gameObject.name+" wave index "+waveIndex+" invalid. Wave start failed.");
			enabled = false;
		}
	}
}
