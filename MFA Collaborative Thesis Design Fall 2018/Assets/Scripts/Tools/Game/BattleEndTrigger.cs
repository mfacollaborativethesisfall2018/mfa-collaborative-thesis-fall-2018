﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleEndTrigger : MonoBehaviour {

	// Author: Glenn Storm
	// This triggers end of battle (end of level) on the battle manager in the scene

	public		float			endDelay;

	private		float			endTimer;
	private		BattleManager	mgr;


	void Start () {
		// validate
		mgr = GameObject.FindObjectOfType<BattleManager>() as BattleManager;
		if (mgr == null) {
			Debug.LogError ("--- BattleEndTrigger [Start] : "+gameObject.name+" no battle manager found in scene. Aborting.");
			enabled = false;
		}
		// initialize
		endTimer = endDelay;
	}

	void Update () {
		endTimer -= Time.deltaTime;
		if (endTimer <= 0f) {
			endTimer = 0f;
			if (!mgr.PlayerWinsLevel())
				Debug.LogError ("--- BattleEndTrigger [Update] : "+gameObject.name+" unable to trigger level end. Aborting.");
			enabled = false;
		}
	}
}
