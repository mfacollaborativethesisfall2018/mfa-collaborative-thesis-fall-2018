﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveManager : MonoBehaviour {

	// Author: Glenn Storm
	// This manager handles saving and loading game data to a binary file.

	public	GameData		game;
	public	string 			dataFileName = "MonkeyJourney.dat";


	void Start () {
		
	}

	void OnEnable() {

		// auto load
		if ( LoadGame() ) {
			Debug.Log("SaveManager [OnEnable] : load game successful from '" + dataFileName + "'.");
		}
		else
			Debug.LogWarning("SaveManager [OnEnable] : load game unsuccessful.");
		
	}

	void OnDisable() {

		// auto save
		if ( SaveGame(game) ) {
			Debug.Log("SaveManager [OnDisable] : save game successful to '" + dataFileName + "'.");
		}
		else
			Debug.LogWarning("SaveManager [OnDisable] : save game unsuccessful.");
	}

	public	bool SaveGame( GameData game ) {

		bool	ret = false;

		if ( !File.Exists( dataFileName ) ) {

			BinaryFormatter bf = new BinaryFormatter();
			FileStream fs = File.Create( dataFileName );
			bf.Serialize( fs, game );
			fs.Close();

			ret = true;

		}
		else {

			BinaryFormatter bf = new BinaryFormatter();
			FileStream fs = File.Open( dataFileName, FileMode.Open );
			// write (overwrite)
			bf.Serialize( fs, game );
			fs.Close();

			ret = true;
		}

		return ret;
	}

	public	bool LoadGame() {

		bool	ret = false;

		if ( File.Exists( dataFileName ) ) {

			BinaryFormatter bf = new BinaryFormatter ();
			FileStream fs = File.Open( dataFileName, FileMode.Open );

			GameData gData = bf.Deserialize(fs) as GameData;
			fs.Close ();

			GameManager gm = GameObject.FindObjectOfType<GameManager>() as GameManager;
			if ( gm == null || !gm.PopulateGameData(gData) )
				Debug.LogError ( "--- Save Game Data [LoadGame] : game data population failed." );

			ret = true;

		}

		return ret;
	}
}
