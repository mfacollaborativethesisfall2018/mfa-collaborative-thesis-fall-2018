﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class OptionsManager : MonoBehaviour {

	// Author: Glenn Storm
	// Handles options data in coordination with audio manager

	AudioManager 			am;

	public	OptionsData		oData;
	public	Texture			sliderBar;
	public	Texture			sliderNub;
	public	int				fontSizeAt1024 = 24;
	public	Font			font;
	public	Color 			fontColor = Color.black;
	public	string			musicSample;
	public	string			sfxSample;
	public	string			voiceSample;

	private	bool			audioChanged;
	private	bool			gameScreen;
	private	bool			optionsScreen;
	private float			sampleTimer;
	private	bool			musicWasChanged;
	private	bool			sfxWasChanged;
	private bool			voiceWasChanged;
	private float			oldMusicSliderPos;
	private float			oldSfxSliderPos;
	//private float			oldVoiceSliderPos;
	private	bool			optionsDisplay;

	private	float			mouseX;
	private float			mSliderPos = 0.5f;
	private float			sSliderPos = 0.681f;
	private float			vSliderPos = 0.95f;

	void Start () {
	
		oData = new OptionsData();
		oData.InitializeOptionsData();
		// set options data to slider default values
		oData.musicVolume = ( mSliderPos - 0.05f ) / 0.9f;
		oData.sfxVolume = ( sSliderPos - 0.05f ) / 0.9f;
		oData.voiceVolume = ( vSliderPos - 0.05f ) / 0.9f;
		// initial volume set
		audioChanged = true;

		am = GameObject.FindObjectOfType<AudioManager>();
		if ( am == null ) {
			Debug.LogError("--- OptionsManager [Start] : audio manager not found. aborting.");
			enabled = false;
		}

		oldMusicSliderPos = mSliderPos;
		oldSfxSliderPos = sSliderPos;
		//oldVoiceSliderPos = vSliderPos;

		SceneManager.activeSceneChanged += SceneChanged;
		SceneManager.sceneUnloaded += SceneChanging;
	}

	void Update () {

		if ( audioChanged && ( gameScreen || am != null ) )
			UpdateAudioVolume();

		if ( !gameScreen ) {
			AudioIdentifier[] id = GameObject.FindObjectsOfType<AudioIdentifier>();
			for ( int i=0; i<id.Length; i++ ) {
				switch ( id[i].audioType ) {
				case AudioIdentifier.menuAudioType.SFX :
					id[i].SetManagedVolume( oData.sfxVolume );
					break;
				case AudioIdentifier.menuAudioType.Music :
					id[i].SetManagedVolume( oData.musicVolume );
					break;
				case AudioIdentifier.menuAudioType.Voice :
					id[i].SetManagedVolume( oData.voiceVolume );
					break;
				}
				id[i].optionsSynced = true;
			}
		}

		if ( !optionsDisplay && sampleTimer > 0f ) {
			sampleTimer -= Time.deltaTime;
			if ( sampleTimer <= 0f ) {
				if ( musicWasChanged ) {
					HaltAudio( musicSample );	
					am.PlayAudio( musicSample );
				}
				if ( sfxWasChanged ) {
					HaltAudio( sfxSample );
					am.PlayAudio( sfxSample );
				}
				if ( voiceWasChanged ) {
					HaltAudio( voiceSample );
					am.PlayAudio( voiceSample );
				}
				musicWasChanged = false;
				sfxWasChanged = false;
				voiceWasChanged = false;
			}
		}
	}

	void HaltAudio( string sound ) {

		if ( !am.IsPlaying( sound ) )
			return;

		for ( int i=0; i<am.sources.Length; i++ ) {
			if ( am.sources[i].clip.name == sound )
				am.sources[i].Stop();
		}
	}

	void SceneChanged( Scene fromScene, Scene toScene ) {

		optionsScreen = ( toScene.name == "Options" );
		gameScreen = ( toScene.name == "Arena1" || toScene.name == "Arena2" );
	}

	void SceneChanging( Scene fromScene ) {

		optionsScreen = false;
		gameScreen = false;
	}

	void UpdateAudioVolume() {
		
		if ( oData.musicVolume != am.musicVolume )
			am.UpdateVolume( AudioManager.aType.MusicLoop, oData.musicVolume );
		if ( oData.sfxVolume != am.sfxVolume )
			am.UpdateVolume( AudioManager.aType.SFXGame, oData.sfxVolume );
		if ( oData.voiceVolume != am.voiceVolume )
			am.UpdateVolume( AudioManager.aType.Voice, oData.voiceVolume );
		audioChanged = false;
	}

	public	void DisplayOptions( bool display ) {

		// override normal options display
		optionsDisplay = display;
	}

	void OnGUI() {

		if ( !optionsScreen && !optionsDisplay )
			return;

		Rect r = new Rect();
		float w = Screen.width;
		float h = Screen.height;
		GUIStyle bar = new GUIStyle();
		bar.normal.background = sliderBar as Texture2D;
		GUIStyle nub = new GUIStyle( GUI.skin.button );
		nub.normal.background = sliderNub as Texture2D;
		nub.hover.background = sliderNub as Texture2D;
		nub.active.background = sliderNub as Texture2D;

		GUI.depth = -99;

		r.x = 0.3f * w;
		r.y = 0.35f * h;
		r.width = 0.4f * w;
		nub.fixedWidth = 0.0381f * h;
		r.height = 0.0618f * h;

		mSliderPos = Mathf.Clamp( GUI.HorizontalSlider( r, mSliderPos, 0f, 1f, bar, nub ), 0.05f, 0.95f );
		if ( oData.musicVolume != ( mSliderPos - 0.05f ) / 0.9f ) {
			oData.musicVolume = ( mSliderPos - 0.05f ) / 0.9f;
			audioChanged = true;
		}
		if ( mSliderPos != oldMusicSliderPos ) {
			musicWasChanged = true;
			sampleTimer = 0.2f;
			oldMusicSliderPos = mSliderPos;
		}
		r.y += 0.15f * h;
		sSliderPos = Mathf.Clamp( GUI.HorizontalSlider( r, sSliderPos, 0f, 1f, bar, nub ), 0.05f, 0.95f );
		if ( oData.sfxVolume != ( sSliderPos - 0.05f ) / 0.9f ) {
			oData.sfxVolume = ( sSliderPos - 0.05f ) / 0.9f;
			audioChanged = true;
		}
		if ( sSliderPos != oldSfxSliderPos ) {
			sfxWasChanged = true;
			sampleTimer = 0.2f;
			oldSfxSliderPos = sSliderPos;
		}
		/*
		r.y += 0.15f * h;
		vSliderPos = Mathf.Clamp( GUI.HorizontalSlider( r, vSliderPos, 0f, 1f, bar, nub ), 0.05f, 0.95f );
		if ( oData.voiceVolume != ( vSliderPos - 0.05f ) / 0.9f ) {
			oData.voiceVolume = ( vSliderPos - 0.05f ) / 0.9f;
			audioChanged = true;
		}
		if ( vSliderPos != oldVoiceSliderPos ) {
			voiceWasChanged = true;
			sampleTimer = 0.2f;
			oldVoiceSliderPos = vSliderPos;
		}
		*/

		r.x = 0.4f * w;
		r.y = 0.425f * h;
		r.width = 0.2f * w;
		r.height = 0.05f * h;
		GUIStyle g = new GUIStyle( GUI.skin.label );
		g.alignment = TextAnchor.MiddleCenter;
		g.fontSize = Mathf.RoundToInt( fontSizeAt1024 * ( w / 1024 ) );
		g.font = font;
		g.normal.textColor = fontColor;
		GUI.Label( r, "Music Volume", g );
		r.y += 0.15f * h;
		GUI.Label( r, "SFX Volume", g );
		//r.y += 0.15f * h;
		//GUI.Label( r, "Voice Volume", g );

		if ( !optionsScreen ) {
			// allow volume to update while options are display in pause mode
			if ( audioChanged && ( gameScreen || am != null ) )
				UpdateAudioVolume();
			
			// allow samples to play while options are display in pause mode
			if ( musicWasChanged ) {
				HaltAudio( musicSample );	
				am.PlayAudio( musicSample );
			}
			if ( sfxWasChanged ) {
				HaltAudio( sfxSample );
				am.PlayAudio( sfxSample );
			}
			if ( voiceWasChanged ) {
				HaltAudio( voiceSample );
				am.PlayAudio( voiceSample );
			}
			musicWasChanged = false;
			sfxWasChanged = false;
			voiceWasChanged = false;
		}
	}
}
