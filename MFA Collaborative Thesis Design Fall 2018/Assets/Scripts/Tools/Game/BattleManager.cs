﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BattleManager : MonoBehaviour {

	// Author: Glenn Storm
	// This manages waves of enemies, level unlock triggers, and handles win and lose conditions


	[Serializable]
	public	struct EnemyWave
	{
		public	string					waveName;
		public	float					waveDelay;
		public	bool 					nextWaveMustBeTriggered;
		public	GameObject 				waveEventTrigger;
		public	CombatDummy[]			enemies;
	}
		
	public	string 						levelName;
	public	GameObject 					introEventTrigger;
	public	GameObject					endEventTrigger;

	public	GameObject					nextLevelUnlockTrigger;
	public	GameObject					winSceneEnd;
	public	GameObject					winAudioObject;
	public	GameObject					loseAudioObject;
	public	GameObject					deathSceneEnd;

	public	EnemyWave[]					waves;

	private	CombatComboTest				playerCombat;
	private	CombatDummy[]				currentWaveEnemies;
	private	bool						playerDied;
	private	float 						currentWaveTimer;
	private	int 						currentWave;
	private	bool						inCombat;
	private	bool 						wavesComplete;


	void Start () {
		// validate
		// acquire player combat script
		playerCombat = GameObject.FindObjectOfType<CombatComboTest>();
		if (playerCombat == null) {
			Debug.LogError ("--- BattleManager [Start] : "+gameObject.name+" no player combat script found. Aborting.");
			enabled = false;
		}
		// initialize
		// *** Rather than instantiating enemies during runtime (given we know what we want each wave), we will simply turn them on ***
		// find all enemies in waves and suspend them for now
		for (int i = 0; i < waves.Length; i++) {
			for (int e = 0; e < waves [i].enemies.Length; e++) {
				waves [i].enemies [e].SuspendEnemy(true);
			}
		}
		// start level
		LevelStart();
	}
	
	void Update () {
		// wave delay timer
		if (currentWave > -1 && currentWaveTimer > 0f) {
			currentWaveTimer -= Time.deltaTime;
			if (currentWaveTimer <= 0f) {
				currentWaveTimer = 0f;
				SpawnWave (currentWave);
			}
		}
		// detect current battle conditions (number enemies alive, player alive)
		bool enemiesDead = true;
		if (currentWaveEnemies != null && currentWaveEnemies.Length > 0) {
			foreach (CombatDummy cd in currentWaveEnemies) {
				if (!cd.IsDead ()) {
					enemiesDead = false;
					break;
				}
			}
		}
		else
			enemiesDead = false;
		playerDied = playerCombat.IsDead ();
		// condition to spawn next wave
		if ( !playerDied && enemiesDead ) {
			if ( currentWave < waves.Length )
				CurrentWaveDone ();
			else
				wavesComplete = true;
		}
		// condition win
		if ( wavesComplete )
			PlayerWin ();
		// condition lose
		if ( playerDied )
			PlayerLose ();
	}

	void SpawnWave( int waveIndex ) {
		if (waveIndex < 0 || waveIndex >= waves.Length) {
			Debug.LogWarning ("--- BattleManager [SpawnWave] : "+gameObject.name+" wave index out of range. Will ignore.");
			return;
		}
		// *** Rather than instantiating enemies at runtime (given we know what we want), simply turn them on ***
		// un-suspend enemies in this wave
		for ( int e=0; e<waves[waveIndex].enemies.Length; e++ ) {
			waves[waveIndex].enemies[e].SuspendEnemy(false);
		}
		// store current enemy list
		currentWaveEnemies = waves[waveIndex].enemies;
		inCombat = true;
	}

	void StopEnemies() {
		foreach (CombatDummy dum in currentWaveEnemies) {
			// stop AI
			dum.enabled = false;
		}
	}

	void CurrentWaveDone() {
		inCombat = false;
		currentWaveTimer = 0f;
		currentWaveEnemies = null;
		bool mustBeTriggered = waves [currentWave].nextWaveMustBeTriggered;
		currentWave++;
		if (currentWave >= waves.Length)
			wavesComplete = true;
		else {
			if ( waves[currentWave].waveEventTrigger != null )
				waves [currentWave].waveEventTrigger.SetActive (true);
			if ( !mustBeTriggered )
				currentWaveTimer = waves [currentWave].waveDelay;
		}
	}

	/// <summary>
	/// Player wins the level.
	/// </summary>
	/// <returns><c>true</c>, if player wins, <c>false</c> otherwise.</returns>
	public	bool PlayerWinsLevel() {
		PlayerWin ();
		return true;
	}

	void PlayerWin() {
		StopWaves ();
		// trigger win audio music
		winAudioObject.SetActive(true);
		// unlock next level(s)
		if ( nextLevelUnlockTrigger != null )
			nextLevelUnlockTrigger.SetActive( true );
		// activate menu scene trigger (or next level?)
		winSceneEnd.SetActive(true);
	}

	void PlayerLose() {
		StopEnemies ();
		StopWaves ();
		// trigger lose audio music
		loseAudioObject.SetActive( true );
		// activate death scene trigger
		deathSceneEnd.SetActive( true );
	}

	void LevelStart() {
		// trigger begin event or start waves
		if (introEventTrigger == null)
			StartWaves (0);
		else
			introEventTrigger.SetActive (true);
	}

	/// <summary>
	/// Starts the wave as configured in the Battle Manager.
	/// </summary>
	/// <param name="waveIndex">Wave index.</param>
	public	bool	StartWaves( int waveIndex ) {
		if (waveIndex >= waves.Length || waveIndex < 0 ) {
			Debug.LogWarning ("BattleManager [StartWave] : "+gameObject.name+" wave index "+waveIndex+" not found. Will ignore.");
			return false;
		}
		else {
			currentWave = waveIndex;
			currentWaveTimer = waves [waveIndex].waveDelay;
			return true;
		}
	}

	void StopWaves() {
		currentWave = -1; // not running
		currentWaveTimer = 0f;
		inCombat = false;
	}

	void LevelEnd() {
		// trigger end event
		endEventTrigger.SetActive(true);
	}

	/// <summary>
	/// Is the player engaged in combat.
	/// </summary>
	/// <returns><c>true</c>, if player is currently in a combat wave, <c>false</c> otherwise.</returns>
	public	bool	IsInCombat() {
		return inCombat;
	}

	void OnDrawGizmos() {
		bool battleValid = true;
		bool wavesValid = true;
		bool enemiesValid = true;
		// validate battle
		if (winSceneEnd == null || deathSceneEnd == null || winAudioObject == null || loseAudioObject == null || nextLevelUnlockTrigger == null)
			battleValid = false;
		// validate waves
		if (waves.Length < 1)
			wavesValid = false;
		// validate enemies
		if (waves.Length > 0) {
			for (int i = 0; i < waves.Length; i++) {
				if (waves [i].enemies.Length < 1)
					enemiesValid = false;
				else {
					for (int e = 0; e < waves [i].enemies.Length; e++) {
						if (waves [i].enemies [e] == null)
							enemiesValid = false;
					}
				}
			}
		}
		else
			enemiesValid = false;
		// display battle manager gizmo in editor scene
		Gizmos.color = Color.green;
		if ( !enemiesValid )
			Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (gameObject.transform.position, 1f);
		Gizmos.color = Color.green;
		if ( !wavesValid )
			Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (gameObject.transform.position, 2f);
		Gizmos.color = Color.green;
		if ( !battleValid )
			Gizmos.color = Color.red;
		Gizmos.DrawWireCube (gameObject.transform.position, Vector3.one * 2f);
	}
}
