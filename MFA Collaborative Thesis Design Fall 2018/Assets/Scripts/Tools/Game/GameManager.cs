﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	// Author: Glenn Storm
	// This manager handles top-level aspects of the game

	private	GameData			game;

	//public	ControlsManager		controlsMgr;
	public	AudioManager		audioMgr;
	public	SaveManager			saveMgr;
	//public	ItemManager			itemMgr;
	//public	PlayerManager		playerMgr;
	//public	PuzzleManager		puzzleMgr;
	public	UIManager			uiMgr;
	//public	AbilityManager		abilityMgr;
	public	OptionsManager		optionsMgr;

	public	GameObject			menuTrigger;


	void Start () {
		
		game = new GameData();
		game.ConfigureGame();

		RegisterManagers();

		// ensure random is random this game by randomizing seed
		Random.InitState( Random.Range( 0, Random.Range( 0, 100000 ) ) );
	}

	void Update() {
		
		game.gameTimeTotal += Time.deltaTime;
	}

	public	GameData	AccessGameData() {
		
		return game;
	}

	public	bool		PopulateGameData( GameData gData ) {
		
		if ( gData != null ) {
			game = gData;
			return true;
		}
		else
			return false;
	}

	public	void		EndGame() {

		menuTrigger.SetActive( true );
	}

	public	void		RegisterManagers() {

		//controlsMgr = GameObject.FindObjectOfType<ControlsManager>() as ControlsManager;
		audioMgr = GameObject.FindObjectOfType<AudioManager>() as AudioManager;
		saveMgr = GameObject.FindObjectOfType<SaveManager>() as SaveManager;
		//itemMgr = GameObject.FindObjectOfType<ItemManager>() as ItemManager;
		//playerMgr = GameObject.FindObjectOfType<PlayerManager>() as PlayerManager;
		//puzzleMgr = GameObject.FindObjectOfType<PuzzleManager>() as PuzzleManager;
		uiMgr = GameObject.FindObjectOfType<UIManager>() as UIManager;
		//abilityMgr = GameObject.FindObjectOfType<AbilityManager>() as AbilityManager;
		optionsMgr = GameObject.FindObjectOfType<OptionsManager>() as OptionsManager;
	}
}
