﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputQuickTimeTest : MonoBehaviour {

	// Author: Glenn
	// Player input tests

	public	enum InputMode
	{
		Direct,
		Toggle,
		QuickTime
	}
	[System.Serializable]
	public struct KeyMap {

		public	KeyCode 	key;
		public	GameObject	hudItem;
		public	InputMode	inputMode;
		public	float 		quickTimeDuration;
		public	ProgressBar	quickTimeBar;
		public	bool 		quickAchieved;
		public	float 		flashTime;

	}
	public	KeyMap[]	keyMaps;

	public	float 		quickTimeThreshold = 0.25f;

	private	float		currentTime;


	void Start () {
		
	}

	void Update () {

		// if SPACE, reset all
		if ( Input.GetKeyDown( KeyCode.Space ) ) {
			
			for (int i = 0; i < keyMaps.Length; i++) {
				keyMaps [i].flashTime = 0f;
				keyMaps [i].quickAchieved = false;
				keyMaps [i].hudItem.transform.GetChild (0).GetComponent<TextMesh> ().color = Color.red;
				keyMaps [i].quickTimeBar.barColor = Color.red;
				keyMaps [i].quickTimeBar.bgColor = Color.clear;
			}

		}

		currentTime += Time.deltaTime;

		// handle each keyMap
		for (int i = 0; i < keyMaps.Length; i++) {

			// flash achievements
			if (keyMaps [i].quickAchieved) {
				keyMaps [i].flashTime += Time.deltaTime;
				if (keyMaps [i].quickTimeBar != null) {
					Color c = Color.black;
					c.a = keyMaps [i].flashTime % 0.25f;
					keyMaps [i].quickTimeBar.bgColor = c;
				}
			}
			else {
				keyMaps [i].quickTimeBar.bgColor = Color.clear;
				keyMaps [i].flashTime = 0f;
			}

			// map key to hud appearance
			if (keyMaps[i].inputMode == InputMode.Direct) {
				keyMaps[i].hudItem.SetActive (Input.GetKey ( keyMaps[i].key ));
			}
			else if (keyMaps[i].inputMode == InputMode.Toggle) {
				if (Input.GetKeyDown (keyMaps[i].key)) {
					keyMaps[i].hudItem.SetActive (!keyMaps[i].hudItem.activeSelf);
				}
			}
			else if (keyMaps[i].inputMode == InputMode.QuickTime) {
				// check if toggle 'off' happens within time threshold
				if ( !keyMaps[i].quickAchieved ) {
					if (Input.GetKeyDown (keyMaps[i].key)) {
						keyMaps[i].hudItem.SetActive (!keyMaps[i].hudItem.activeSelf);
						if (keyMaps[i].hudItem.activeSelf) {
							currentTime = 0f;
							keyMaps[i].quickTimeBar.value = 1f;
						} else if (Mathf.Abs (currentTime - keyMaps[i].quickTimeDuration) < quickTimeThreshold) {
							keyMaps[i].hudItem.transform.GetChild (0).GetComponent<TextMesh> ().color = Color.white;
							keyMaps[i].hudItem.SetActive (!keyMaps[i].hudItem.activeSelf);
							keyMaps[i].quickTimeBar.barColor = Color.white;
							keyMaps[i].quickAchieved = true;
						}
					}
					keyMaps[i].quickTimeBar.value = 1 - ( currentTime / keyMaps[i].quickTimeDuration );
					if (currentTime >= keyMaps[i].quickTimeDuration)
						keyMaps[i].quickTimeBar.value = 0f;
				}
			}

		}

	}
}
