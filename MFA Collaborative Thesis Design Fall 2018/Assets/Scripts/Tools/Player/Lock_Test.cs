﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock_Test : MonoBehaviour {
    //Author Alecksandar Jackowicz
    //This Script will take the important parts of the camera swivel and lock on to make a clear example of how camera should work

    //public PlayerControllerTest playConTest;

    //Camera Swivel Variables
    public bool lockCameraPosition;
    public bool swivel;
    public GameObject thirdPersonController;
    public GameObject cameraRigSwivel;
    public float swivelTime = 2f;
    public Quaternion rigSwivelRot = Quaternion.identity;
    public Quaternion targetSwivel = Quaternion.identity;
    public GameObject enemyObject;
    public GameObject cameraAimObject;
    public at_AimLock aimLockComponent;
    public bool isLocked = false;

   //private float currentTime;
    //private bool runTimer;
    //private bool enemyLock;
    private Vector3 savedAimPosition;
    

    //Targeting Variables
    public Transform targetPoint, playerTrans;
    //public Camera cameraOBJ;
    public GameObject targetingCamera;
    //public float smoothTime;
    //private Vector3 velocity = Vector3.zero;
    private int currentID;

    //Camera Controller Targeting
   
    public Transform target;//要控制的对象

    



    //public Transform targetCamera;
    //private new Transform camera;


    // Use this for initialization
    void Start () {
        //Camera Swivel Start
        savedAimPosition = cameraAimObject.transform.localPosition;

        
        //Targeting Start
        if (playerTrans == null || playerTrans != GameObject.FindGameObjectWithTag("Player").transform) { 
            playerTrans = GameObject.FindGameObjectWithTag("Player").transform;
            target = playerTrans;
        }
        
    }
	
	// Update is called once per frame
	void Update () {

        //Test Inputs
        //Press Q to switch Lock on on/off
        if (Input.GetKeyDown(KeyCode.Q))
        {
            NearestTagEnemy();
            isLocked = !isLocked; 
           // playConTest.mouseLook = !playConTest.mouseLook;

           
            
        }


        //Targeting Update
        

        if (Input.GetKeyDown(KeyCode.Mouse2))
        {
            NextTagEnemy();
            //print(targetPoint.parent);
        }

        
            


        //Camera Swivel Update
        if (!isLocked) {
            //print("1");
            //Camera Follows

            if (lockCameraPosition)
            {
                cameraRigSwivel.transform.position = thirdPersonController.transform.position;
            }

            cameraAimObject.transform.localPosition = savedAimPosition;
            aimLockComponent.enabled = false;

            if (!swivel)
                return;

        }

        

        //Camera Controller targeting Update
        if (isLocked) {

            if (lockCameraPosition)
            {
                cameraRigSwivel.transform.position = thirdPersonController.transform.position;
            }

            cameraAimObject.transform.position = enemyObject.transform.position;
            aimLockComponent.enabled = true;

           

            // Don't do anything if target is not defined
            
            if (!target)
            {
             GameObject go = GameObject.Find("Player");
                target = go.transform;
                transform.LookAt(target);
                return;
            }
         
    

            
        }
    }



    //Targeting Functions
    //Finds the closest enemy and assigns enemyObject to its targetPoint
    public void NearestTagEnemy()
    {
        float closestDistance = Mathf.Infinity;
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject current in enemies)
        {
            
            float distance = (current.transform.Find("Target Point").position - playerTrans.position).sqrMagnitude;
            if (distance < closestDistance)
            {
                closestDistance = distance;
                targetPoint = current.transform.Find("Target Point");

                enemyObject = targetPoint.gameObject;
            }
        }
        //Debug.DrawLine(playerTrans.position, targetPoint.position, Color.red);
    }

    //Assigns enemyObject to the next enemy that is not locked on
    public void NextTagEnemy()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        string parse = this.targetPoint.GetComponentInParent<Transform>().gameObject.name;
        string[] parseSplit = parse.Split(' ');
        int.TryParse(parseSplit[1], out currentID);

        foreach (GameObject current in enemies)
        {
            int nextID;
            string parseNext = current.GetComponentInParent<Transform>().gameObject.name;
            string[] parseNextSplit = parseNext.Split(' ');
            int.TryParse(parseNextSplit[1], out nextID);

            if (currentID != nextID)
            {
                targetPoint = current.transform.Find("Target Point");
                enemyObject = targetPoint.gameObject;
                currentID = nextID;
                break;
            }
        }
    }

   

}
