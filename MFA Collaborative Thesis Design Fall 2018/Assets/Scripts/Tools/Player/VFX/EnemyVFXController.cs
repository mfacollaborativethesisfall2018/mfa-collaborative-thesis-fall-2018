﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Author: Kevin Caton-Largent
/// VFX controller controls when vfx appears on the enemy (wip)
/// </summary>
public class EnemyVFXController : MonoBehaviour {
	public List<ParticleSystem> dodgeEffect;
	public ParticleSystem parryEffect;
	public List<ParticleSystem> enemyHitEffect;
	public ParticleSystem deathEffect;
	public float dodgeTimer;
	public float defaultDodgeInterval;
	public float enemyHitTimer;
	public float defaultEnemyHitInterval;
	public bool dodgeActivated;
	public bool enemyHitActivated;

	public float timer;

	/// <summary>
	/// Helper function to reset enemy hit timers
	/// </summary>
	/// <param name="duration">new duration for enemy hit timers</param>
	public void ResetEnemyHitTimer(float duration)
	{
		defaultEnemyHitInterval = duration;
		enemyHitTimer = defaultEnemyHitInterval;
	}


    public void ActivateEnemyHit()
    {
        enemyHitActivated = !enemyHitActivated;
        foreach (ParticleSystem current in enemyHitEffect)
        {
            if (enemyHitActivated)
                current.Play();
            else
                current.Stop();
        }

    }


    public void ActivateParry()
	{
		// Debug.Log("Enemy Parry");
		if (!parryEffect.isPlaying)
            parryEffect.Play();
	}
	/// <summary>
	/// Helper function to reset dodge timers
	/// </summary>
	/// <param name="duration">new duration for dodge timers</param>
	public void ResetDodgeTimer(float duration)
	{
		defaultDodgeInterval = duration;
		dodgeTimer = defaultDodgeInterval;
	}

	public void ToggleDodge()
	{

		dodgeActivated = !dodgeActivated;
		// Debug.Log("Dodge");
		foreach (ParticleSystem current in dodgeEffect)
		{
			if (dodgeActivated)
				current.Play();

			else
				current.Stop();
		}



	}
	public void ActivateDeath()
	{
		// Debug.Log("Enemy Death");
		if (!deathEffect.isPlaying)
			deathEffect.Play();

	}
	private void Start()
	{
		dodgeActivated = false;
		if (defaultDodgeInterval <= 0f)
			defaultDodgeInterval = .6f;
	}

	private void Update()
	{
		if (dodgeActivated)
		{
			dodgeTimer -= Time.deltaTime;
			if (dodgeTimer <= 0f)
			{
				dodgeTimer = defaultDodgeInterval;
				ToggleDodge();
			}
		}
		if (enemyHitActivated)
		{
			enemyHitTimer -= Time.deltaTime;
			if (enemyHitTimer <= 0f)
			{
				enemyHitTimer = defaultEnemyHitInterval;
				ActivateEnemyHit();
			}
		}
	}

	public void ActivateHit()
    {
        // this is controlled via an auxilliary system now so its use is deprecated
        
        enemyHitActivated = !enemyHitActivated;
        foreach (ParticleSystem current in enemyHitEffect)
        {
            if (enemyHitActivated)
                current.Play();
            else
                current.Stop();
        }
    }
}
