﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatDummy : MonoBehaviour {

	// Author: Glenn Storm
	// This script handles dummy combat

	public	enum EnemyType
	{
		Fast,
		Heavy
	}
	public	EnemyType	dummyType;

    public enum CurrentAttackType
    {
        Light,
        Heavy        
    }
	public	GameObject	standInModel;
	public	GameObject	fastEnemyModel;
	public	GameObject	heavyEnemyModel;
    public  GameObject  fastVFXHitboxes; // Kevin
    public  GameObject  heavyVFXHitboxes; // Kevin
    public	EnemyVFXController vfxController; // Kevin
	public	GameObject	deathVFX; // Kevin

	public	float		moveSpeed = 1f;
	public	float		turnSpeed = 5f;
	public	float		combatRange = 5f;
	public	float		combatRate = 2.5f;
	public	float		combatDamage = 1f;
    /// <summary>
    ///  Kevin
    /// </summary>
    public  string      AttackCue;
    public  string      CurrentAttack;
    public  float       combatHeavyAttackDamage = 1f; 
    public  float       blockTimer; 
    public  float       parryTimer;
    public  float       dodgeTimer;
    public  float       dodgeActivationTimer;
    public  float       blockActivationTimer;
    public  float       beatTime; 
    public  int         beatMax = 4;
    //public  int         thisBeat;
    public  bool        isEnemyBlocking; // activate the enemy's block when triggered
    public  bool        isEnemyParrying; // to keep track for reseting parry
    public  bool        isEnemyDodging;
    public  bool        bIsBlockingAvailable;
    public  bool        bIsDodgingAvailable;
    public  bool        queueHeavyAttackDamage; // used for applying proper heavy attack damage


    public  bool        lightAttack1;
    public  bool        lightAttack2;

    public  bool        heavyAttack1;
    public  bool        heavyAttack2;
    public  bool        heavyAttack3;

    public  float        resultTimer;
    public  float        currentTime;
    
    /// <summary>
    /// End of Kevin Block
    /// </summary>

    public	bool 		enemyStunned;
	public	float 		enemyStunTime;

	public	bool 		enemySlow;
	public	float		enemySlowTime;
	public	float		enemyTimeRate = 1f;	// maintains this enemy relative time scale
	public	float 		enemyTimeSlowRate = 0.25f;

	public	bool 		enemySuspended; // wave suspend via battle manager


    private CombatComboTest cct;
	private	Rigidbody	rb;
	private	GameObject	playerObject;
    public	bool		inCombat;
	public	float		combatTime;

	private	float		health = 300f;
    public bool         startCombo; // Kevin
    public float        dodgeDelay;

	// anim controller properties (to be relayed to anim controller)
	private	Animator	animComponent;
	private	float		forward;
	private	float		turn;
	private	bool 		hit;
	private	bool 		stunned;
	private	bool 		dead;
    private bool        resetNeeded;


    const   float       ENEMYHITSTUNMAX = 2f;
	const	float 		ENEMYSTUNMAX = 5f;
	const	float		ENEMYSLOWMAX = 5f;
	const	float 		ENEMYATTACKANGLE = 30f;

	const	float 		FASTENEMYHEALTH = 300f;
	const	float 		FASTENEMYDAMAGE = 50f;
	const	float 		FASTENEMYATTACKDELAY = 1.5f;
	const	float 		FASTENEMYMOVESPEED = 1f;
    const   float       DODGEFORCE = 60f; // Kevin : enemy is lighter than the player so it's dodge should be more
    const   float       DODGEMAX = 0.6f; // Kevin
    const   float       FASTDODGEDELAY = 10f; // Kevin


    const   float	    HEAVYENEMYHEALTH = 600f;
	const	float 		HEAVYENEMYDAMAGE = 75f;
    const   float       HEAVYENEMYHEAVYATTACKDAMAGE = 150f; // Kevin : allows for heavy attack to be the most powerful attack heavy's have while still hitting hard normally
	const	float 		HEAVYENEMYATTACKDELAY = 2.5f;
	const	float 		HEAVYENEMYMOVESPEED = 0.5f;
    const   float       HEAVYBLOCKINGMAX = 5f; // Kevin
    const   float       PARRYINGMAX = 2f; // Kevin
    const   float       HEAVYBLOCKDELAY = 10f; // Kevin
    const   float       HEAVYDODGEDELAY = 10f; // Kevin


    void Awake() {
		rb = gameObject.GetComponent<Rigidbody> ();
    }

    void Start () {
		// TODO: validate anim controller
		// initialize type

		if (dummyType == EnemyType.Fast) {
			moveSpeed = FASTENEMYMOVESPEED;
			combatRange = 5f;
			combatRate = FASTENEMYATTACKDELAY;
			combatDamage = FASTENEMYDAMAGE;
			health = FASTENEMYHEALTH;
			animComponent = fastEnemyModel.GetComponent<Animator> ();
            dodgeDelay = FASTDODGEDELAY;
			// TEMP visualization
			standInModel.SetActive (false);
			fastEnemyModel.SetActive (true);
			// adjust collider settings for smaller enemy
			CapsuleCollider cc = gameObject.GetComponent<CapsuleCollider>();
			if (cc != null) {
                /*cc.radius = 2f;
				cc.height = 8f;
				Vector3 v = Vector3.zero;
				v.y = 4f;*/
                cc.radius = 1f;
                cc.height = 0f;
                Vector3 v = Vector3.zero;
                v.y = 0.94f;
                v.z = 0.0f;
                v.x = 0.0f;
                cc.center = v;
			}
		}
		else if (dummyType == EnemyType.Heavy) {
			moveSpeed = HEAVYENEMYMOVESPEED;
			combatRange = 5f;
			combatRate = HEAVYENEMYATTACKDELAY;
			combatDamage = HEAVYENEMYDAMAGE;
            combatHeavyAttackDamage = HEAVYENEMYHEAVYATTACKDAMAGE;
            health = HEAVYENEMYHEALTH;
			animComponent = heavyEnemyModel.GetComponent<Animator> ();
            dodgeDelay = HEAVYDODGEDELAY;
			// TEMP visualization
			standInModel.SetActive (false);
			heavyEnemyModel.SetActive (true);
			// default collider settings
		}
		cct = GameObject.FindObjectOfType<CombatComboTest> ().GetComponent<CombatComboTest> ();
		if (cct == null)
			Debug.LogWarning ("no combat combo test found. will ignore.");
		playerObject = GameObject.FindObjectOfType<CodeStore> ().gameObject as GameObject;
        combatTime = combatRate;
        currentTime = 0f;
        beatTime = .25f;
        bIsDodgingAvailable = true;
        bIsBlockingAvailable = true;
        //callTimer = 0f;
        //lastCall = 0f;
	}

	/// <summary>
	/// Suspends the enemy.
	/// </summary>
	/// <param name="suspendNow">If set to <c>true</c> suspend now.</param>
	public	void SuspendEnemy( bool suspendNow ) {
		if (suspendNow) {
			// make enemy invisible
			if (dummyType == EnemyType.Fast) {
				Renderer[] rs = fastEnemyModel.GetComponentsInChildren<Renderer> ();
				foreach (Renderer r in rs) {
					r.enabled = false;
				}
			}
			else if (dummyType == EnemyType.Heavy) {
				Renderer[] rs = heavyEnemyModel.GetComponentsInChildren<Renderer> ();
				foreach (Renderer r in rs) {
					r.enabled = false;
				}
			}
			// remove enemy collision and physics gravity
			gameObject.GetComponent<Collider> ().enabled = false;
			rb.useGravity = false;

        }
		else {
			// reveal enemy
			if (dummyType == EnemyType.Fast) {
				Renderer[] rs = fastEnemyModel.GetComponentsInChildren<Renderer> ();
				foreach (Renderer r in rs) {
					r.enabled = true;
				}
			}
			else if (dummyType == EnemyType.Heavy) {
				Renderer[] rs = heavyEnemyModel.GetComponentsInChildren<Renderer> ();
				foreach (Renderer r in rs) {
					r.enabled = true;
				}
			}
			// make enemy solid and fall with gravity
			gameObject.GetComponent<Collider> ().enabled = true;
			rb.useGravity = true;

        }
		enemySuspended = suspendNow;
	}

	void Update () {
            
        // SUSPEND HOOK (via Battle Manager)
        if (enemySuspended)
			return;
       
        // anim controller properties
        if (!resetNeeded)
        {
            ClearAnimFlags();
            hit = false;
        }
        else
        {
            resetNeeded = false;
        }

        if (resultTimer > 0f)
        {
            RunResultTimer();
        }

        stunned = enemyStunned;
        // anim controller relay
        if (animComponent != null)
        {
            if(animComponent.GetFloat("Current Speed") <= 0.0f)
            {
                turn = 0.0f;
                forward = 0.0f;
            }
            animComponent.speed = enemyTimeRate;
            animComponent.SetFloat("Forward", forward);
            animComponent.SetFloat("Turn", turn);
            animComponent.SetBool("Hit", hit);
            animComponent.SetBool("Stunned", stunned);
            animComponent.SetBool("Dead", dead);
        }
        // SLOW HOOK (Relative Time Management)
        if (enemySlow)
        {
            enemySlowTime -= Time.deltaTime;
            if (enemySlowTime <= 0f)
            {
                enemySlowTime = 0f;
                enemyTimeRate = 1f;
                // TODO: reset animation rate
                enemySlow = false;
            }
            else
            {
                enemyTimeRate = enemyTimeSlowRate;
                // TODO: alter animation rate
            }
        }

        // Block Hook
        if (isEnemyBlocking)
        {
            if (enemyStunned)
            {
                enemyStunned = false;
            }
            blockTimer -= Time.deltaTime;
            if (blockTimer <= 0f)
            {
                blockTimer = 0f;
                isEnemyBlocking = false;
                //queueHeavyAttack = false;
                //queueLightAttack = true;
                animComponent.SetBool("Block", false);
                float attackDistanceCheck = Vector3.Distance(gameObject.transform.position, playerObject.transform.position);
                Debug.Log("AttackDistanceCheck: " + attackDistanceCheck);
                if (attackDistanceCheck <= combatRange)
                {
                    if (dummyType == EnemyType.Heavy)
                    {
                        AddAttackToCue(CurrentAttackType.Light);
                    }
                    PlayAttackAnimation();
                }
            }
        }
        // Parrying Hook
        if (isEnemyParrying)
        {
            if (enemyStunned)
            {
                enemyStunned = false;
            }
            parryTimer -= Time.deltaTime;
            if (parryTimer <= 0f)
            {
                parryTimer = 0f;
                isEnemyParrying = false;
                animComponent.SetBool("Parry", false);
                float attackDistanceCheck = Vector3.Distance(gameObject.transform.position, playerObject.transform.position);
                Debug.Log("AttackDistanceCheck: " + attackDistanceCheck);
                if (attackDistanceCheck <= combatRange)
                {
                    if (dummyType == EnemyType.Heavy)
                    {
                        AddAttackToCue(CurrentAttackType.Heavy);
                    }
                    PlayAttackAnimation();
                }
            }
        }

        // STUN HOOK
        if (enemyStunned)
        {
            enemyStunTime -= Time.deltaTime * enemyTimeRate;
            if (enemyStunTime <= 0f)
            {
                enemyStunTime = 0f;
                enemyStunned = false;
            }
            else
                return; // if stunned, no control
        }
      
        // Dodge hook
        if (isEnemyDodging)
        {
            dodgeTimer -= Time.deltaTime;
            if (dodgeTimer <= 0f)
            {
                dodgeTimer = 0f;
                isEnemyDodging = false;
                if (vfxController.dodgeActivated)
                {
                    vfxController.ResetDodgeTimer(DODGEMAX);
                    vfxController.ToggleDodge();
                }
            }
        }
        if (!bIsBlockingAvailable)
        {
            blockActivationTimer -= Time.deltaTime;
            if (blockActivationTimer <= 0f)
            {
                blockActivationTimer = HEAVYBLOCKDELAY;
                bIsBlockingAvailable = true;
            }
        }
        if (!bIsDodgingAvailable)
        {
            dodgeActivationTimer -= Time.deltaTime;
            if (dodgeActivationTimer <= 0f)
            {
                dodgeActivationTimer = dodgeDelay;
                bIsDodgingAvailable = true;
            }
        }

        

        if (startCombo)
        {
            currentTime += Time.deltaTime;
            if (currentTime > (beatTime * beatMax))
            {
                currentTime = 0f;
                startCombo = false;
                EndCombo();
            }
            //thisBeat = Mathf.RoundToInt((currentTime / beatTime) + 0.5f);
        }
        
            


        Vector3 m = playerObject.transform.position - gameObject.transform.position;
		float ang = Vector3.SignedAngle( gameObject.transform.forward, m, Vector3.up );
		if (inCombat)
        {
            if (animComponent != null)
            {
                animComponent.SetFloat("Current Speed", 0.0f);
            }

            if (dummyType == EnemyType.Fast)
            {
                EnemyFastCombatBehavior();
            }
            else if (dummyType == EnemyType.Heavy)
            {
                EnemyHeavyCombatBehavior();
            }

            combatTime -= Time.deltaTime * enemyTimeRate;
            if (combatTime <= 0f)
            {
                if (Mathf.Abs(ang) <= ENEMYATTACKANGLE)
                {
                    combatTime = combatRate;
                    Attack();
                }
            }
			
		}
		else {
            // move
            if (animComponent != null)
            {
                animComponent.SetFloat("Current Speed", moveSpeed * enemyTimeRate);
            }
            m.Normalize ();
			m *= moveSpeed * enemyTimeRate;
			m.y = 0f;
			forward = m.z / moveSpeed;
			rb.AddForce (m, ForceMode.Impulse);
			// range check
			float d = Vector3.Distance (gameObject.transform.position, playerObject.transform.position);
			if (d <= combatRange) {
                inCombat = true;
            }
		}
		// turn
		if ( Mathf.Abs(ang) > (ENEMYATTACKANGLE / 2f) ) {
			ang = Mathf.Clamp (ang, -turnSpeed * enemyTimeRate, turnSpeed * enemyTimeRate);
			turn = ( ang / turnSpeed );
			//rb.AddTorque (0f, ang, 0f,ForceMode.Force);
			transform.Rotate(0, ang, 0);
		}
	}

	public	float	HealthPct() {
		float pct = 1f;
		if (dummyType == EnemyType.Fast)
			pct = (health / FASTENEMYHEALTH);
		else if (dummyType == EnemyType.Heavy)
			pct = (health / HEAVYENEMYHEALTH);
		return pct;
	}

	public	bool	IsDead() {
		return (health <= 0f);
	}

	void Attack() {

		// PERFECT BLOCK HOOK
		if (cct != null && cct.perfectBlock.activeSelf) {
			enemyStunned = true;
			enemyStunTime = ENEMYSTUNMAX;
			cct.DoParry ( this );
			return; // cancel attack
		}

		// PERFECT DODGE HOOK
		if (cct != null && cct.perfectDodge.activeSelf) {
			enemySlow = true;
			enemySlowTime = ENEMYSLOWMAX;
			print ("perfect dodge time slow");
		}

		float d = Vector3.Distance (gameObject.transform.position, playerObject.transform.position);
		if (d <= combatRange) {
            PlayAttackAnimation();
		}
		else {
			combatTime = combatRate;
			inCombat = false;
		}
	}
    private bool IsThePlayerCurrentlyAttacking()
    {
        AnimatorClipInfo[] currentClipInfo = cct.playerAnimator.GetCurrentAnimatorClipInfo(0);
        if (currentClipInfo[0].clip.name.Contains("Light Attack 1"))
        {
            Debug.Log("Player's Light Attack 1 confirmed by enemy");
            return true;
        }
        else if (currentClipInfo[0].clip.name.Contains("Light Attack 2"))
        {
            Debug.Log("Player's Light Attack 2 confirmed by enemy");
            return true;
        }
        else if (currentClipInfo[0].clip.name.Contains("Light Attack 3"))
        {
            Debug.Log("Player's Light Attack 3 confirmed by enemy");
            return true;
        }
        else if (currentClipInfo[0].clip.name.Contains("Heavy Attack 1"))
        {
            Debug.Log("Player's Heavy Attack 1 confirmed by enemy");
            return true;
        }
        else if (currentClipInfo[0].clip.name.Contains("Heavy Attack 2"))
        {
            Debug.Log("Player's Heavy Attack 2 confirmed by enemy");
            return true;
        }
        else if (currentClipInfo[0].clip.name.Contains("Heavy Attack 3"))
        {
            Debug.Log("Player's Heavy Attack 3 confirmed by enemy");
            return true;
        }
        else
        {
            return false;
        }
       
    }

    private void ClearAnimFlags()
    {
        if (dummyType == EnemyType.Fast)
        {
            animComponent.SetBool("Dodge Left", false);
            animComponent.SetBool("Dodge Right", false);

            animComponent.SetBool("Light Attack 1", false);
            animComponent.SetBool("Light Attack 2", false);

            animComponent.SetBool("Heavy Attack 1", false);
            animComponent.SetBool("Heavy Attack 2", false);

            animComponent.SetBool("Hit", false);
        }
        else if (dummyType == EnemyType.Heavy)
        {
            animComponent.SetBool("Dodge Back", false);

            animComponent.SetBool("Light Attack", false);

            animComponent.SetBool("Heavy Attack 1", false);
            animComponent.SetBool("Heavy Attack 2", false);
            animComponent.SetBool("Heavy Attack 3", false);

            animComponent.SetBool("Block", false);
            animComponent.SetBool("Parry", false);
            animComponent.SetBool("Hit", false);
        }
    }

    private void EndCombo()
    {
        beatMax = 4;
        resultTimer = 0.25f;
    }

    private void RunResultTimer()
    {
        resultTimer -= Time.deltaTime;
        if (resultTimer <= 0f)
        {
            resultTimer = 0f;
            ClearAttackBooleans();
        }

    }

    private void ClearAttackBooleans()
    {
        lightAttack1 = false;
        lightAttack2 = false;
        heavyAttack1 = false;
        heavyAttack2 = false;
        heavyAttack3 = false;
    }

    private bool AmInAnAttackAnimation()
    {
        AnimatorClipInfo[] currentClipInfo = animComponent.GetCurrentAnimatorClipInfo(0);
        if (currentClipInfo.Length == 0)
        {
            return false;
        }
        else if (currentClipInfo[0].clip.name.Contains("Light Attack"))
        {
            Debug.Log("I am: " + currentClipInfo[0].clip.name);
            CurrentAttack = currentClipInfo[0].clip.name;
            return true;
        }
        else if (currentClipInfo[0].clip.name.Contains("Heavy Attack"))
        {
            Debug.Log("I am: " + currentClipInfo[0].clip.name);
            CurrentAttack = currentClipInfo[0].clip.name;
            return true;
        }
        else
        {
            return false;
        }
    }
    private void PlayAttackAnimation()
    {
        
        if (AttackCue == "")
        {
            return;
        }

        if (dummyType == EnemyType.Fast)
        {

            if (AttackCue.Equals("Light Attack 1"))
            {
                animComponent.SetBool("Light Attack 1", true);
                //animComponent.Play("Light Attack 1");
                resetNeeded = true;
                startCombo = true;
                beatMax = 4;
            }
            else if (AttackCue.Equals("Light Attack 2"))
            {
                animComponent.SetBool("Light Attack 2", true);
                //animComponent.Play("Light Attack 2");
                resetNeeded = true;
                currentTime = 0f;
                beatMax = 4;
            }
            else if (AttackCue.Equals("Heavy Attack 1"))
            {
                animComponent.SetBool("Heavy Attack 1", true);
                //animComponent.Play("Heavy Attack 1");
                resetNeeded = true;
                startCombo = true;
                beatMax = 4;
            }
            else if (AttackCue.Equals("Heavy Attack 2"))
            {
                animComponent.SetBool("Heavy Attack 2", true);
                //animComponent.Play("Heavy Attack 2");
                resetNeeded = true;
                currentTime = 0f;
                beatMax = 4;
            }
            
        }
        else if (dummyType == EnemyType.Heavy)
        {


            if (AttackCue.Equals("Light Attack"))
            {
                animComponent.SetBool("Light Attack", true);
                //animComponent.Play("Light Attack");
                resetNeeded = true;
                startCombo = true;
                beatMax = 4;
            }
            else if (AttackCue.Equals("Heavy Attack 1"))
            {
                animComponent.SetBool("Heavy Attack 1", true);
                //animComponent.Play("Heavy Attack 1");
                resetNeeded = true;
                startCombo = true;
                beatMax = 4;
            }
            else if (AttackCue.Equals("Heavy Attack 2"))
            {
                animComponent.SetBool("Heavy Attack 2", true);
                //animComponent.Play("Heavy Attack 2");
                resetNeeded = true;
                currentTime = 0f;
                beatMax = 4;
            }
            else if (AttackCue.Equals("Heavy Attack 3"))
            {
                animComponent.SetBool("Heavy Attack 3", true);
                //animComponent.Play("Heavy Attack 3");
                resetNeeded = true;
                currentTime = 0f;
                beatMax = 4;
            }

        }
        AttackCue = "";
    }
   /// <summary>
   /// Adds the attack to the cue line.
   /// </summary>
    private void AddAttackToCue(CurrentAttackType attackType)
    {
        if (dummyType == EnemyType.Fast)
        {
            if (attackType == CurrentAttackType.Heavy)
            {
                if (heavyAttack1)
                {
                    AttackCue = "Heavy Attack 2";
                    heavyAttack1 = false;
                    heavyAttack2 = true;
                }
                else if (heavyAttack2)
                {
                    AttackCue = "Heavy Attack 1";
                    heavyAttack1 = true;
                    heavyAttack2 = false;
                }
                else if (!heavyAttack1 && !heavyAttack2)
                {
                    heavyAttack1 = true;
                    AttackCue = "Heavy Attack 1";
                }
            }
            else if (attackType == CurrentAttackType.Light)
            {
                if (lightAttack1)
                {
                    AttackCue = "Light Attack 2";
                    lightAttack1 = false;
                    lightAttack2 = true;
                }
                else if (lightAttack2)
                {
                    AttackCue = "Light Attack 1";
                    lightAttack1 = true;
                    lightAttack2 = false;
                }
                else if (!lightAttack1 && !lightAttack2)
                {
                    AttackCue = "Light Attack 1";
                    lightAttack1 = true;
                }
            }
        }
        else if (dummyType == EnemyType.Heavy)
        {
            //Debug.Log("Add attack cue heavy enemy ");
            if (attackType == CurrentAttackType.Heavy)
            {
                if (heavyAttack1)
                {
                    AttackCue = "Heavy Attack 2";
                    heavyAttack1 = false;
                    heavyAttack2 = true;
                }
                else if (heavyAttack2)
                {
                    AttackCue = "Heavy Attack 3";
                    heavyAttack2 = false;
                    heavyAttack3 = true;
                }
                else if (heavyAttack3)
                {
                    AttackCue = "Heavy Attack 1";
                    heavyAttack1 =  true;
                    heavyAttack3 = false;
                }
                else if (!heavyAttack1 && !heavyAttack2 && !heavyAttack3)
                {
                    heavyAttack1 = true;
                    AttackCue = "Heavy Attack 1";
                }
            }
            else if (attackType == CurrentAttackType.Light)
            {
                AttackCue = "Light Attack";
            }
        }
    }
    /// <summary>
    /// Cause the enemy to dodge
    /// </summary>
    private void Dodge()
    {
        if (bIsDodgingAvailable)
        {
            bIsDodgingAvailable = false;
        }
        else
        {
            return;
        }
        if (!isEnemyDodging)
        {
            if (startCombo)
            {
                //int thisBeat = Mathf.RoundToInt((currentTime / beatTime) + 0.5f);

                currentTime = 0f;
            }
            else
            {
                startCombo = true;
            }

            beatMax = 4;

            isEnemyDodging = true;
            vfxController.ResetDodgeTimer(DODGEMAX);
            vfxController.ToggleDodge();
            int dodgeChoice = Random.Range(1, 3);

            if (dodgeChoice == 1)
            {
                rb.AddForce(rb.gameObject.transform.right * -DODGEFORCE, ForceMode.Impulse);
                animComponent.SetBool("Dodge Left", true);
                resetNeeded = true;

            }
            else if (dodgeChoice == 2)
            {
                rb.AddForce(rb.gameObject.transform.right * DODGEFORCE, ForceMode.Impulse);
                animComponent.SetBool("Dodge Right", true);
                resetNeeded = true;
            }

        }
    }
    /// <summary>
    /// Cause the enemy to Block
    /// </summary>
    private void Block()
    {
        if (bIsBlockingAvailable)
        {
            bIsBlockingAvailable = false;
        }
        else
        {
            return;
        }
        if (!isEnemyBlocking)
        {
            if (startCombo)
            {
                //int thisBeat = Mathf.RoundToInt((currentTime / beatTime) + 0.5f);

                currentTime = 0f;
            }
            else
            {
                startCombo = true;
            }

            blockTimer = HEAVYBLOCKINGMAX;
            isEnemyBlocking = true;
            animComponent.SetBool("Block", true);
            resetNeeded = true;
            beatMax = 5;
        }
    }

    /// <summary>
    /// Cause the enemy to Parry
    /// </summary>
    private void Parry()
    {
        if (!bIsBlockingAvailable)
        {
            return;
        }
        if (!vfxController.parryEffect.isPlaying)
        {
            //Debug.Log("Enemy is activating their parry");
            vfxController.ActivateParry();
            parryTimer = PARRYINGMAX;

            isEnemyParrying = true;
            isEnemyBlocking = false;

            cct.ResetPlayerStunTimer();
            cct.playerStunned = true;
            animComponent.SetBool("Parry", true);
            resetNeeded = true;

        }
    }
    /// <summary>
    ///  Applies damage and knockback to the player
    /// </summary>
    private void ApplyDamageAndKnockback()
    {
        float d = Vector3.Distance(gameObject.transform.position, playerObject.transform.position);
        if (d > combatRange)
        {
            queueHeavyAttackDamage = false;
            return; // return if the player is not within the proper range for damage
        }

        float cdam = 0f;

        if (queueHeavyAttackDamage)
        {
            cdam = combatHeavyAttackDamage;
            //Debug.Log("Enemy heavy attack strike");
        }
        else
        {
            cdam = combatDamage;
            //Debug.Log("Enemy light attack strike");
        }

       

        // reference to player combat component (hit)
        if (cct != null && cct.block.activeSelf)
            cdam *= 0.25f; // block damage reduction
        playerObject.GetComponent<CodeStore>().playerCombat.TakeDamage(cdam);
        // knockback player
        if (queueHeavyAttackDamage)
        {
           // Debug.Log("Apply Knockback");
            playerObject.GetComponent<Rigidbody>().AddForce(playerObject.GetComponent<CodeStore>().playerCombat.Knockback((cdam), gameObject.transform.position, playerObject.transform.position), ForceMode.Impulse);
        }
        queueHeavyAttackDamage = false;
        
    }

    /// <summary>
    /// The main ai behavior for a fast enemy
    /// </summary>
    private void EnemyFastCombatBehavior()
    {
        // what is the state of attack   start < currentState < middle
        // dodge if currentState is in this range
        // attack if currentState is outside this range
        float d = Vector3.Distance(gameObject.transform.position, playerObject.transform.position);
        if (d > combatRange)
        {
            return;
        }
        if (!IsThePlayerCurrentlyAttacking())
        {
            // queue fast attack
            // queueLightAttack = true;
            // queueHeavyAttack = false;
            AddAttackToCue(CurrentAttackType.Light);
        }
        else
        {
            Dodge();
        }
    }


    /// <summary>
    /// The main ai behavior for a heavy enemy
    /// </summary>
    private void EnemyHeavyCombatBehavior()
    {
        // what is the state of the attack
        // start or middle = block
        // finish = parry
        // after blocks, attack player
        // after parries, heavy attack player
        // if there is no attack state, heavy attack the player
        float d = Vector3.Distance(gameObject.transform.position, playerObject.transform.position);
        if (d > combatRange)
        {
            return;
        }
        if (!IsThePlayerCurrentlyAttacking())
        {
            // queue heavy attack
            // queueLightAttack = false;
            // queueHeavyAttack = true;
            AddAttackToCue(CurrentAttackType.Heavy);
        }
        else
        {
            int playerBeat = Mathf.RoundToInt((cct.currentTime / cct.beatTime) + 0.5f);

            if (playerBeat > 0)
            {
                Debug.Log("Player Beat is: " + playerBeat);
            }

            if (playerBeat >= 2)
            {
                Block();
            }
            else if (playerBeat == 1)
            {
                Parry();
            }
            
        }
    }

    public	void DoAttackDamage( string attackType ) {
        // TEMP - legacy attack routine, non-specific
        //Attack();

        // add distance check
        
		// called from AnimEventListener to perform attack at correct anim time
		if (dummyType == EnemyType.Fast) {
			switch (attackType) {
			case "LightAttack":
                    // fast enemy light attack hit results
                    ApplyDamageAndKnockback();
                    break;
			case "HeavyAttack":
                    queueHeavyAttackDamage = true;
                    ApplyDamageAndKnockback();
                    // fast enemy heavy attack hit results
                    break;
			}
		}
		else if (dummyType == EnemyType.Heavy) {
			switch (attackType) {
			case "LightAttack":
                    // heavy enemy light attack hit results
                    ApplyDamageAndKnockback();
                    break;
			case "HeavyAttack":
                    queueHeavyAttackDamage = true;
                    // heavy enemy heavy attack hit results
                    ApplyDamageAndKnockback();
                    break;
			case "Parry":
                    // heavy enemy parry hit results 
                    //? Not sure we had anything about parrying causing damage in GDD
				break;
			}
		}
	}

	public void TakeDamage( float dam ) {

        float cdam = dam;
        if (isEnemyDodging)
        {
            //Debug.Log("Enemy is dodging; it can't be hurt atm");
            cdam *= 0.15f;
        }


        if (isEnemyBlocking)
            cdam *= 0.25f; // block damage reduction

        health -= cdam;
		hit = true;
		resetNeeded = true;
		if (health <= 0) {
			health = 0;
			Dead ();
		}
        else
        {
            
            if (!stunned)
            {
                enemyStunned = true;
                enemyStunTime = ENEMYHITSTUNMAX;
            }
            vfxController.ResetEnemyHitTimer(1.5f);
            vfxController.ActivateEnemyHit();
        }
	}

	void Dead() {
		// reset time
		enemySlow = false;
		enemyTimeRate = 1f;
		// TODO: reset anim rate
		dead = true;
		if (deathVFX != null) {
			vfxController.ActivateDeath ();
			deathVFX.transform.parent = null;
			GameObject.Destroy (deathVFX, 3f);
		}
		//Destroy (gameObject,2f);
		SuspendEnemy( true );
	}
}
