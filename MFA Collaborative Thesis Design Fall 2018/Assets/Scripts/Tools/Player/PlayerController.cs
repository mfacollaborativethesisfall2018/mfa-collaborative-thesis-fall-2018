﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	// Author: Glenn Storm
	// Player avatar controller script (move, combat and hud)

	public	GameObject	cameraRoot;
	public	GameObject	combatComboTest;

	public	GameObject	pauseObject;

	public	bool 		performLanding;
	public	float		landingTime;

	public	CombatComboTest	playerCombat;

	public	bool 		lockOnOverride;
	public	GameObject	lockOnTarget;

	public	Transform 	m_Cam;
	public	Vector3 	m_CamForward;
	public	Vector3 	m_Move;

	public	Rigidbody	m_Rigidbody;
	public	Animator	m_Animator;

	public	float		m_StationaryTurnSpeed = 360f;
	public	float		m_MovingTurnSpeed = 180f;

	public	float		m_MoveSpeedMultiplier = 1f;
	public	float		m_AnimSpeedMultiplier = 1f;

	public	float		m_ForwardAmount;
	public	float		m_TurnAmount;

	public	Vector3		m_GroundNormal = Vector3.up;


	void Start () {
		cameraRoot.transform.parent = null;
		combatComboTest.transform.parent = null;

		m_Cam = Camera.main.transform;

		if (performLanding) {
			m_Rigidbody.drag = 0f;
			m_Animator.SetBool ("Enter", true);
			m_Animator.SetBool ("Idle", false);
		}
	}


	// disable (temp)
	void UpdateDisabled () {

		// PAUSE HOOK
		// TODO: more efficient, remove legacy pause
		if (GameObject.FindObjectOfType<PauseManager> () != null) {
			if (GameObject.FindObjectOfType<PauseManager> ().paused)
				return;
		}
		else {
			if (pauseObject.activeSelf)
				return;
		}

		// camera position follow
		cameraRoot.transform.position = gameObject.transform.position;
		lockOnOverride = playerCombat.lockOn.activeSelf;
		if (!lockOnOverride) {
			// mouse look swivel
			Vector3 swiv = Vector3.zero;
			swiv.y = -180f + ((Input.mousePosition.x / Screen.width) * 180f);
			cameraRoot.transform.eulerAngles = swiv;
		}
		else {
			if (playerCombat.combatLockTarget != null) {
				// camera lock on
				Vector3 swiv = Vector3.zero;
				swiv.y = Quaternion.LookRotation( playerCombat.combatLockTarget.transform.position - gameObject.transform.position, Vector3.up ).eulerAngles.y;
				cameraRoot.transform.eulerAngles = swiv;
			}
			else {
				// disengage
				lockOnOverride = false;
				playerCombat.lockOn.SetActive (false);
			}
		}

		// landing bypass
		if (performLanding) {
			landingTime -= Time.deltaTime;
			// FIXME:
			if (landingTime <= 0f) {
				landingTime = 0f;
				performLanding = false;
				m_Rigidbody.drag = 3f;
				m_Animator.SetBool ("Enter", false);
				m_Animator.SetBool ("Idle", true);
			}
			else {
				m_Animator.speed = 0.1f;
				if (landingTime < .51f)
					m_Animator.speed = 1f;
				return;
			}
		}

		// avatar

		float h = 0f; //CrossPlatformInputManager.GetAxis("Horizontal");
		float v = 0f; //CrossPlatformInputManager.GetAxis("Vertical");

		if (Input.GetKey (KeyCode.W))
			v = 1f;
		if (Input.GetKey (KeyCode.S))
			v = -1f;
		if (Input.GetKey (KeyCode.A))
			h = -1f;
		if (Input.GetKey (KeyCode.D))
			h = 1f;

		// calculate camera relative direction to move:
		m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
		m_Move = v*m_CamForward + h*m_Cam.right;

		///

		// ground up vector reset on grounded checks

		// convert the world relative moveInput vector into a local-relative
		// turn amount and forward amount required to head in the desired
		// direction.
		if (m_Move.magnitude > 1f)
			m_Move.Normalize();
		m_Move = transform.InverseTransformDirection(m_Move);
		//CheckGroundStatus();
		m_Move = Vector3.ProjectOnPlane(m_Move, m_GroundNormal);
		m_TurnAmount = Mathf.Atan2(m_Move.x, m_Move.z);
		m_ForwardAmount = m_Move.z;

		// help the character turn faster (this is in addition to root rotation in the animation)
		float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
		transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);

		// update the animator parameters
		if (m_Animator != null) {
			m_Animator.SetFloat ("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
			m_Animator.SetFloat ("Turn", m_TurnAmount, 0.1f, Time.deltaTime);

			if (m_Move.magnitude > 0f)
				m_Animator.speed = m_AnimSpeedMultiplier;
			else
				m_Animator.speed = 1f;
		}

		m_Rigidbody.AddRelativeForce (m_Move, ForceMode.Impulse);
	}
}
