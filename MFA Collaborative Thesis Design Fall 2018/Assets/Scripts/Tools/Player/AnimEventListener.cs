﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimEventListener : MonoBehaviour {

	// Author: Glenn Storm
	// This tool resides on objects with an Animation component and relays animation event signals to the appropriate script

	public	enum ListenerType
	{
		Default,
		PlayerCombat,
		EnemyCombat
	}

	public	ListenerType		listenType;
	public	CombatComboTest		playerCombat;
	public	CombatDummy 		enemyCombat;


	void Start () {
		// validatate
		if (playerCombat == null && enemyCombat == null) {
			Debug.LogWarning ("AnimEventListener [Start] : "+gameObject.name+" no player or enemy combat components referenced. Will act as default listener.");
		}
		// initialize
		if (playerCombat != null && enemyCombat == null) {
			listenType = ListenerType.PlayerCombat;
		} else if (enemyCombat != null && playerCombat == null) {
			listenType = ListenerType.EnemyCombat;
		} else
			listenType = ListenerType.Default;
	}

	/// <summary>
	/// Relays a signal to do damage at the correct time to the appropriate combat script.
	/// </summary>
	/// <param name="attackType">Attack type.</param>
	public	void DoDamage( string attackType ) {
		switch (listenType) {
		case ListenerType.PlayerCombat:
			playerCombat.DoAttackDamage (attackType);
			break;
		case ListenerType.EnemyCombat:
			enemyCombat.DoAttackDamage (attackType);
			break;
		case ListenerType.Default:
			// dynamic props?
			break;
		default:
			// ?
			break;
		}

	}
}
