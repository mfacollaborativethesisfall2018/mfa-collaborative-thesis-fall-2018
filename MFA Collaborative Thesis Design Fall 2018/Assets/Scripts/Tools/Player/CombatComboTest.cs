﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatComboTest : MonoBehaviour {

	// Author: Glenn Storm
	// This tests player input timing in combat combo sequences

	public	enum AttackType
	{
		LightAttack1,
		LightAttack2,
		LightAttack3,
		HeavyAttack,
		Parry
	}

	public	Animator	playerAnimator;
	public	bool 		resetNeeded;

	public	bool 		alternateControls;

	public	GameObject	backMenuObject;
	public	GameObject	painFlash;
	public	GameObject	deathFade;
	public	GameObject	deathSceneEnd;
	public	GameObject	winAudioObject;
	public	GameObject	loseAudioObject;

	public	ProgressBar	healthHPBar;
	public	ProgressBar	energyStaminaBar;

	public	Image 		playerHPRadial;
	public	Image 		playerEnergyRadial;
	public	GameObject	playerEnergyDeco;

	public	GameObject	hudBlock;
	public	GameObject	hudDodge;
	public	GameObject	hudLightAttack;
	public	GameObject	hudHeavyPrep;
	public	GameObject	hudHeavyAttack1;
	public	GameObject	hudHeavyAttack2;
	public	GameObject	hudHeavyAttack3;
	public	GameObject	hudEnergyRecover;

	public	GameObject	enemyHPHUD;
	public	Image 		enemyHPRadial;
	public	RectTransform	enemyHPRectTrans;
	public	RectTransform	enemyFrameRectTrans;
	public	GameObject	lockOnReticle;
	public	Image 		lockOnRectTrans;

	public	GameObject	combatLockTarget;

	public	bool 		playerStunned;
	public	float 		playerStunTime;
	public	bool		playerDodging;
	public	float		playerDodgeTime;
	public	bool 		playerBlocking;
	public	float 		playerBlockTime;

    public	VFXController	vfxController;

	public	ProgressBar	fourBeatBar;

	public	GameObject	lightAttack;
	public	GameObject	heavyAttack;
	public	GameObject	dodge;
	public	GameObject	block;
	public	GameObject	lockOn;
	public	GameObject	pause;

	public	GameObject	cursorHint;

	public	GameObject	lightAttack2;
	public	GameObject	lightAttack3;
	public	GameObject	heavyPrep;
	public	GameObject	perfectBlock;
	public	GameObject	perfectDodge;

	public	GameObject	forward;
	public	GameObject	back;
	public	GameObject	left;
	public	GameObject	right;

	public	float 		beatTime = 0.25f;
	public	int 		beatMax = 4;

	private	AudioManager	am;

	private	bool 		startCombo;
	public	float		currentTime;
	private	float[]		timeMarkers = new float[5];
	private	int 		currentMarker;
	private	Color		savedBarColor;
	private	float		resultTimer;

	private	float 		healthHP = 450f;
	private	float		energyStamina = 3f;
	private	float		combatRange = 5f;
	private	float		combatDamage = 50f;

	const	float 		STARTHEALTH = 450f;
	const	float		LIGHTDAMAGE = 50f;
	const 	float		HEAVYDAMAGE = 100f;
	const	float 		PARRYDAMAGE = .5f; // TODO: ensure proper damage amount
	const 	float		HEAVYPOW1 = 150f;
	const 	float		HEAVYPOW2 = 200f;
	const 	float		HEAVYPOW3 = 300f;
	const	float 		ENERGYMAX = 3f;
	const	float 		PLAYERSTUNMAX = 1f;
	const	float		PLAYERDODGEMAX = .6f;
	const	float		PLAYERBlOCKMAX = 5f;
	const	float 		DODGEFORCE = 50f;
	const	float 		KNOCKBACKFORCE = 0f;
	const	float 		MAXTARGETDISTANCE = 999f;
	const	float 		TARGETANGLETHRESHOLD = 120f;


	void Start () {

		am = GameObject.FindObjectOfType<AudioManager> ();

		fourBeatBar.value = 0f;
		ClearMarkers ();
		savedBarColor = fourBeatBar.barColor;

		healthHP = STARTHEALTH;

		// mouse look = invisible cursor
		Cursor.visible = false;
		cursorHint.SetActive (true);
	}

	void Update () {

		// revised hud objects sync with legacy
		hudBlock.SetActive( block.activeSelf );
		hudDodge.SetActive( dodge.activeSelf );
		hudLightAttack.SetActive( ( lightAttack.activeSelf || lightAttack2.activeSelf || lightAttack3.activeSelf ) );
		if ( hudHeavyPrep != null )
			hudHeavyPrep.SetActive( heavyPrep.activeSelf );
		hudHeavyAttack1.SetActive( heavyAttack.activeSelf );
		if (!heavyAttack.activeSelf) {
			hudHeavyAttack2.SetActive (false);
			hudHeavyAttack3.SetActive (false);
		}
		hudEnergyRecover.SetActive( currentTime > 0f );

		if (GameObject.FindObjectOfType<PauseManager> () == null) { // TODO: more efficient, remove legacy pause
			// PAUSE HOOK
			if ((alternateControls && Input.GetKeyDown (KeyCode.Escape)) || (!alternateControls && Input.GetKeyDown (KeyCode.Escape))) {

				pause.SetActive (!pause.activeSelf);

				// if pause, pause menu with mouse cursor
				// if not, no mouse cursor (then mouse look? if so, reset cam pivot angle X and Y)
				Cursor.visible = (pause.activeSelf);
				cursorHint.SetActive (!pause.activeSelf);

				if ( backMenuObject != null )
					backMenuObject.SetActive (pause.activeSelf);

			}
			if (pause.activeSelf)
				return;
		}

		// RESET HOOK
		if (!resetNeeded)
			ClearAnimFlags ();
		else
			resetNeeded = false;

		// ENTER LANDING HOOK
		if (playerAnimator.GetBool("Enter"))
			return;

		// STUN HOOK
		if (playerStunned) {
			playerStunTime -= Time.deltaTime;
			if (playerStunTime <= 0f) {
				playerStunTime = 0f;
				playerStunned = false;
			}
			else
				return; // stunned = no control
		}

		// DODGE HOOK
		if (playerDodging) {
			playerDodgeTime -= Time.deltaTime;
			if (playerDodgeTime <= 0f) {
				playerDodgeTime = 0f;
				playerDodging = false;
				// Kevin
				if (vfxController.dodgeActivated)
				{
					vfxController.ResetDodgeTimer(PLAYERDODGEMAX);
					vfxController.ToggleDodge();
				}
			}
		}

		// BLOCK HOOK
		if (playerBlocking) {
			playerBlockTime -= Time.deltaTime;
			if (playerBlockTime <= 0f) {
				playerBlockTime = 0f;
				playerBlocking = false;
				playerAnimator.SetBool ("Block", false);
				playerAnimator.SetBool ("Parry", false);
			}
		}

		// LOCK ON TOGGLE
		if ( ( alternateControls && Input.GetKeyDown( KeyCode.E ) ) || ( !alternateControls && Input.GetKeyDown( KeyCode.Q ) ) ) {

			lockOn.SetActive( !lockOn.activeSelf );
			if (lockOn.activeSelf) {
				// acquire target
				CombatDummy dum = FindTarget ();
				if (dum != null) {
					combatLockTarget = dum.gameObject;
				}
			}
			else {
				if ( alternateControls && combatLockTarget != null ) {
					// next lock on target (alt controls)
					combatLockTarget = FindNextTarget (combatLockTarget.GetComponent<CombatDummy> ()).gameObject;
				}
				else {
					// lose target
					combatLockTarget = null;
				}
			}
		}

		// NEXT LOCK ON (normal controls)
		if ( !alternateControls && Input.GetMouseButtonDown(2) ) {
			CombatDummy cd = null;
			if (combatLockTarget != null)
				cd = FindNextTarget (combatLockTarget.GetComponent<CombatDummy> ());
			else
				cd = FindTarget ();
			if ( cd != null )
				combatLockTarget = cd.gameObject;
		}

		// cancel target check
		if (combatLockTarget != null) {
			if (combatLockTarget.GetComponent<CombatDummy> ().IsDead ()) {
				combatLockTarget = null;
				lockOn.SetActive (false);
			}
			else {
				// update enemy hud elements
				enemyHPRadial.fillAmount = combatLockTarget.GetComponent<CombatDummy> ().HealthPct ();
				enemyHPHUD.gameObject.SetActive (true);
				enemyHPRectTrans.gameObject.SetActive (true);
				lockOnReticle.gameObject.SetActive (true);
				// anchored position adjustment based on screen position
				Vector2 ap = Vector2.zero;
				Vector3 lPos = Camera.main.WorldToScreenPoint (combatLockTarget.transform.position + (Vector3.up * 3f));
				ap.x = (lPos.x) - (Screen.width/2);
				ap.y = (lPos.y) - (Screen.height/2);
				enemyHPRectTrans.anchoredPosition = ap;
				enemyFrameRectTrans.anchoredPosition = ap;
			}
		}
		else {
			// deactivate enemy hud elements
			enemyHPRadial.fillAmount = 1f;
			enemyHPHUD.gameObject.SetActive (false);
			enemyHPRectTrans.gameObject.SetActive (false);
			lockOnReticle.gameObject.SetActive (false);
			Vector2 ap = Vector2.zero;
			enemyHPRectTrans.anchoredPosition = ap;
			enemyFrameRectTrans.anchoredPosition = ap;
		}

		// movement
		forward.SetActive( Input.GetKey( KeyCode.W ) );
		back.SetActive( Input.GetKey( KeyCode.S ) );
		left.SetActive( Input.GetKey( KeyCode.A ) );
		right.SetActive( Input.GetKey( KeyCode.D ) );

		if (resultTimer > 0f)
			RunResultTimer ();

		if (startCombo) {

			currentTime += Time.deltaTime;
			fourBeatBar.value = 1f - ( currentTime / ( beatTime * beatMax ) );
			if ( currentTime > (beatTime * beatMax) ) {
				currentTime = 0f;
				fourBeatBar.value = 0f;
				ClearMarkers ();
				EndCombo ();
				startCombo = false;
			}

			int thisBeat = Mathf.RoundToInt( ( currentTime / beatTime ) + 0.5f );
            //Debug.Log("Player's beat is: " + ((currentTime/beatTime) + 0.5f));

			// ui time indicator
			Color c = savedBarColor;
			c.r = ((float)thisBeat / beatMax);
			//c.g = ((float)thisBeat / beatMax);
			c.b = 1f - ((float)thisBeat / beatMax);
			fourBeatBar.barColor = c;

			// WARNING: (only w/ mouse pad) mouse button -> key press works, key press -> mouse button does not

			// heavy attack prep off
			if (thisBeat > 2 && heavyAttack.activeSelf && heavyPrep.activeSelf) {
				heavyPrep.SetActive (false);
				// Attack performed via anim event
				//Attack (AttackType.HeavyAttack, HEAVYDAMAGE, FindTarget ());
			}
			// perfect block off
			else if ( thisBeat > 2 && block.activeSelf && perfectBlock.activeSelf)
				perfectBlock.SetActive (false);
			// perfect dodge off
			else if ( thisBeat > 1 && dodge.activeSelf && perfectDodge.activeSelf)
				perfectDodge.SetActive (false);

			// end combat scene
			CombatDummy[]	dums = GameObject.FindObjectsOfType<CombatDummy>();
			int dumCount = 0;
			foreach (CombatDummy cd in dums) {
				if (cd.HealthPct() > 0f)
					dumCount++;
			}
			if (dumCount == 0) {
				playerAnimator.SetBool ("Win", true);
				resetNeeded = true;
				if (winAudioObject != null)
					winAudioObject.SetActive (true);
				if (deathSceneEnd != null)
					deathSceneEnd.SetActive (true);
			}

			// attack result (end)

			if ( ( alternateControls && Input.GetKeyDown( KeyCode.J ) ) || ( !alternateControls && Input.GetMouseButtonDown (0) ) ) {

				if (lightAttack.activeSelf && thisBeat == 3 && !lightAttack2.activeSelf) {
					//print ("light light at beat "+thisBeat);
					lightAttack2.SetActive (true);
					hudLightAttack.SetActive (true);
					// Attack performed via anim event
					//Attack (AttackType.LightAttack2, LIGHTDAMAGE, FindTarget ());
					playerAnimator.SetBool("Light Attack 2", true);
					resetNeeded = true;
					currentTime = 0f;
					fourBeatBar.value = 1f;
					beatMax = 4;
				}
				else if (lightAttack.activeSelf && thisBeat == 3 && !lightAttack3.activeSelf) {
					//print ("light light light at beat "+thisBeat);
					lightAttack3.SetActive (true);
					hudLightAttack.SetActive (true);
					// Attack performed via anim event
					//Attack (AttackType.LightAttack3, LIGHTDAMAGE, FindTarget ());
					playerAnimator.SetBool("Light Attack 3", true);
					resetNeeded = true;
					currentTime = 0f;
					fourBeatBar.value = 1f;
					beatMax = 4;
				}
				else if (heavyAttack.activeSelf) {
					//print ("heavy light at beat "+thisBeat);
				}
				else if (block.activeSelf) {
					//print ("block light at beat "+thisBeat);
				}
				else if (dodge.activeSelf) {
					//print ("dodge light at beat "+thisBeat);
				}

			}

			if ( ( alternateControls && Input.GetKeyDown( KeyCode.I ) ) || ( !alternateControls && Input.GetMouseButtonDown (1) ) ) {

				if (lightAttack.activeSelf && thisBeat == 3 && !lightAttack2.activeSelf ) {
					//print ("light heavy at beat "+thisBeat);
					playerAnimator.SetBool("Heavy Attack 2", true);
					resetNeeded = true;
					heavyAttack.SetActive (true);
					hudHeavyAttack2.SetActive (true);
					heavyPrep.SetActive (true);
					if (hudHeavyPrep != null)
						hudHeavyPrep.SetActive (true);
					currentTime = 0f;
					fourBeatBar.value = 1f;
					beatMax = 4;

					vfxController.ResetPlayerHeavyTimer(1.5f);
					vfxController.ActivateHeavyTrail();
				}
				else if (lightAttack.activeSelf && thisBeat == 3 && !lightAttack3.activeSelf ) {
					//print ("light light heavy at beat "+thisBeat);
					playerAnimator.SetBool("Heavy Attack 3", true);
					resetNeeded = true;
					heavyAttack.SetActive (true);
					hudHeavyAttack3.SetActive (true);
					heavyPrep.SetActive (true);
					if (hudHeavyPrep != null)
						hudHeavyPrep.SetActive (true);
					currentTime = 0f;
					fourBeatBar.value = 1f;
					beatMax = 4;

					vfxController.ResetPlayerHeavyTimer(1.5f);
					vfxController.ActivateHeavyTrail();
				}
				else if (heavyAttack.activeSelf) {
					//print ("heavy heavy at beat "+thisBeat);
				}
				else if (block.activeSelf) {
					//print ("block heavy at beat "+thisBeat);
				}
				else if (dodge.activeSelf) {
					//print ("dodge heavy at beat "+thisBeat);
				}

			}

			if ( ( alternateControls && Input.GetKeyDown( KeyCode.L ) ) || ( !alternateControls && Input.GetKeyDown (KeyCode.E) ) ) {

				if (lightAttack.activeSelf && thisBeat == 3 && !lightAttack2.activeSelf ) {
					//print ("light block at beat "+thisBeat);
					block.SetActive (true);
					hudBlock.SetActive(true);
					playerAnimator.SetBool ("Block", true);
					perfectBlock.SetActive (true);
					currentTime = 0f;
					fourBeatBar.value = 1f;
					beatMax = 6;
				}
				else if (lightAttack.activeSelf && thisBeat == 3 && !lightAttack3.activeSelf ) {
					//print ("light light block at beat "+thisBeat);
					block.SetActive (true);
					hudBlock.SetActive(true);
					playerAnimator.SetBool ("Block", true);
					perfectBlock.SetActive (true);
					currentTime = 0f;
					fourBeatBar.value = 1f;
					beatMax = 6;
				}
				else if (heavyAttack.activeSelf && thisBeat == 2) {
					//print ("heavy block at beat "+thisBeat);
					block.SetActive (true);
					hudBlock.SetActive(true);
					playerAnimator.SetBool ("Block", true);
					perfectBlock.SetActive (true);
					currentTime = 0f;
					fourBeatBar.value = 1f;
					beatMax = 6;
				}
				else if (block.activeSelf) {
					//print ("block block at beat "+thisBeat);
				}
				else if (dodge.activeSelf) {
					//print ("dodge block at beat "+thisBeat);
				}

			}

			if ( ( alternateControls && Input.GetKeyDown( KeyCode.K ) ) || ( !alternateControls && Input.GetKeyDown( KeyCode.Space ) ) ) {

				if (lightAttack.activeSelf && thisBeat == 3 && !lightAttack2.activeSelf ) {
					//print ("light dodge at beat "+thisBeat);
					dodge.SetActive (true);
					hudDodge.SetActive (true);
					perfectDodge.SetActive (true);
					currentTime = 0f;
					fourBeatBar.value = 1f;
					beatMax = 4;
				}
				else if (lightAttack.activeSelf && thisBeat == 3 && !lightAttack3.activeSelf ) {
					//print ("light light dodge at beat "+thisBeat);
					dodge.SetActive (true);
					hudDodge.SetActive (true);
					perfectDodge.SetActive (true);
					currentTime = 0f;
					fourBeatBar.value = 1f;
					beatMax = 4;
				}
				else if (heavyAttack.activeSelf && thisBeat == 2) {
					//print ("heavy dodge at beat "+thisBeat);
					dodge.SetActive (true);
					hudDodge.SetActive (true);
					perfectDodge.SetActive (true);
					currentTime = 0f;
					fourBeatBar.value = 1f;
					beatMax = 4;
				}
				else if (block.activeSelf) {
					//print ("block dodge at beat "+thisBeat);
				}
				else if (dodge.activeSelf) {
					//print ("dodge dodge at beat "+thisBeat);
				}
					
			}

		}
		else {

			if ( ( alternateControls && Input.GetKeyDown( KeyCode.J ) ) || ( !alternateControls && Input.GetMouseButtonDown (0) ) ) {

				lightAttack.SetActive (true);
				hudLightAttack.SetActive (true);
				playerAnimator.SetBool ("Light Attack 1", true);
				resetNeeded = true;
				// Attack performed via anim event
				//Attack (AttackType.LightAttack1, LIGHTDAMAGE, FindTarget ());
				startCombo = true;
				SetMarker (currentMarker);
				beatMax = 4;

			}

			if ( ( alternateControls && Input.GetKeyDown( KeyCode.I ) ) || ( !alternateControls && Input.GetMouseButtonDown (1) ) ) {

				heavyAttack.SetActive (true);
				hudHeavyAttack1.SetActive (true);
				playerAnimator.SetBool ("Heavy Attack 1", true);
				resetNeeded = true;
				startCombo = true;
				SetMarker (currentMarker);

				heavyPrep.SetActive( true );
				beatMax = 4;

				vfxController.ResetPlayerHeavyTimer(1.5f);
				vfxController.ActivateHeavyTrail();

			}

			if ( ( alternateControls && Input.GetKeyDown( KeyCode.L ) ) || ( !alternateControls && Input.GetKeyDown (KeyCode.E) ) ) {

				block.SetActive (true);
				hudBlock.SetActive (true);
				playerAnimator.SetBool ("Block", true);
				startCombo = true;
				SetMarker (currentMarker);

				perfectBlock.SetActive (true);
				beatMax = 6;

				playerBlocking = true;
				playerDodgeTime = PLAYERBlOCKMAX;

			}

			if ( ( alternateControls && Input.GetKeyDown( KeyCode.K ) ) || ( !alternateControls && Input.GetKeyDown (KeyCode.Space) ) ) {

				dodge.SetActive (true);
				hudDodge.SetActive (true);
				startCombo = true;
				SetMarker (currentMarker);

				perfectDodge.SetActive (true);
				beatMax = 4;

				playerDodging = true;
				EnergyDrain ();
				playerDodgeTime = PLAYERDODGEMAX;

				// FIXME: more efficient
				// FIXME: dodge in direction based on camera?
				// FIXME: dodge only if locked on?
				Rigidbody rb = GameObject.FindObjectOfType<CodeStore> ().gameObject.GetComponent<Rigidbody>();
				// DODGE MOVEMENT
				if (Input.GetKey (KeyCode.W)) {
					// forward
					rb.AddForce( rb.gameObject.transform.forward * DODGEFORCE, ForceMode.Impulse );
					playerAnimator.SetBool ("Dash Fwd", true);
					resetNeeded = true;
				}
				else if (Input.GetKey (KeyCode.A)) {
					// left
					rb.AddForce( rb.gameObject.transform.right * -DODGEFORCE, ForceMode.Impulse );
					playerAnimator.SetBool ("Dash Left", true);
					resetNeeded = true;
				}
				else if (Input.GetKey (KeyCode.D)) {
					// right
					rb.AddForce( rb.gameObject.transform.right * DODGEFORCE, ForceMode.Impulse );
					playerAnimator.SetBool ("Dash Right", true);
					resetNeeded = true;
				}
				else {
					// back
					rb.AddForce( rb.gameObject.transform.forward * -DODGEFORCE, ForceMode.Impulse );
					playerAnimator.SetBool ("Dash Back", true);
					resetNeeded = true;
				}

				//vfxController.dodgeActivated = true;
				// Kevin
				vfxController.ResetDodgeTimer(PLAYERDODGEMAX);
				vfxController.ToggleDodge();

			}
		
		}

	}

    /// <summary>
    /// Kevin : Used to set the playerStunTime to PLAYERSTUNMAX when accessing from outside this script
    /// Currently used for when a heavy enemy stuns the player via a parry
    /// </summary>
    public void ResetPlayerStunTimer()
    {
        playerStunTime = PLAYERSTUNMAX;
    }

	void SetMarker( int thisMarker ) {

		if ( thisMarker > (beatMax-1) )
			return;
		timeMarkers [thisMarker] = currentTime;
		currentMarker++;

		// TODO: Use time markers to determine player skill in combo timing
		// necessary?

	}

	void ClearMarkers() {

		for (int i = 0; i < 5; i++) {
			timeMarkers [i] = 0f;
		}
		currentMarker = 0;

	}

	void EndCombo() {

		resultTimer = .25f;
		beatMax = 4;

		hudEnergyRecover.SetActive( false );
	}

	void RunResultTimer() {

		resultTimer -= Time.deltaTime;
		if ( resultTimer <= 0f ) {
			resultTimer = 0f;
			EndResultDisplay ();
		}
		Color c = savedBarColor;
		// FIXME: make better
		c.r = (resultTimer * 8) % 4f;
		c.g = (resultTimer * 8) % 4f;
		lightAttack.GetComponent<TextMesh> ().color = c;
		heavyAttack.GetComponent<TextMesh> ().color = c;
		dodge.GetComponent<TextMesh> ().color = c;
		block.GetComponent<TextMesh> ().color = c;
		lightAttack2.GetComponent<TextMesh> ().color = c;
		lightAttack3.GetComponent<TextMesh> ().color = c;
		heavyPrep.GetComponent<TextMesh> ().color = c;
		perfectBlock.GetComponent<TextMesh> ().color = c;
		perfectDodge.GetComponent<TextMesh> ().color = c;

	}

	void EndResultDisplay() {

		lightAttack.SetActive( false );
		heavyAttack.SetActive( false );
		dodge.SetActive( false );
		block.SetActive( false );
		// lockOn.SetActive( false );  // not effected unless unlocked, relocked or enemy dead ?
		pause.SetActive( false );
		lightAttack2.SetActive (false);
		lightAttack3.SetActive (false);
		heavyPrep.SetActive (false);
		perfectBlock.SetActive (false);
		perfectDodge.SetActive (false);

	}

	public	void DoParry( CombatDummy dum ) {
		// Attack performed via anim event?
		//Attack (AttackType.Parry, PARRYDAMAGE, dum);
		playerAnimator.SetBool("Parry", true);
		resetNeeded = true;
	}

	public	void DoAnim( string flag, bool reset ) {
		playerAnimator.SetBool( flag, true );
		if (reset)
			resetNeeded = true;
	}

	public	void DoAttackDamage( string attackType ) {
		// called from AnimEventListener to perform attack at correct anim time
		switch (attackType) {
		case "LightAttack1":
			Attack(AttackType.LightAttack1, LIGHTDAMAGE, FindTarget ());
			break;
		case "LightAttack2":
			Attack(AttackType.LightAttack2, LIGHTDAMAGE, FindTarget ());
			break;
		case "LightAttack3":
			Attack(AttackType.LightAttack3, LIGHTDAMAGE, FindTarget ());
			break;
		case "HeavyAttack":
			Attack(AttackType.HeavyAttack, HEAVYDAMAGE, FindTarget ());
			break;
		case "Parry":
			Attack(AttackType.Parry, PARRYDAMAGE, FindTarget ());
			break;
		}
	}

	void ClearAnimFlags() {
		// Clear Dash Flags
		playerAnimator.SetBool("Dash Fwd", false);
		playerAnimator.SetBool("Dash Back", false);
		playerAnimator.SetBool("Dash Left", false);
		playerAnimator.SetBool("Dash Right", false);
		// Clear Parry Flag
		playerAnimator.SetBool("Parry", false);
		// Clear Hit Flags
		playerAnimator.SetBool("Hit Fwd", false);
		playerAnimator.SetBool("Hit Back", false);
		playerAnimator.SetBool("Hit Left", false);
		playerAnimator.SetBool("Hit Right", false);
		// Win flag
		playerAnimator.SetBool("Win", false);
		// Attacks
		playerAnimator.SetBool("Light Attack 1", false);
		playerAnimator.SetBool("Light Attack 2", false);
		playerAnimator.SetBool("Light Attack 3", false);
		playerAnimator.SetBool("Heavy Attack 1", false);
		playerAnimator.SetBool("Heavy Attack 2", false);
		playerAnimator.SetBool("Heavy Attack 3", false);
	}

	public	void TakeDamage( float dam ) {

		// Player cannot be hit while dodging
		if (playerDodging)
			return;

		// TODO: input direction of hit
		playerAnimator.SetBool ("Hit Fwd", true);
		resetNeeded = true;

		healthHP -= dam;
		painFlash.SetActive (true);
		if (healthHP <= 0f) {
			healthHP = 0f;
			// boom!
			playerAnimator.SetBool("Dead", true);
			vfxController.ActivateDeath ();
			if ( loseAudioObject != null )
				loseAudioObject.SetActive (true);
			deathFade.SetActive (true);
			if ( deathSceneEnd != null )
				deathSceneEnd.SetActive (true);
		}
		else {
			vfxController.ResetPlayerHitTimer (1.5f);
			vfxController.ActivatePlayerHit ();
		}
		healthHPBar.value = ( healthHP / 450f );
		playerHPRadial.fillAmount = ( healthHP / 450f );

	}

	public	bool	IsDead() {
		return (healthHP <= 0f);
	}

	/// <summary>
	/// Player recovers one energy point upon successful light attack hit.
	/// </summary>
	public void EnergyRecover() {
		//print ("energy recover called.");
		// light attack adds one energy upon hit
		energyStamina += 1f;
		hudEnergyRecover.SetActive (true);
		// clamp energy
		energyStamina = Mathf.Clamp(energyStamina,0f,3f);
		// adjust energy display
		energyStaminaBar.value = ( energyStamina / ENERGYMAX );
		playerEnergyRadial.fillAmount = ( energyStamina / ENERGYMAX );
	}

	void EnergyDrain() {
		// dodge drains one energy
		energyStamina -= 1f;
		// clamp energy
		energyStamina = Mathf.Clamp(energyStamina,0f,3f);
		// adjust energy display
		energyStaminaBar.value = ( energyStamina / ENERGYMAX );
		playerEnergyRadial.fillAmount = ( energyStamina / ENERGYMAX );
	}

	void Attack( AttackType type, float dam, CombatDummy target ) {
		if (target == null)
			return;
		float dmg = combatDamage;
		switch (type) {
		case AttackType.LightAttack1:
				if (am != null)
					am.PlayAudio ("SFX Light Attack 1");
				break;
			case AttackType.LightAttack2:
				if (am != null)
					am.PlayAudio ("SFX Light Attack 2");
				break;
			case AttackType.LightAttack3:
				if (am != null)
					am.PlayAudio ("SFX Light Attack 3");
				break;
			case AttackType.HeavyAttack:
				if (am != null)
					am.PlayAudio ("SFX Heavy Swing");
				// adjust heavy dmg based on energy
				if (energyStamina < 1f)
					dmg *= 2f;
				else if (energyStamina < 2f)
					dmg *= 3f;
				else if (energyStamina < 3f)
					dmg *= 4f;
				else if (energyStamina == 3f)
					dmg *= 6f;
				// heavy attack drains all energy
				energyStamina = 0f;
				break;
		case AttackType.Parry:
				if (am != null)
					am.PlayAudio ("SFX Perfect Block Parry");
				vfxController.ActivateParry ();
				playerAnimator.SetBool ("Parry", true);
				// need reset?
				break;
		}
		// clamp energy
		energyStamina = Mathf.Clamp(energyStamina,0f,3f);
		// adjust energy display
		energyStaminaBar.value = ( energyStamina / ENERGYMAX );
		playerEnergyRadial.fillAmount = ( energyStamina / ENERGYMAX );
		Transform myTrans = GameObject.FindObjectOfType<CodeStore> ().gameObject.transform;
		float d = Vector3.Distance (myTrans.position, target.gameObject.transform.position);
		if (d <= combatRange) {
			target.TakeDamage (dmg);
			target.gameObject.GetComponent<Rigidbody> ().AddForce (Knockback (dmg, myTrans.position, target.transform.position), ForceMode.Impulse);
			// attack hit vfx
			switch (type) {
				case AttackType.LightAttack1:
					if (am != null)
						am.PlayAudio ("SFX Light Impact");
					EnergyRecover ();
					break;
				case AttackType.LightAttack2:
					if ( am!=null )
						am.PlayAudio ("SFX Light Impact");
					EnergyRecover ();
					break;
				case AttackType.LightAttack3:
					if ( am!=null )
						am.PlayAudio ("SFX Light Impact");
					EnergyRecover ();
					break;
				case AttackType.HeavyAttack:
					if ( am!=null )
						am.PlayAudio ("SFX Heavy Attack Impact");
					break;
				case AttackType.Parry:
					if ( am!=null )
						am.PlayAudio ("SFX Light Impact");
					break;
			}
		}
	}

	public Vector3 Knockback( float pow, Vector3 attacker, Vector3 target ) {

		// Player cannot be hit while dodging
		if (playerDodging)
			return Vector3.zero;
		
		Vector3 knock = (target - attacker);
		knock.Normalize ();
		knock *= pow * KNOCKBACKFORCE;
		return knock;
	}

	CombatDummy FindTarget() {

		CombatDummy thisDum = null;
		float dist = MAXTARGETDISTANCE;
		float angl = TARGETANGLETHRESHOLD;
		CombatDummy[] dums = GameObject.FindObjectsOfType<CombatDummy> ();
		Transform myTrans = GameObject.FindObjectOfType<CodeStore> ().gameObject.transform;
		foreach (CombatDummy d in dums) {
			float ang = Vector3.Angle(d.gameObject.transform.position - myTrans.position,myTrans.forward);
			float dst = Vector3.Distance (myTrans.position, d.gameObject.transform.position);
			if (!d.enemySuspended && ang < angl) {
				if (dst < dist) {
					dist = dst;
					thisDum = d;
				}
			}
		}
		return thisDum;
	}

	CombatDummy FindNextTarget( CombatDummy currentTarget ) {

		if ( currentTarget == null )
			return FindTarget ();
		CombatDummy thisDum = currentTarget;
		float dist = MAXTARGETDISTANCE;
		float angl = TARGETANGLETHRESHOLD;
		CombatDummy[] dums = GameObject.FindObjectsOfType<CombatDummy> ();
		if (dums.Length < 2)
			return thisDum;
		Transform myTrans = GameObject.FindObjectOfType<CodeStore> ().gameObject.transform;
		float dumDist = Vector3.Distance (currentTarget.transform.position, myTrans.position);
		CombatDummy foundDum = null;
		foreach (CombatDummy d in dums) {
			float ang = Vector3.Angle(d.gameObject.transform.position - myTrans.position,myTrans.forward);
			float dst = Vector3.Distance (myTrans.position, d.gameObject.transform.position);
			if (!d.enemySuspended && ang < angl) {
				if (dst < dist && d != currentTarget) {
					dist = dst;
					thisDum = d;
				}
				if (dst < dist && d != currentTarget && dst > dumDist) {
					dist = dst;
					foundDum = d;
				}
			}
		}
		if ( foundDum != null )
			thisDum = foundDum;
		return thisDum;
	}
}
