﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeStore : MonoBehaviour {

	// placeholder code from third person controller and third person character

	public	GameObject	pauseObject;

	public	bool 		performLanding;
	public	float		landingTime;

	public	CombatComboTest	playerCombat;

	public	GameObject	cameraRoot;
	public	bool 		lockOnOverride;
	public	GameObject	lockOnTarget;

	public	Transform 	m_Cam;
	public	Vector3 	m_CamForward;
	public	Vector3 	m_Move;

	public	Rigidbody	m_Rigidbody;
	public	Animator	m_Animator;

	public	float		m_StationaryTurnSpeed = 360f;
	public	float		m_MovingTurnSpeed = 180f;

	public	float		m_MoveSpeedMultiplier = 1f;
	public	float		m_AnimSpeedMultiplier = 1f;

	public	float		m_ForwardAmount;
	public	float		m_TurnAmount;

	public	Vector3		m_GroundNormal = Vector3.up;

	public	bool		tutorialCamLock;

	const	float 		CAMERASWIVELRATE = 10f;


	// Use this for initialization
	void Start () {

		m_Cam = Camera.main.transform;

		if (performLanding) {
			m_Rigidbody.drag = 0f;
			m_Animator.SetBool ("Enter", true);
			m_Animator.SetBool ("Idle", false);
		}
	}

	public void OnAnimatorMove()
	{
		// we implement this function to override the default root motion.
		// this allows us to modify the positional speed before it's applied.
		if (Time.deltaTime > 0)
		{
			Vector3 v = (m_Animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

			// we preserve the existing y part of the current velocity.
			v.y = m_Rigidbody.velocity.y;
			m_Rigidbody.velocity = v;
		}
	}
	
	// Update is called once per frame
	void Update () {

		// PAUSE HOOK
		// TODO: more efficient, remove legacy pause
		if (GameObject.FindObjectOfType<PauseManager> () != null) {
			if (GameObject.FindObjectOfType<PauseManager> ().paused)
				return;
		}
		else {
			if (pauseObject.activeSelf)
				return;
		}

		// camera position follow
		cameraRoot.transform.position = gameObject.transform.position;
		lockOnOverride = playerCombat.combatLockTarget != null;
		m_Animator.SetBool ("LockOn", lockOnOverride);
		if (!lockOnOverride) {
			// mouse look swivel
			Vector3 swiv = cameraRoot.transform.eulerAngles;
			if (!tutorialCamLock)
				swiv.y += ( CAMERASWIVELRATE * Input.GetAxisRaw ("Mouse X") );
			cameraRoot.transform.eulerAngles = swiv;
		}
		else {
			if (playerCombat.combatLockTarget != null) {
				// camera lock on
				Vector3 swiv = Vector3.zero;
				swiv.y = Quaternion.LookRotation( playerCombat.combatLockTarget.transform.position - gameObject.transform.position, Vector3.up ).eulerAngles.y;
				cameraRoot.transform.eulerAngles = swiv;
			}
			else {
				// disengage
				lockOnOverride = false;
				playerCombat.lockOn.SetActive (false);
			}
		}

		// landing bypass
		if (performLanding) {
			landingTime -= Time.deltaTime;
			// FIXME:
			if (landingTime <= 0f) {
				landingTime = 0f;
				performLanding = false;
				m_Rigidbody.drag = 3f;
				m_Animator.SetBool ("Enter", false);
				m_Animator.SetBool ("Idle", true);
			}
			else {
				m_Animator.speed = 0.1f;
				if (landingTime < .51f)
					m_Animator.speed = 1f;
				return;
			}
		}
			
		// avatar

		float h = 0f; //CrossPlatformInputManager.GetAxis("Horizontal");
		float v = 0f; //CrossPlatformInputManager.GetAxis("Vertical");

		if (Input.GetKey (KeyCode.W))
			v = 1f;
		if (Input.GetKey (KeyCode.S))
			v = -1f;
		if (Input.GetKey (KeyCode.A))
			h = -1f;
		if (Input.GetKey (KeyCode.D))
			h = 1f;

		// calculate camera relative direction to move:
		m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
		m_Move = v*m_CamForward + h*m_Cam.right;

		// ground up vector reset on grounded checks

		// convert the world relative moveInput vector into a local-relative
		// turn amount and forward amount required to head in the desired
		// direction.
		if (m_Move.magnitude > 1f)
			m_Move.Normalize();
		m_Move = transform.InverseTransformDirection(m_Move);
		//CheckGroundStatus();
		m_Move = Vector3.ProjectOnPlane(m_Move, m_GroundNormal);

		float m_Sidestep = m_Move.x;

		if (!lockOnOverride) {
			m_TurnAmount = Mathf.Atan2 (m_Move.x, m_Move.z);
			m_ForwardAmount = m_Move.z;

			// help the character turn faster (this is in addition to root rotation in the animation)
			float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
			transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
		}
		else {
			m_TurnAmount = 0f;
			m_ForwardAmount = m_Move.z;

			// turn toward target
			Vector3 pPos = transform.position;
			Vector3 ePos = playerCombat.combatLockTarget.transform.position;
			pPos.y = 0f;
			ePos.y = 0f;
			Vector3 lockRot = (ePos - pPos);
			float	angTurn = Vector3.SignedAngle(transform.forward,lockRot,Vector3.up);
			float	deltaTurn = m_StationaryTurnSpeed * Time.deltaTime;
			angTurn = Mathf.Clamp (angTurn, -deltaTurn, deltaTurn);
			transform.Rotate(Vector3.up, angTurn);
		}

		// update the animator parameters
		if (m_Animator != null) {
			m_Animator.SetFloat ("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
			m_Animator.SetFloat ("Turn", m_TurnAmount, 0.1f, Time.deltaTime);
			m_Animator.SetFloat ("Sidestep", m_Sidestep, 0.1f, Time.deltaTime);

			if (m_Move.magnitude > 0f)
				m_Animator.speed = m_AnimSpeedMultiplier;
			else
				m_Animator.speed = 1f;
		}

		m_Rigidbody.AddRelativeForce (m_Move, ForceMode.Impulse);
	}
}
