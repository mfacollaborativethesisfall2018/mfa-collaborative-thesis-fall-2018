﻿using UnityEngine;
using System.Collections;

public class AudioTrigger : MonoBehaviour {

	// Author: Glenn Storm
	// Audio trigger to use outside of game (no game manager)

	public	string			soundName;
	public	float			soundDelay;
	public	bool			noRetrigger;
	public	bool			musicFadeOut;
	public	bool			resetOnComplete;

	private	AudioManager	am;
	private bool			logging;
	private float			delayTime;


	void Awake () {
	
		logging = Debug.isDebugBuild;
	}

	void Start () {

		am = GameObject.FindObjectOfType<AudioManager>();
		if ( am == null ) {
			if ( logging )
				Debug.LogError("--- Audio Trigger [Start] : no audio manager found. aborting.");
			enabled = false;
		}
		if ( soundName == "" && !musicFadeOut ) {
			if ( logging )
				Debug.LogError("--- Audio Trigger [Start] : no sound name. aborting.");
			enabled = false;
		}
		if ( soundDelay < 0f ) {
			if ( logging )
				Debug.LogWarning("--- Audio Trigger [Start] : sound delay is less than zero. will ignore.");
		}
		else
			delayTime = soundDelay;
	}

	void Update () {
	
		delayTime -= Time.deltaTime;
		if ( delayTime <= 0f ) {
			if ( am != null ) {
				if ( musicFadeOut )
					am.FadeOutMusicLoops();
				if ( !noRetrigger || !am.IsPlaying( soundName ) )
					am.PlayAudio( soundName );
				else if ( am.IsFadingOut( soundName ) ) {
					for ( int i=0; i<am.fades.Length; i++ ) {
						if ( am.fades[i].sourceIndex < am.sources.Length && am.sources[am.fades[i].sourceIndex].clip.name == soundName )
							am.fades[i].fadeIn = true;
					}
				}
			}
			enabled = resetOnComplete;
			if ( resetOnComplete ) {
				delayTime = soundDelay;
				gameObject.SetActive(false);
			}
		}
	}
}
