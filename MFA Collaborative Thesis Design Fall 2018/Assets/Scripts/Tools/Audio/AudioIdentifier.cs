﻿using UnityEngine;
using System.Collections;

public class AudioIdentifier : MonoBehaviour {

	// Author: Glenn Storm
	// This identifies an object by its type of audio, and is accessed by Options Manager for purposes of adjusting audio outside of the main game

	public enum menuAudioType {
		SFX,
		Music,
		Voice
	}
	public	menuAudioType	audioType;
	public	bool			optionsSynced;

	private	float			managedVolume;
	private bool			isAtMusicManager;


	void Start() {

		isAtMusicManager = ( gameObject.GetComponent<at_MusicManager>() != null );
	}

	void LateUpdate() {

		if ( optionsSynced ) {
			// find all audio sources on this object and adjust volume to managed volume
			AudioSource[] a = GetComponents<AudioSource>();
			for ( int i=0; i<a.Length; i++ ) {
				if ( isAtMusicManager && managedVolume > 0f && ( GetComponent<at_MusicManager>().fader != 0f && GetComponent<at_MusicManager>().fader != 1f ) ) {
					// TODO: handle 'chirp' on end of fade, where source is about to be muted
					a[i].volume = Mathf.Min( managedVolume, a[i].volume );
				}
				else
					a[i].volume = managedVolume;
			}
		}
	}

	public void SetManagedVolume( float volume ) {

		managedVolume = volume;
	}
}
