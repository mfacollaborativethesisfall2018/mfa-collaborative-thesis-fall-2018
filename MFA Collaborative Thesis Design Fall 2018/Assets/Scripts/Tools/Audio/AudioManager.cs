﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	// Author: Glenn Storm
	// A dynamic audio source manager for sound effects, music and voice

	GameManager				gm;

	public	enum aType
	{
		Invalid,
		Default,
		SFXUI,
		SFXGame,
		MusicSting,
		MusicLoop,
		Voice
	}
	public	struct AudioMap
	{
		public	string		name;
		public	aType		type;
	}
	public	struct Fade
	{
		public	string		fadeName;
		public	int			sourceIndex;
		public	bool		fadeIn;
		public	float		fadeTime;
		public	float		totalTime;
		public	float		maxVolume;
	}

	public	AudioMap[]		sounds = new AudioMap[0];
	public	AudioClip[]		clips = new AudioClip[0];
	public	float			defaultVolume = 1f;
	public	float			sfxVolume = 1f;
	public	float			musicVolume = 1f;
	public	float			voiceVolume = 1f;
	public	AudioSource[] 	sources = new AudioSource[0];
	public	Fade[]			fades = new Fade[0];

	private bool			detectedPaused;
	private bool			detectedFocus;
	private string			lastMusicRequested;
	private float			musicCheckTime;


	void Start () {

		// validate
		if ( defaultVolume < 0f || defaultVolume > 1f ) {
			Debug.LogWarning("--- AudioManager [Start] : Default Volume is invalid. will clamp to value between 0 and 1.");
			defaultVolume = Mathf.Clamp( defaultVolume, 0f, 1f );
		}
		if ( sfxVolume < 0f || sfxVolume > 1f ) {
			Debug.LogWarning("--- AudioManager [Start] : Sfx Volume is invalid. will clamp to value between 0 and 1.");
			sfxVolume = Mathf.Clamp( sfxVolume, 0f, 1f );
		}
		if ( musicVolume < 0f || musicVolume > 1f ) {
			Debug.LogWarning("--- AudioManager [Start] : Music Volume is invalid. will clamp to value between 0 and 1.");
			musicVolume = Mathf.Clamp( musicVolume, 0f, 1f );
		}
		if ( voiceVolume < 0f || voiceVolume > 1f ) {
			Debug.LogWarning("--- AudioManager [Start] : Voice Volume is invalid. will clamp to value between 0 and 1.");
			voiceVolume = Mathf.Clamp( voiceVolume, 0f, 1f );
		}
		if ( sources.Length > 0 ) {
			Debug.LogWarning("--- AudioManager [Start] : Sources array is not empty. will clear for playing sounds.");
			sources = new AudioSource[0];
		}
		sources.Initialize();
		if ( clips.Length > 0 ) {
			Debug.LogWarning("--- AudioManager [Start] : Clips array is not empty. will clear for loading sounds array items.");
			clips = new AudioClip[0];
		}
		clips.Initialize();

		// initialize
		InitializeSounds();
		if ( sounds.Length == 0 ) {
			Debug.LogError("--- AudioManager [Start] : Sounds array is empty. aborting.");
			enabled = false;
		}
		else
			LoadAudioArray();
	}

	void OnApplicationPause( bool paused ) {

		detectedPaused = paused;
	}

	void OnApplicationFocus( bool focus ) {

		detectedFocus = focus;
	}

	void Update() {

		foreach (AudioSource s in sources) {
			// This fixes bug when application minimized will destroy audio sources, detecting they are not playing
			if ( !s.isPlaying && ( !detectedPaused && ( Application.platform != RuntimePlatform.OSXPlayer || detectedFocus ) ) ) {
				// remove old source from array
				AudioSource[] src = sources;
				sources = new AudioSource[(src.Length - 1)];
				int n = 0;
				for (int i = 0; i < src.Length; i++) {
					if (src [i] == s)
						continue;
					else {
						sources [n] = src [i];
						n++;
					}
				}
				// destroy source
				Destroy (s);
			}
		}

		// handle fades
		if ( fades != null && fades.Length > 0 ) {
			int fadesDone = 0;
			for ( int i=0; i<fades.Length; i++ ) {
				fades[i].fadeTime -= Time.deltaTime;
				if ( fades[i].fadeTime <= 0f )
					fades[i].fadeTime = 0f;
				float val = ( fades[i].fadeTime / fades[i].totalTime );
				if ( fades[i].fadeIn ) {
					val = 1-val;
					val *= fades[i].maxVolume;
				}
				else
					val *= fades[i].maxVolume;
				if ( fades[i].sourceIndex < sources.Length ) {
					sources[ fades[i].sourceIndex ].volume = val;
					// stop clips that fade out (will be removed)
					if ( val == 0f && fades[i].fadeTime <= 0f )
						sources[ fades[i].sourceIndex ].Stop();
				}
				else {
					Debug.LogWarning ("--- AudioManager [Update] : fade source "+i+" out of range. if music, will skip fade and recover last requested.");
					if ( FindSound( fades[i].fadeName ).type == aType.MusicLoop )
						fades[i].fadeTime = 0f;
				}
				if ( fades[i].fadeTime <= 0f )
					fadesDone++;
			}
			// remove done fades
			if ( fadesDone > 0 ) {
				Fade[] newFades = fades;
				int n = 0;
				fades = new Fade[newFades.Length-fadesDone];
				for ( int i=newFades.Length-1; i>=0; i-- ) {
					if ( newFades[i].fadeTime != 0f ) {
						fades[n] = newFades[i];
						n++;
					}
				}
			}
		}

		// HACK: safety music check time
		if ( musicCheckTime > 0f ) {
			musicCheckTime -= Time.deltaTime;
			if ( musicCheckTime <= 0f ) {
				musicCheckTime = 0f;
				if ( !IsPlaying( lastMusicRequested ) )
					AddSource( FindSound( lastMusicRequested ) );
				lastMusicRequested = "";
			}
		}
	}

	/// <summary>
	/// Initializes the sounds array
	/// </summary>
	void InitializeSounds() {

		AudioData 	data = new AudioData();
		sounds = new AudioMap[data.totalSounds];

		sounds.Initialize();
		data.InitializeAudioData();
		for ( int i=0; i<sounds.Length; i++ ) {
			sounds[i].name = data.soundName[i];
			sounds[i].type = data.soundType[i];
		}
	}

	/// <summary>
	/// Loads the audio array based on AudioData
	/// </summary>
	void LoadAudioArray() {

		AudioClip 	clip = null;
		AudioClip[]	newClips = new AudioClip[0];
		int 		count = 0;

		foreach( AudioMap m in sounds ) {
			count = 0;
			clip = null;
			clip = Resources.Load(m.name) as AudioClip;
			if ( clip == null )
				Debug.LogWarning("--- AudioManager [LoadAudioArray] : unable to load audio clip '"+m.name+"' at #"+(clips.Length+1)+". will ignore.");
			else {
				newClips = new AudioClip[(clips.Length+1)];
				foreach ( AudioClip c in clips ) {
					newClips[count] = c;
					count++;
				}
				newClips[count] = clip;
				clips = newClips;
			}
		}
	}

	/// <summary>
	/// Plays audio based on sound name
	/// </summary>
	/// <param name="name">sound name</param>
	public void PlayAudio( string sName ) {
		
		AudioMap sound = FindSound( sName );
		if ( sound.type == aType.Invalid )
			Debug.LogWarning("--- AudioManager [PlayAudio] : unable to load audio clip for '"+sName+"'. will ignore.");
		else
			AddSource( sound );
	}

	/// <summary>
	/// Plays the audio with a random variation of pitch
	/// </summary>
	/// <param name="sName">Sound name.</param>
	/// <param name="pitchVariance">Pitch variance.</param>
	public void PlayPitchedAudio( string sName, float pitchVariance ) {
		PlayAudio (sName);
		for (int i = sources.Length-1; i > 0; i--) {
			if (sources [i].clip.name == sName) {
				sources [i].pitch = 1f + (Random.Range (0, pitchVariance) - pitchVariance / 2);
				break;
			}
		}
	}

	/// <summary>
	/// Finds the Audio Map element in sounds array by name
	/// </summary>
	/// <returns>AudioMap sound</returns>
	/// <param name="sName">sound name</param>
	AudioMap FindSound( string sName ) {

		AudioMap retMap = new AudioMap();

		foreach( AudioMap m in sounds ) {
			if ( m.name == sName )
				retMap = m;
		}

		return retMap;
	}

	/// <summary>
	/// Adds the audio source and configures for sound details
	/// </summary>
	/// <param name="sound">Audio Map element from sounds array</param>
	void AddSource( AudioMap sound ) {

		// create source
		AudioSource	s = gameObject.AddComponent<AudioSource> ();
		s.clip = MapAudio (sound);
		s.volume = MapVolume (sound);
		if (sound.type == aType.MusicLoop) {
			// handle music loop addition as fade-in / cross-fade
			s.loop = true;
			s.volume = 0f;
			FadeOutMusicLoops ();
			StartFade (sound.name, sources.Length, true, 2f, MapVolume (sound));
			lastMusicRequested = sound.name;
			musicCheckTime = 2.1f;
		}
		s.Play ();
		// add new source to array
		AudioSource[] src = sources;
		sources = new AudioSource[ (src.Length + 1) ];
		for (int i = 0; i < src.Length; i++) {
			sources [i] = src [i];
		}
		sources [sources.Length - 1] = s;
	}

	/// <summary>
	/// Maps the sound to an AudioClip
	/// </summary>
	/// <returns>sound AudioClip</returns>
	/// <param name="sound">AudioManager.sounds element</param>
	AudioClip MapAudio( AudioMap sound ) {
		
		int			count = 0;

		foreach( AudioMap m in sounds ) {
			if ( m.Equals(sound) )
				break;
			count++;
		}
		
		if ( count < clips.Length )
			return clips[count];
		else
			return null;
	}

	/// <summary>
	/// Maps the sound to a volume level
	/// </summary>
	/// <returns>sound volume</returns>
	/// <param name="sound">AudioManager.sounds element</param>
	float	MapVolume( AudioMap sound ) {
		
		float	retVal = defaultVolume;

		switch ( sound.type ) {
		case aType.SFXUI:
			retVal = sfxVolume;
			break;
		case aType.SFXGame:
			retVal = sfxVolume;
			break;
		case aType.MusicSting:
			retVal = musicVolume;
			break;
		case aType.MusicLoop:
			retVal = musicVolume;
			break;
		case aType.Voice:
			retVal = voiceVolume;
			break;
		}

		return retVal;
	}

	/// <summary>
	/// Determines whether the AudioManager is playing the specified soundName
	/// </summary>
	/// <returns><c>true</c> if this instance is playing the specified soundName; otherwise, <c>false</c>.</returns>
	/// <param name="soundName">Sound name</param>
	public bool IsPlaying( string soundName ) {

		bool retBool = false;

		foreach( AudioSource s in sources ) {
			if ( s.clip.name == soundName ) {
				retBool = true;
				break;
			}
		}

		return retBool;
	}

	/// <summary>
	/// Determines whether sound is currently being faded out
	/// </summary>
	/// <returns><c>true</c> if this instance is fading out the specified soundName; otherwise, <c>false</c>.</returns>
	/// <param name="soundName">Sound name.</param>
	public bool IsFadingOut( string soundName ) {

		bool retBool = false;

		foreach( Fade f in fades ) {
			if ( f.sourceIndex < sources.Length && sources[f.sourceIndex].clip.name == soundName && !f.fadeIn ) {
				retBool = true;
				break;
			}
		}

		return retBool;
	}

	/// <summary>
	/// Updates the volume.
	/// </summary>
	/// <param name="type">Type</param>
	/// <param name="volume">Volume</param>
	public void	UpdateVolume( aType type, float volume ) {

		switch (type) {
		case aType.Default:
			defaultVolume = volume;
			musicVolume = volume;
			sfxVolume = volume;
			voiceVolume = volume;
			break;
		case aType.MusicLoop:
			musicVolume = volume;
			break;
		case aType.MusicSting:
			musicVolume = volume;
			break;
		case aType.SFXGame:
			sfxVolume = volume;
			break;
		case aType.SFXUI:
			sfxVolume = volume;
			break;
		case aType.Voice:
			voiceVolume = volume;
			break;
		}

		// update currently playing sounds
		AdjustPlayingVolume();
	}

	void AdjustPlayingVolume() {

		if ( ( sources.Length - fades.Length ) < 1 )
			return;

		// list all sounds currently playing that are not also fading
		AudioMap[] playing = new AudioMap[sources.Length - fades.Length];
		int n = 0;
		foreach( AudioMap s in sounds ) {
			if ( IsPlaying( s.name ) ) {
				bool found = false;
				foreach( Fade f in fades ) {
					if ( sources[ f.sourceIndex ].clip.name == s.name ) {
						found = true;
						break;
					}
				}
				if ( !found ) {
					playing[n] = s;
					n++;
				}
			}
		}

		// set volume on each source to match the current type volume
		for ( int i=0; i<playing.Length; i++ ) {
			foreach( AudioSource s in sources ) {
				if ( s.clip.name == playing[i].name )
					s.volume = MapVolume( playing[i] );
			}
		}
	}

	/// <summary>
	/// Starts the fade.
	/// </summary>
	/// <param name="sourceIndex">Source index</param>
	/// <param name="fadeIn">If set to <c>true</c> fade in</param>
	/// <param name="totalTime">Total time</param>
	/// <param name="maxVolume">Max volume</param>
	void StartFade( string clipName, int sourceIndex, bool fadeIn, float totalTime, float maxVolume ) {

		Fade[] newFades = fades;
		if ( fades != null )
			fades = new Fade[newFades.Length+1];
		else
			fades = new Fade[1];
		fades[0] = new Fade();
		fades[0].fadeName = clipName;
		fades[0].sourceIndex = sourceIndex;
		fades[0].fadeIn = fadeIn;
		fades[0].totalTime = totalTime;
		fades[0].maxVolume = maxVolume;
		fades[0].fadeTime = totalTime;
		if ( newFades != null && newFades.Length > 0 ) {
			for ( int i=1; i<fades.Length; i++ ) {
				fades[i] = newFades[i-1];
			}
		}
	}

	/// <summary>
	/// Fades down all music loops
	/// </summary>
	public void FadeOutMusicLoops() {

		/*
		// find any current fades of music loops and fade them out
		for ( int i=0; i<fades.Length; i++ ) {
			if ( fades[i].sourceIndex < sources.Length && FindSound( sources[fades[i].sourceIndex].clip.name).type == aType.MusicLoop ) {
				fades [i].fadeIn = false;
			}
		}
		*/

		for ( int i=0; i<sources.Length; i++ ) {
			if ( sources[i].loop ) {
				StartFade( sources[i].clip.name, i, false, 2f, sources[i].volume );
			}
		}
	}

	/// <summary>
	/// Fades down all audio matching sound.
	/// </summary>
	/// <param name="sound">Sound</param>
	/// <param name="fadeTime">Fade time</param>
	public void FadeOutSound( string sound, float fadeTime ) {

		if ( IsPlaying( sound ) ) {
			for ( int i=0; i<sources.Length; i++ ) {
				if ( sources[i].clip.name == sound ) {
					StartFade( sources[i].clip.name, i, false, fadeTime, sources[i].volume );
				}
			}
		}
	}
}
