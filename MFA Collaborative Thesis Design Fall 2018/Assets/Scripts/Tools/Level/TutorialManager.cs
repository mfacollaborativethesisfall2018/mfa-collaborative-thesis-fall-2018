﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour {

	// Author: Glenn Storm
	// This manages the sequence of tutorial popup displays, the OK button, and begins detection of the next TutorialTrigger

	[System.Serializable]
	public	struct TutorialStep
	{
		public	string			stepName;
		public	TutorialTrigger	triggerToActivate;
		public	GameObject 		objectToActivate;
		public	string			stepTextToDisplay;
	}
	public	TutorialStep[] 		steps;

	public	Rect				tutorialRect;
	public	Texture2D 			tutorialBG;
	public	int 				tutorialPadding = 4;
	public	Font				tutorialFont;
	public	int 				fontSizeAt1024 = 30;
	public	Color 				fontColor = Color.white;

	public	Rect 				okButton;
	public	Texture2D 			okUnpressed;
	public	Texture2D			okPressed;

	public	Rect				hintLabel;

	private	int					currentStep;
	private	string 				currentText;
	private	bool 				displayText;
	private	float				pauseTimer;

	private	float 				gameTimeRate;
	private	bool				restartTime;
	private	float				restartTimer;
	private	float 				savedTimeDelta = 0.01f;
	private	CodeStore 			cs;
	private	PauseManager 		pm;
		
	const	float 				PAUSETIME = 0.5f;


	void Start () {
		gameTimeRate = Time.timeScale;
		pauseTimer = PAUSETIME;
		cs = GameObject.FindObjectOfType<CodeStore> ();
		if (cs == null) {
			Debug.LogError ("--- TutorialManager [Start] : "+gameObject.name+" no Code Store object found in scene. Aborting.");
			enabled = false;
		}
		pm = GameObject.FindObjectOfType<PauseManager> ();
		if (pm == null) {
			Debug.LogError ("--- TutorialManager [Start] : "+gameObject.name+" no Pause Manager object found in scene. Aborting.");
			enabled = false;
		}
	}

	void Update () {
		if ( pauseTimer > 0f ) {
			pauseTimer -= Time.deltaTime;
			if ( pauseTimer <= 0f ) {
				if ( currentStep >= steps.Length )
					CompleteTutorial ();
				else
					DisplayStepDialog (currentStep);
			}
		}
	}

	void ActivateTrigger() {
		displayText = false;
		Cursor.visible = false;
		restartTimer = 1f;
		steps [currentStep].triggerToActivate.gameObject.SetActive (true);
		if (steps [currentStep].objectToActivate != null)
			steps [currentStep].objectToActivate.SetActive (true);
	}

	public	void	TriggerNextStep() {
		steps [currentStep].triggerToActivate.gameObject.SetActive (false);
		currentStep++;
		pauseTimer = PAUSETIME;
	}

	void CompleteTutorial() {
		// tutorial steps complete
	}

	void DisplayStepDialog( int step ) {
		currentText = steps[step].stepTextToDisplay;
		displayText = true;
		Cursor.visible = true;
		Time.timeScale = 0f;
		Time.fixedDeltaTime = 0f;
		savedTimeDelta = Mathf.Max (0.01f, Time.deltaTime);
		cs.tutorialCamLock = true;
	}

	void OnGUI() {
		// run restart timer even if time stopped
		if (restartTimer > 0f) {
			restartTimer -= savedTimeDelta;
			if (restartTimer <= 0f) {
				restartTimer = 0f;
				Time.timeScale = gameTimeRate;
				Time.fixedDeltaTime = 0.02f * gameTimeRate;
				cs.tutorialCamLock = false;
			}
		}

		// display tutorial GUI elements
		Rect r = new Rect ();
		float w = Screen.width;
		float h = Screen.height;
		GUIStyle g = new GUIStyle (GUI.skin.label);
		g.normal.background = okPressed;
		g.font = tutorialFont;
		g.fontSize = Mathf.RoundToInt( fontSizeAt1024 * ( w / 1024f ));
		g.normal.textColor = fontColor;
		g.alignment = TextAnchor.MiddleCenter;
		string s = "Press H for HINT";

		if (pm.paused)
			return;

		// tutorial hint button to re-display step text popup
		if (!displayText) {
			r = hintLabel;
			r.x *= w;
			r.y *= h;
			r.width *= w;
			r.height *= h;
			GUI.Label (r, s, g);
			if (Input.GetKeyDown(KeyCode.H)) {
				DisplayStepDialog (currentStep);
				steps [currentStep].triggerToActivate.gameObject.SetActive (false);
			}
			return;
		}
			
		g = new GUIStyle (GUI.skin.box);
		g.normal.background = null;
		g.normal.textColor = fontColor;
		RectOffset ro = new RectOffset ();
		ro.top = tutorialPadding;
		ro.bottom = tutorialPadding;
		ro.left = tutorialPadding;
		ro.right = tutorialPadding;
		g.padding = ro;
		g.font = tutorialFont;
		g.fontSize = Mathf.RoundToInt( fontSizeAt1024 * ( w / 1024f ));
		g.wordWrap = true;
		g.alignment = TextAnchor.MiddleCenter;
		s = currentText;
		r = tutorialRect;
		r.x *= w;
		r.y *= h;
		r.width *= w;
		r.height *= h;
		GUI.DrawTexture (r, tutorialBG);
		GUI.Box (r, s, g);

		r = okButton;
		r.x *= w;
		r.y *= h;
		r.width *= w;
		r.height *= h;
		g = new GUIStyle (GUI.skin.button);
		g.normal.background = okUnpressed;
		g.hover.background = okUnpressed;
		g.focused.background = okUnpressed;
		g.active.background = okPressed;
		g.font = tutorialFont;
		g.fontSize = Mathf.RoundToInt( fontSizeAt1024 * ( w / 1024f ));
		g.normal.textColor = fontColor;
		g.alignment = TextAnchor.MiddleCenter;
		s = "OK";
		if (GUI.Button (r, s, g)) {
			ActivateTrigger ();
		}
	}
}
