﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NYFA Level Design/Level/Lock Game Object")]
public class LockGameObject : MonoBehaviour {

	// Author: Glenn Storm
	// Locks a game object in place, with all transforms reset, both in the editor and at runtime, to allow it to be used as a folder safely

	private	Vector3		savedLocalPostion = Vector3.zero;
	private Quaternion 	savedLocalRotation = Quaternion.identity;
	private	Vector3		savedLocalScale = Vector3.one;


	void Update()
	{
		gameObject.transform.position = savedLocalPostion;
		gameObject.transform.rotation = savedLocalRotation;
		gameObject.transform.localScale = savedLocalScale;
	}

	void FixedUpdate()
	{
		gameObject.transform.position = savedLocalPostion;
		gameObject.transform.rotation = savedLocalRotation;
		gameObject.transform.localScale = savedLocalScale;
	}

	void LateUpdate()
	{
		gameObject.transform.position = savedLocalPostion;
		gameObject.transform.rotation = savedLocalRotation;
		gameObject.transform.localScale = savedLocalScale;
	}
}

