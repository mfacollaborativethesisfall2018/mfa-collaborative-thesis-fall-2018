﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUnlockTrigger : MonoBehaviour {

	// Author: Glenn Storm
	// This triggers the level unlock manager to unlock levels

	public	string 				levelToUnlock;

	private	LevelUnlockManager	lum;


	void Start () {
		// validate
		lum = GameObject.FindObjectOfType<LevelUnlockManager>();
		if (lum == null) {
			Debug.LogError ("--- LevelUnlockTrigger [Start] : "+gameObject.name+" no level unlock manager found. aborting.");
			enabled = false;
		}
		// unlock
		if (enabled) {
			if (!lum.UnlockLevel (levelToUnlock))
				Debug.LogWarning ("--- LevelUnlockTrigger [Start] : "+gameObject.name+" level '"+levelToUnlock+"' not found on level lock manager. not unlocked.");
		}
	}
}
