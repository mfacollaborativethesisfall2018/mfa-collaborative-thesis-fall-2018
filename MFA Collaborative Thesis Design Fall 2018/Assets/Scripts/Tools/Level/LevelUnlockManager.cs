﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelUnlockManager : MonoBehaviour {

	// Author: Glenn Storm
	// This handles a linear sequence of locked levels

	[System.Serializable]
	public	struct LevelLock
	{
		public	string		levelName;
		public	bool		unlocked;
	}
	public	LevelLock[]		levels;
	public	string 			tutorialLevelName;
	public	string			levelMenuName = "Level Select Menu";
	public	string			cheatUnlockAll = "cheatunlockall";

	private	bool 			tutorialHidden;  // TODO: have a way to return to tutorial level
	private	bool			levelsUpdated;
	private	bool 			allLevelsUnlocked;
	private	bool			levelsScreen;
	private	bool 			menuScreen;
	private	at_MenuManager	levelMenu;
	private at_SceneManager sceneMgr;


	void Start () {
		// validation

		// delegates
		SceneManager.activeSceneChanged += SceneChanged;
		SceneManager.sceneUnloaded += SceneChanging;
	}

	void SceneChanged( Scene fromScene, Scene toScene ) {

		levelsScreen = ( toScene.name == "Levels" );
		menuScreen = ( toScene.name == "Main" );
		levelsUpdated = (levelsScreen || menuScreen);
	}

	void SceneChanging( Scene fromScene ) {

		levelsScreen = false;
		menuScreen = false;
	}

	public	bool UnlockLevel( string levelName ) {
		
		bool levelUnlocked = false;

		if (levelName == cheatUnlockAll) {
			for (int i = 0; i < levels.Length; i++) {
				levels [i].unlocked = true;
			}
			levelUnlocked = true;
			allLevelsUnlocked = true;
		}
		else {
			for (int i = 0; i < levels.Length; i++) {
				if (levels [i].levelName == levelName) {
					levels [i].unlocked = true;
					levelUnlocked = true;
				}
			}
		}

		// check all levels unlocked
		int countLvls = 0;
		for (int i = 0; i < levels.Length; i++) {
			if (levels [i].unlocked)
				countLvls++;
		}
		if (countLvls == levels.Length)
			allLevelsUnlocked = true;

		levelsUpdated = levelUnlocked;
		tutorialHidden = true; // TODO: tutorial hidden after any unlock, provide way to return later
		return levelUnlocked;
	}

	void Update () {

		if (levelsScreen && levelMenu == null) {
			at_MenuManager[] menus = GameObject.FindObjectsOfType<at_MenuManager> () as at_MenuManager[];
			for (int i = 0; i < menus.Length; i++) {
				if (menus [i].name == levelMenuName) {
					levelMenu = menus [i];
					break;
				}
			}
		}
		else
			levelMenu = null;

		if (menuScreen && sceneMgr == null) {
			sceneMgr = GameObject.FindObjectOfType<at_SceneManager> () as at_SceneManager;
		}

		if (levelMenu != null && levelsUpdated && levels.Length > 0) {
			// iterate through levels array and set pressed state of disable button [0] on level menu
			for (int i = 0; i < levels.Length; i++) {
				for (int n = 0; n < levelMenu.buttons.Length; n++) {
					if (levelMenu.buttons [n].buttonLabel == levels [i].levelName) {
						if (levelMenu.buttons [n].conditionalEnabled) {
							if (levelMenu.buttons [n].disableButtons.Length > 0)
								levelMenu.buttons [levelMenu.buttons [n].disableButtons [0]].buttonPressed = !levels [i].unlocked;
						}
						else {
							Debug.LogWarning ("--- conditional enabled button index not set for "+levels[i].levelName+". will set conditional enabled.");
							levelMenu.buttons [n].conditionalEnabled = true;
						}
					}
				}
			}
			levelsUpdated = false;
		}

		// Tutorial level display management
		if (levelMenu != null && tutorialLevelName != "" && levelMenu.buttons.Length > 0) {
			for (int i = 0; i < levelMenu.buttons.Length; i++) {
				if (levelMenu.buttons [i].buttonLabel == tutorialLevelName) {
					levelMenu.buttons [levelMenu.buttons [i].displayButton].buttonPressed = (!tutorialHidden);
					break;
				}
			}
		}

		int playIndex = 0;
		if (menuScreen && sceneMgr != null) {
			for (int i = 0; i < sceneMgr.controls.Count; i++) {
				if (sceneMgr.controls [i].buttonLabel == "Play") {
					playIndex = i;
					break;
				}
			}
		}
		if (menuScreen && sceneMgr != null && allLevelsUnlocked) {
			// set 'Play' button to random level
			sceneMgr.controls[playIndex].levelName = SelectRandomLevel ();
		}
		else if (menuScreen && levelsUpdated && sceneMgr != null) {
			// iterate through levels array and configure 'Play' button to highest level progression
			string lvlName = sceneMgr.controls[playIndex].levelName; // 'Play' button current
			for (int i = 0; i < levels.Length; i++) {
				// stop if no more unlocked, skip if not a level
				if (lvlName == tutorialLevelName || ( !levels [i].unlocked && levels[i].levelName != "Bedroom" && levels[i].levelName != "Kitchen" && levels[i].levelName != "Living Room" ) )
					break;
				else
					lvlName = levels [i].levelName;
			}
			// map level button name to scene name
			switch (lvlName) {
			case "Tutorial":
				lvlName = "Tutorial";
				break;
			case "Bedroom Desk":
				lvlName = "Game01";
				break;
			case "Bedroom Floor":
				lvlName = "Game02";
				break;
			case "Bedroom Table":
				lvlName = "Game03";
				break;
			case "Kitchen Table":
				lvlName = "Game04";
				break;
			case "Kitchen Counter":
				lvlName = "Game05";
				break;
			case "Kitchen Floor":
				lvlName = "Game06";
				break;
			case "Fireplace":
				lvlName = "Game07";
				break;
			case "Coffee Table":
				lvlName = "Game08";
				break;
			case "Living Room Floor":
				lvlName = "Game09";
				break;
			}
			sceneMgr.controls [playIndex].levelName = lvlName;
		}
	}

	string	SelectRandomLevel() {
		return "Game0" + Random.Range (1, 10).ToString();
	}
}
