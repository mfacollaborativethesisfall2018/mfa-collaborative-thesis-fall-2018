﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : MonoBehaviour {

	// Author: Glenn Storm
	// This detects player actions as steps in the tutorial sequence and triggers the TutorialManager to start the next step

	// TODO: detail all types
	public	enum TutorialTriggerType
	{
		Default,
		Welcome,
		Movement,
		MouseLook,
		Dash,
		LockOn,
		LightAttack,
		Energy,
		HeavyAttack,
		PerfectDodge,
		Combo,
		Block,
		Parry,
		TutorialEnd
	}
	public	TutorialTriggerType		type;

	private	TutorialManager			tm;
	private CombatComboTest 		cct;
	private	CodeStore 				cs;

	private	float					timer;
	private float					timeMovedForward;
	private	float					timeMovedBack;
	private	float					timeMovedLeft;
	private	float					timeMovedRight;
	private	float					camAngle;
	private	bool					dashing;
	private	int						timesDashed;

	const	float 					WELCOMEPAUSE = 4f;
	const	float 					MOVEDIRECTIONTIME = 0.5f;
	const	float 					MOUSELOOKANGLE = 150f;
	const	float 					DASHTIMES = 3;


	void Start () {
		// validate
		tm = GameObject.FindObjectOfType<TutorialManager> ();
		if (tm == null) {
			Debug.LogError ("--- TutorialTrigger [Start] : "+gameObject.name+" no TutorialManager object found in scene. Aborting.");
			enabled = false;
		}
		cct = GameObject.FindObjectOfType<CombatComboTest> ();
		if (cct == null) {
			Debug.LogError ("--- TutorialTrigger [Start] : "+gameObject.name+" no CombatComboTest object found in scene. Aborting.");
			enabled = false;
		}
		cs = GameObject.FindObjectOfType<CodeStore> ();
		if (cs == null) {
			Debug.LogError ("--- TutorialTrigger [Start] : "+gameObject.name+" no CodeStore object found in scene. Aborting.");
			enabled = false;
		}
		// initialize based on type
		switch (type) {
		case TutorialTriggerType.Default:
			break;
		case TutorialTriggerType.Welcome:
			timer = WELCOMEPAUSE;
			break;
		case TutorialTriggerType.Movement:
			break;
		case TutorialTriggerType.MouseLook:
			// start camera angle
			camAngle = cs.cameraRoot.transform.eulerAngles.y;
			break;
		}
	}

	void Update () {

		// run down timer
		if (timer > 0f) {
			timer -= Time.deltaTime;
			if (timer <= 0f) {
				timer = 0f;
			}
		}

		// detect conditions based on trigger type
		switch (type) {
		case TutorialTriggerType.Default:
			break;
		case TutorialTriggerType.Welcome:
			if (timer == 0f)
				DoTrigger ();
			break;
		case TutorialTriggerType.Movement:
			if (cs.m_ForwardAmount > 0.1f)
				timeMovedForward += Time.deltaTime;
			if (cs.m_ForwardAmount < -0.1f)
				timeMovedBack += Time.deltaTime;
			if (cs.m_TurnAmount < -0.1f)
				timeMovedLeft += Time.deltaTime;
			if (cs.m_TurnAmount > 0.1f)
				timeMovedRight += Time.deltaTime;
			if (timeMovedForward > MOVEDIRECTIONTIME && timeMovedBack > MOVEDIRECTIONTIME && timeMovedLeft > MOVEDIRECTIONTIME && timeMovedRight > MOVEDIRECTIONTIME)
				DoTrigger ();
			break;
		case TutorialTriggerType.MouseLook:
			if (Mathf.Abs(camAngle-cs.cameraRoot.transform.eulerAngles.y) > MOUSELOOKANGLE)
				DoTrigger ();
			break;
		case TutorialTriggerType.Dash:
			if (!dashing && cct.dodge.activeSelf)
				timesDashed++;
			dashing = cct.dodge.activeSelf;
			if (timesDashed > DASHTIMES)
				DoTrigger ();
			break;
		case TutorialTriggerType.LockOn:
			if (cct.lockOn.activeSelf)
				DoTrigger ();
			break;
		case TutorialTriggerType.LightAttack:
			if (cct.lightAttack.activeSelf)
				DoTrigger ();
			break;
		case TutorialTriggerType.Energy:
			if (cct.playerEnergyRadial.fillAmount > .5f)
				DoTrigger ();
			break;
		case TutorialTriggerType.HeavyAttack:
			if (cct.heavyAttack.activeSelf)
				DoTrigger ();
			break;
		case TutorialTriggerType.PerfectDodge:
			if (cct.perfectDodge.activeSelf)
				DoTrigger ();
			break;
		case TutorialTriggerType.Combo:
			if (cct.lightAttack2.activeSelf || cct.lightAttack3.activeSelf)
				DoTrigger ();
			break;
		case TutorialTriggerType.Block:
			if (cct.block.activeSelf)
				DoTrigger ();
			break;
		case TutorialTriggerType.Parry:
			if (cct.perfectBlock.activeSelf)
				DoTrigger ();
			break;
		case TutorialTriggerType.TutorialEnd:
			// TODO: end conditions
			break;
		}
	}

	void DoTrigger() {
		tm.TriggerNextStep ();
	}
}
