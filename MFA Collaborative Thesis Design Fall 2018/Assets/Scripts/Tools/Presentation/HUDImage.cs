﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDImage : MonoBehaviour {

	// Author: Glenn Storm
	// This presents a colored image against a background color

	[Tooltip("The image to display")]
	public	Texture 		image;
	[Tooltip("The position and size of the image on screen")]
	public	Rect			screenSpace = new Rect(0.8f,0.1f,0.1f,0.1f);
	[Tooltip("If true, use Screen Space width as the size of the image, and use the original image shape")]
	public	bool 			useImageAspect = true;
	[Tooltip("The color applied to the image (white = full color)")]
	public	Color			imageColor = Color.white;
	[Tooltip("The color of the background behind the image")]
	public	Color			bgColor = Color.clear;
	[Tooltip("The layer depth of this GUI element")]
	public	int 			depth;


	void Start () {
		if (image == null) {
			Debug.LogWarning ("--- HUDImage [Start] : " + gameObject.name + " image is missing. will use white texture.");
			image = Texture2D.whiteTexture;
		}
		if (screenSpace == Rect.zero) {
			Debug.LogError ("--- HUDImage [Start] : " + gameObject.name + " screen space zero.");
			enabled = false;
		}
	}

	void OnGUI () {
		Rect r = screenSpace;
		float w = Screen.width;
		float h = Screen.height;
		r.x *= w;
		r.y *= h;
		r.width *= w;
		r.height *= h;
		if (useImageAspect)
			r.height = r.width * (image.height/image.width);
		Texture t = Texture2D.whiteTexture;
		GUI.color = bgColor;
		GUI.depth = depth;
		GUI.DrawTexture (r, Texture2D.whiteTexture);
		GUI.color = imageColor;
		GUI.DrawTexture (r, image);
	}
}
