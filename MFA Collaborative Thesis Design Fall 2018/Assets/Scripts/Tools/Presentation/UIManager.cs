﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

	// Author: Glenn Storm
	// This manager handles UI elements

	[Serializable]
	public	struct HUDItem {
		public	string		HUDItemName;
		public	Texture		HUDTexture;
		public	Rect		HUDRect;
		public	int 		HUDDepth;
		public	bool 		alignToTop;
		public	bool 		displayHUDItem;
		public	float		pulseDuration;
	}
	public	HUDItem[]		HUDItems;
	public	bool			hideAll;

	private	GameManager 	gm;
	private	UIWorks			works;
	private string			cheatString = "toggleui";
	private	int				cheatIndex;


	void Start () {

		// validation
		gm = GameObject.FindObjectOfType<GameManager>();
		if ( gm == null ) {
			Debug.LogWarning("--- UIManager [Start] : cannot find game manager. will ignore.");
		}
	}
	
	void Update () {

		// handle pulsing HUD duration
		for (int i=0; i< HUDItems.Length; i++ ) {
			if ( HUDItems[i].pulseDuration > 0f ) {
				HUDItems[i].pulseDuration -= Time.deltaTime;
				// properly fade HUD display in OnGUI
				if ( HUDItems[i].pulseDuration <= 0f )
					HUDItems[i].pulseDuration = 0f;
			}
		}

		// handle cheat code to toggle UI display
		string s = Input.inputString;
		if ( s == cheatString.Substring( cheatIndex, 1 ) ) {
			cheatIndex++;
			if ( cheatIndex == cheatString.Length ) {
				// cheat code compelte
				hideAll = !hideAll;
				cheatIndex = 0;
			}
		}
		else if ( s != "" )
			cheatIndex = 0;
	}

	void InitializeUI() {

		UIHUDData[] uiHuds = new UIHUDData[1];

		uiHuds = works.ConfigureHUDItems( uiHuds );
	}

	public	void	DisplayHUDItem( string itemName, bool display ) {

		bool	found = false;

		for (int i=0; i< HUDItems.Length; i++ ) {
			if ( HUDItems[i].HUDItemName == itemName ) {
				HUDItems[i].displayHUDItem = display;
				found = true;
				break;
			}
		}

		if ( !found )
			Debug.LogWarning("--- UIManager [DisplayHUDItem] : HUD item '"+itemName+"' not found. will ignore.");
	}

	public	void	PulseHUDItem( string itemName ) {

		bool	found = false;

		for (int i=0; i< HUDItems.Length; i++ ) {
			if ( HUDItems[i].HUDItemName == itemName ) {
				HUDItems[i].pulseDuration = 3.5f; // this duration starts transparent and ends opaque
				found = true;
				break;
			}
		}

		if ( !found )
			Debug.LogWarning("--- UIManager [PulseHUDItem] : HUD item '"+itemName+"' not found. will ignore.");
	}

	public	void	SetAll ( bool display ) {

		for (int i=0; i< HUDItems.Length; i++ ) {
			HUDItems [i].displayHUDItem = display;
		}
	}

	void OnGUI() {

		if ( hideAll )
			return;

		Rect r = new Rect();
		float w = Screen.width;
		float h = Screen.height;
		Texture t; // = new Texture();

		foreach( HUDItem i in HUDItems ) {
			if ( !i.displayHUDItem )
				continue;
			r.x = i.HUDRect.x * w;
			r.y = i.HUDRect.y * h;
			r.width = i.HUDRect.width * w;
			t = i.HUDTexture;
			// to maintain aspect ratio of original texture, adjust height to original texture aspect
			r.height = ((float)(t.height)/(float)(t.width)) * r.width;
			if ( !i.alignToTop ) {
				// then adjust y position to compensate for height change
				r.y += ((i.HUDRect.height * h) - r.height);
			}
			GUI.depth = i.HUDDepth;
			if ( i.pulseDuration > 0f ) {
				// adjust alpha of item based on pulse duration sine value
				float a = Mathf.Abs( Mathf.Sin( ( i.pulseDuration + 0.5f ) * Mathf.PI ) );
				Color fade = Color.white;
				fade.a = a;
				GUI.color = fade;
			}
			else
				GUI.color = Color.white;
			GUI.DrawTexture( r, t );
		}
	}
}
