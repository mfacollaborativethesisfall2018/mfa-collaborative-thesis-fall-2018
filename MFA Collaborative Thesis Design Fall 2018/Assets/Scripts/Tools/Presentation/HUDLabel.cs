﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDLabel : MonoBehaviour {

	// Author: Glenn Storm
	// This presents a label of colored text over a color background

	[Tooltip("The text to display on this label")]
	public	string			text;
	[Tooltip("The position and size of the label on screen")]
	public	Rect			screenSpace = new Rect(0.4f,0.1f,0.2f,0.1f);
	[Tooltip("The font to use for this label (default = Arial)")]
	public	Font 			labelFont;
	[Tooltip("The font size presented when the screen dimension width is 1024 pixels")]
	public	int 			fontSizeAt1024 = 18;
	[Tooltip("The font style of the label")]
	public	FontStyle 		labelStyle;
	[Tooltip("If true, the label text will be centered within the Screen Space")]
	public	bool			centered;
	[Tooltip("The color of the font text")]
	public	Color			fontColor = Color.white;
	[Tooltip("The color of the background behind the text")]
	public	Color			bgColor = Color.clear;
	[Tooltip("The layer depth of this GUI element")]
	public	int 			depth;


	void Start () {
		if (text == "")
			Debug.LogWarning ("--- HUDLabel [Start] : " + gameObject + " text is missing");
		if (screenSpace == Rect.zero) {
			Debug.LogError ("--- HUDLabel [Start] : " + gameObject.name + " screen space zero.");
			enabled = false;
		}
	}

	void OnGUI () {
		Rect r = screenSpace;
		float w = Screen.width;
		float h = Screen.height;
		r.x *= w;
		r.y *= h;
		r.width *= w;
		r.height *= h;
		Texture t = Texture2D.whiteTexture;
		GUI.color = bgColor;
		GUI.depth = depth;
		GUI.DrawTexture (r, Texture2D.whiteTexture);
		GUIStyle g = new GUIStyle (GUI.skin.label);
		if (labelFont != null)
			g.font = labelFont;
		g.fontSize = Mathf.RoundToInt(fontSizeAt1024 * (w / 1024f));
		g.fontStyle = labelStyle;
		if (centered)
			g.alignment = TextAnchor.MiddleCenter;
		GUI.color = fontColor;
		GUI.Label (r, text, g);
	}
}
