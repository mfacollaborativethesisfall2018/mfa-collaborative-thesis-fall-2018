﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPreviewManager : MonoBehaviour {

	// Author: Glenn Storm
	// This manages level preview image and text elements in the level menu scene

	public	at_MenuManager	levelMenu;
	public	Texture[] 		levelPreviews;
	public	HUDImage		previewFG;
	public	HUDImage 		previewBG;
	public	string[] 		levelDescriptions;
	public	at_TypeBox 		previewText;
	public	Rect 			playButton;
	public	GameObject[]	levelPreviewEvents;
	public	GameObject[]	sceneTriggers;

	private	int 			currentLevel;
	private	int 			previousLevel;
	private	bool			fading;
	private	float 			previewFadeTime;
	private	GUIStyle		menuButtonStyle;
	private	bool			displayPlayButton;

	const	float			FADETIMEMAX = 1f;


	void Start () {
		// validate
		if (levelMenu == null) {
			Debug.LogError("--- LevelPreviewManager [Start] : "+gameObject.name+" no level menu manager referenced. Aborting.");
			enabled = false;
		}
		else
			menuButtonStyle = levelMenu.buttonStyle;
		// initialize
		previewFG.imageColor = Color.clear;
		previewBG.imageColor = Color.clear;
	}

	public	void StartCrossFade( int levelIndex ) {
		currentLevel = levelIndex;
		previewFG.image = levelPreviews [currentLevel];
		fading = true;
		// duplicate level description object and reset with new description
		previewText.enabled = false;
		GameObject g = GameObject.Instantiate(previewText.gameObject,previewText.gameObject.transform.parent);
		g.name = previewText.gameObject.name;
		Destroy (previewText.gameObject);
		previewText = g.GetComponent<at_TypeBox> ();
		previewText.textToDisplay = levelDescriptions[currentLevel];
		previewText.enabled = true;
		displayPlayButton = true;
	}

	void Update () {
		UpdateCrossFade ();

		// work with level menu tool to detect current level selected (use level preview event objects)
		for (int i = 0; i < levelPreviewEvents.Length; i++) {
			if (levelPreviewEvents [i].activeSelf) {
				StartCrossFade (i);
				levelPreviewEvents [i].SetActive (false);
				break;
			}
		}
	}

	void UpdateCrossFade() {
		if (fading) {
			previewFadeTime += Time.deltaTime;
			if (previewFadeTime >= FADETIMEMAX) {
				previewFadeTime = 0f;
				fading = false;
				previousLevel = currentLevel;
				previewFG.imageColor = Color.clear;
				previewBG.imageColor = Color.white;
				previewBG.image = levelPreviews [previousLevel];
			}
			else {
				// cross fade
				Color fgC = Color.white;
				Color bgC = Color.white;
				float pct = (previewFadeTime / FADETIMEMAX);
				fgC.a = pct;
				bgC.a = 1f - pct;
				previewFG.imageColor = fgC;
				previewBG.imageColor = bgC;
			}
		}
	}

	void OnGUI() {

		if (!displayPlayButton)
			return;

		// display separate start level button for current level
		Rect r = playButton;
		r.x *= Screen.width;
		r.y *= Screen.height;
		r.width *= Screen.width;
		r.height *= Screen.height;
		GUIStyle g = new GUIStyle (GUI.skin.button);
		if (menuButtonStyle != null)
			g = menuButtonStyle;
		string s = "Play";
		if (GUI.Button (r, s, g)) {
			// launch current selected level
			sceneTriggers[currentLevel].SetActive(true);
		}
	}
}
