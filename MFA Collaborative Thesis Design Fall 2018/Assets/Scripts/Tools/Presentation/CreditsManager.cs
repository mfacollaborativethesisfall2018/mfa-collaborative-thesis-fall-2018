﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreditsManager : MonoBehaviour {

	// Author: Glenn Storm
	// Handles credits presentation

	public	Text[]			credits;
	public	float			creditsTime = 5f;

	private	int				currentIndex;
	private	Text			currentCredits;
	private float			currentCreditsTime;
	private float			currentPadTime = 0.5f;

	void Start () {
		
		if ( credits.Length == 0 ) {
			Debug.LogError("--- CreditsManager [Start] : credits length is zero. aborting.");
			enabled = false;
			return;
		}
		if ( creditsTime <= 0f ) {
			Debug.LogWarning("--- CreditsManager [Start] : credits time invalid. will set to 5 seconds.");
			creditsTime = 5f;
		}
		NextCredits();
	}
	
	void Update () {

		if ( currentPadTime > 0f ) {
			currentPadTime -= Time.deltaTime;
			if ( currentPadTime <= 0f ) {
				currentPadTime = 0f;
				if ( currentCredits != null )
					currentCredits.gameObject.SetActive(true);
			}
			else
				return;
		}

		currentCreditsTime -= Time.deltaTime;
		if ( currentCreditsTime <= 0f )
			NextCredits();
	}

	void NextPad() {
		
		currentPadTime = 0.5f;
	}

	void NextCredits() {
		
		if ( currentCredits != null )
			currentCredits.gameObject.SetActive(false);
		if ( currentIndex >= credits.Length ) {
			currentCredits = null;
			currentCreditsTime = creditsTime / 2f;
			currentIndex = -1;
		}
		else {
			currentCredits = credits[currentIndex];
			currentCreditsTime = creditsTime;
		}
		currentIndex++;
		currentPadTime = 0.5f;
	}
}
