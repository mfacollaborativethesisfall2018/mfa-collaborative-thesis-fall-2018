﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadingScreenSwitch : MonoBehaviour {

	// Author: Glenn Storm
	// Works with the scene manager's loading screen to switch between menu background and a black swatch

	public		Texture			menuTexture;
	public		Texture			gameTexture;
	public		bool			switchToGame;

	private		at_SceneManager	sm;
	private		at_MenuManager	mm;

	void Start() {

		sm = GameObject.FindObjectOfType<at_SceneManager>();
		if ( sm == null ) {
			Debug.LogError("--- LoadingScreenSwitch [Start] : no scene manager found.");
		}
		mm = GameObject.FindObjectOfType<at_MenuManager>();
		if ( mm == null ) {
			Debug.LogError("--- LoadingScreenSwitch [Start] : no menu manager found.");
		}

		if ( sm != null ) {
			if ( switchToGame )
				sm.loadingScreen = gameTexture;
			else
				sm.loadingScreen = menuTexture;
		}
	}
}
