﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class AttractManager : MonoBehaviour {

	// Author: Glenn Storm
	// Works with at_MenuManager to press a hidden button and launch splash screen again, as an 'attract mode'

	public	string				attractScene;
	public	float				attractDelay;

	private	at_MenuManager		mm;


	void Start () {

		mm = GameObject.FindObjectOfType<at_MenuManager>();
		if ( mm == null ) {
			Debug.Log("--- MenuButtonPress [Start] : no menu manager found. will ignore.");
			//enabled = false;
		}
		if ( attractScene == "" ) {
			Debug.Log("--- MenuButtonPress [Start] : no attract scene defined. aborting.");
			enabled = false;
		}
		if ( attractDelay < 0f )
			Debug.Log("--- MenuButtonPress [Start] : press delay is less than zero. will ignore.");
	}

	void Update () {

		attractDelay -= Time.deltaTime;
		if ( attractDelay <= 0f ) {
			if ( mm != null )
				mm.enabled = false;
			SceneManager.LoadScene( attractScene );
		}
	}
}
