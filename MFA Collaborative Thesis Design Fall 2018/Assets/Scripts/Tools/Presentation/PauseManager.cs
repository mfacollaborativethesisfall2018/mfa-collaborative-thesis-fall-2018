﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour {

	// Author: Glenn Storm
	// This manager handles input, display and triggering for the mid-game pause menu

	public	bool		paused;
	public	Rect		pausePanel = new Rect( .09f, 0f, .8f, 1f );
	public	Texture2D	pauseBackground;
	public	int			panelPadding = 7;
	public	Font		pauseFont;
	public	int			fontSizeAt1024 = 20;
	public	Texture2D	buttonUnpressed;
	public	Texture2D	buttonPressed;
	public	Rect		resumeButton = new Rect( .2f, .67f, .2f, .15f );
	public	Rect		tutorialButton = new Rect( 0.4f, 0.67f, .2f, .15f );
	public	Rect		exitButton = new Rect( .6f, .67f, .2f, .15f );
	public	GameObject	menuTriggerObject;
	public	Texture2D	tutorialPanel;
	public	Texture2D[]	tutorialScreens;

	private	GameManager	gm;
	private	float		savedTimeScale;
	private float		savedFixedDelta;
	private	bool		tutorialDisplay;
	private	int			tutorialScreenIndex;


	void Start () {

		// validate
		if ( menuTriggerObject == null ) {
			Debug.LogError("--- PauseManager [Start] : no menu trigger object defined. Aborting.");
			enabled = false;
		}

		// get reference to game manager
		gm = GameObject.FindObjectOfType<GameManager>();
		if ( gm == null ) {
			Debug.LogError("--- PauseManager [Start] : no game manager found. Aborting.");
			enabled = false;
		}

		// record time scale and fixed delta time
		if ( enabled ) {
			savedTimeScale = Time.timeScale;
			savedFixedDelta = Time.fixedDeltaTime;
		}
	}

	void OnDisable() {

		Cursor.visible = true; // in case of demo
	}

	void Update() {

		// handle input for pause menu to trigger time management
		if ( Input.GetKeyDown( KeyCode.Escape ) ) {
			paused = !paused;
			// play button click sfx
			gm.audioMgr.PlayAudio("SFX UI Button Forward");
			// stop or resume time
			if ( paused ) {
				Time.timeScale = 0f;
				Time.fixedDeltaTime = 0f;
				gm.optionsMgr.DisplayOptions( true );
				if ( gm.uiMgr != null )
					gm.uiMgr.hideAll = true;
				Cursor.visible = true; // in case of demo
			}
			else {
				Time.timeScale = savedTimeScale;
				Time.fixedDeltaTime = savedFixedDelta;
				gm.optionsMgr.DisplayOptions( false );
				if ( gm.uiMgr != null )
					gm.uiMgr.hideAll = false;
				tutorialDisplay = false;
			}
		}
	}
		
	void OnGUI() {

		if ( !paused )
			return;

		// display pause menu and buttons
		Rect r = new Rect();
		float w = Screen.width;
		float h = Screen.height;
		GUIStyle g = new GUIStyle( GUI.skin.box );
		string s = "Paused";

		if ( tutorialDisplay ) {
			// display tutorial panel instead
			r = new Rect( 0f,0f,1f,1f);
			r.x *= w;
			r.y *= h;
			r.width *= w;
			r.height *= h;
			g.normal.background = tutorialPanel;
			g.font = pauseFont;
			g.fontSize = Mathf.RoundToInt( fontSizeAt1024 * ( Screen.width / 1024f ) );
			int pad = ( panelPadding * g.fontSize );
			g.padding = new RectOffset( pad, pad, pad, pad );
			s = "";
		}
		else {
			r = pausePanel;
			r.x *= w;
			r.y *= h;
			r.width *= w;
			r.height *= h;

			//if ( pauseBackground != null )
			//	g = new GUIStyle();
			g.normal.background = pauseBackground;
			g.font = pauseFont;
			g.fontSize = Mathf.RoundToInt( fontSizeAt1024 * ( Screen.width / 1024f ) );
			int pad = ( panelPadding * g.fontSize );
			g.padding = new RectOffset( pad, pad, pad, pad );
			s = "Paused";
		}

		GUI.depth = -99;
		
		GUI.Box( r, s, g );

		if ( tutorialDisplay ) {
			// display current tutorial screen
			g.normal.background = tutorialScreens[tutorialScreenIndex];
			s = "";
			GUI.Box( r,s,g );
		}
			
		r = resumeButton;
		r.x *= w;
		r.y *= h;
		r.width *= w;
		r.height *= h;
		g = new GUIStyle( GUI.skin.button );
		g.font = pauseFont;
		g.fontSize = Mathf.RoundToInt( fontSizeAt1024 * ( Screen.width / 1024f ) );
		g.normal.background = buttonUnpressed;
		g.active.background = buttonPressed;
		g.hover.background = buttonPressed;
		if ( tutorialDisplay )
			s = "Previous";
		else
			s = "Resume";

		if ( GUI.Button( r, s, g ) ) {
			if ( tutorialDisplay ) {
				tutorialScreenIndex--;
				if ( tutorialScreenIndex < 0 )
					tutorialScreenIndex = tutorialScreens.Length-1;
			}
			else {
				// play button click sfx
				gm.audioMgr.PlayAudio("SFX UI Button Forward");
				// resume time
				Time.timeScale = savedTimeScale;
				Time.fixedDeltaTime = savedFixedDelta;
				gm.optionsMgr.DisplayOptions( false );
				if ( gm.uiMgr != null )
					gm.uiMgr.hideAll = false;
				paused = false;
			}
		}

		r = tutorialButton;
		r.x *= w;
		r.y *= h;
		r.width *= w;
		r.height *= h;
		if ( tutorialDisplay )
			s = "Back";
		else
			s = "Tutorial";

		if ( GUI.Button( r, s, g ) ) {
			// play button click sfx
			tutorialDisplay = !tutorialDisplay;
			if ( !tutorialDisplay ) {
				tutorialScreenIndex = 0;
				gm.optionsMgr.DisplayOptions( true );
			}
			else
				gm.optionsMgr.DisplayOptions( false );
			gm.audioMgr.PlayAudio("SFX UI Button Forward");
		}

		r = exitButton;
		r.x *= w;
		r.y *= h;
		r.width *= w;
		r.height *= h;
		if ( tutorialDisplay )
			s = "Next";
		else
			s = "Exit";

		if ( GUI.Button( r, s, g ) ) {
			if ( tutorialDisplay ) {
				tutorialScreenIndex++;
				if ( tutorialScreenIndex > tutorialScreens.Length-1 )
					tutorialScreenIndex = 0;
			}
			else {
				// play button click sfx
				gm.audioMgr.PlayAudio("SFX UI Button Forward");
				// resume time
				Time.timeScale = savedTimeScale;
				Time.fixedDeltaTime = savedFixedDelta;
				gm.optionsMgr.DisplayOptions( false );
				if ( gm.uiMgr != null )
					gm.uiMgr.hideAll = false;
				paused = false;
				menuTriggerObject.SetActive( true );
			}
		}
	}
}
