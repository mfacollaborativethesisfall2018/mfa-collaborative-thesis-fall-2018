﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[AddComponentMenu("NYFA Level Design/Presentation/Scene Trigger")]
public class SceneTrigger : MonoBehaviour {

	// Author: Glenn Storm
	// Switches scene, either based on collider trigger (default) or by timer, using a given delay

	[Tooltip("The name of the scene to switch to. Make sure this is set in the project's build settings. (File -> Build Settings...)")]
	public	string		sceneName;
	[Tooltip("If true, this tool will work with Physics2D objects. If false, this tool will work with 3D Physics objects.")]
	public	bool		configureFor2D;
	[Tooltip("If true, the Switch Delay property will be used to determine when to switch, after this tool is activated. If false, this tool will wait until it is triggered by its Collider component. Make sure the Collider is set to isTrigger = true.")]
	public	bool		useTimer;
	[Tooltip("If Use Timer is true, this is the amount of time in seconds after activation of this tool until the desired scene is switched.")]
	public	float		switchDelay;

	private	float		switchTime;


	void Start()
	{
		// validate
		if ( sceneName == "" )
		{
			Debug.LogError(". SceneTrigger '"+gameObject.name+"' does not have a Scene Name defined. Aborting.");
			enabled = false;
		}
		if ( !useTimer )
		{
			Collider c = gameObject.GetComponent<Collider>();
			if ( c == null )
			{
				Debug.LogError(".SceneTrigger '"+gameObject.name+"' has Use Timer set to true, but there is no Collider component. Aborting.");
				enabled = false;
			}
			else if ( !c.isTrigger )
			{
				Debug.LogError(".SceneTrigger '"+gameObject.name+"' has Use Timer set to true, but the Collider component has isTrigger = false. Aborting.");
				enabled = false;
			}
		}
		if ( switchDelay >= 0f )
		{
			if ( !useTimer )
				Debug.LogWarning(". SceneTrigger '"+gameObject.name+"' has Use Timer set to false, but Switch Delay is configured as well. Will ignore delay.");
		}
		else
		{
			Debug.LogWarning(". SceneTrigger '"+gameObject.name+"' has an invalid Switch Delay setting of "+switchDelay+". Will ignore.");
		}

		if ( enabled && useTimer ) {
			switchTime = switchDelay;
			if (switchDelay == 0f)
				DoSwitch ();
		}
	}
	
	void Update()
	{
		// run timer
		if ( switchTime > 0f )
		{
			switchTime -= Time.deltaTime;
			if ( switchTime < 0f )
			{
				switchTime = 0f;
				DoSwitch();
			}
		}
	}

	void OnTriggerEnter( Collider other )
	{
		// handle trigger

		if ( configureFor2D )
			return;

		// NOTE: Any collider will trigger. To make this specific for particular collders, this could be modified to accept only that with a particular name or tag, etc.
		DoSwitch();
	}

	void OnTriggerEnter2D( Collider2D other )
	{
		// handle trigger

		if ( !configureFor2D )
			return;

		// NOTE: Any collider will trigger. To make this specific for particular collders, this could be modified to accept only that with a particular name or tag, etc.
		DoSwitch();
	}

	void DoSwitch()
	{
		// switch scene
		SceneManager.LoadScene( sceneName );
	}
}
