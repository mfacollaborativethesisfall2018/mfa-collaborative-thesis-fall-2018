﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBar : MonoBehaviour {

	// Author: Glenn Storm
	// This presents a colored progress bar with a background color

	public	enum BarOrientation
	{
		LeftToRight,
		RightToLeft,
		DownToUp,
		UpToDown
	}
	[Tooltip("The direction of the bar, from zero to maximum")]
	public	BarOrientation	orientation;
	[Tooltip("The position and size of the bar on screen")]
	public	Rect			screenSpace = new Rect(0.1f,0.1f,0.2f,0.1f);
	[Tooltip("The currect value being tracked")]
	public	float			value;
	[Tooltip("The maximum value the bar displays")]
	public	float			maxValue = 1f;
	[Tooltip("The color of the bar")]
	public	Color			barColor = Color.white;
	[Tooltip("The color of the background behind the bar")]
	public	Color			bgColor = Color.clear;
	[Tooltip("The layer depth of this GUI element")]
	public	int 			depth;


	void Start () {
		if (screenSpace == Rect.zero) {
			Debug.LogError ("--- ProgressBar [Start] : " + gameObject.name + " screen space zero.");
			enabled = false;
		}
		if (maxValue == 0) {
			Debug.LogError ("--- ProgressBar [Start] : " + gameObject.name + " max value zero.");
			enabled = false;
		}
	}

	void Update () {
		if (value > maxValue)
			value = maxValue;
		if (value < 0)
			value = 0;
	}

	void OnGUI () {
		Rect r = screenSpace;
		float w = Screen.width;
		float h = Screen.height;
		r.x *= w;
		r.y *= h;
		r.width *= w;
		r.height *= h;
		GUI.color = bgColor;
		GUI.depth = depth;
		GUI.DrawTexture (r, Texture2D.whiteTexture);
		if (orientation == BarOrientation.LeftToRight)
			r.width *= ( value / maxValue );
		else if (orientation == BarOrientation.RightToLeft) {
			r.x += r.width;
			r.width *= -1;
			r.width *= ( value / maxValue );
		} else if (orientation == BarOrientation.DownToUp) {
			r.y += r.height;
			r.height *= -1;
			r.height *= ( value / maxValue );
		} else if (orientation == BarOrientation.UpToDown) {
			r.height *= ( value / maxValue );
		}
		GUI.color = barColor;
		GUI.DrawTexture (r, Texture2D.whiteTexture);
	}
}
