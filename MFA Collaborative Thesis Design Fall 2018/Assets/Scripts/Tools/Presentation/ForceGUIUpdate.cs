﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceGUIUpdate : MonoBehaviour {

	// Author: Glenn Storm
	// This detects mouse movement when cursor is visible, and forces redraw of GUI

	// NOTE: The bug this attempts to fix is seen in builds only: button hover behavior ignored

	private	Vector3 	lastMousePosition;
	private	bool 		lastCursorVisible;
	private	bool		mouseMoved;


	void Start () {
		lastMousePosition = Input.mousePosition;
		lastCursorVisible = Cursor.visible;
	}
	
	void Update () {

		if (Input.mousePosition != lastMousePosition || Cursor.visible != lastCursorVisible) {
			// Redraw GUI elements (buttons)
			at_MenuManager mm = GameObject.FindObjectOfType<at_MenuManager>();
			mm.BroadcastMessage ("OnGUI"); // FIXME: need to find a way to force gui to update
		}

		// revise detection conditions
		lastMousePosition = Input.mousePosition;
		lastCursorVisible = Cursor.visible;
	}
}
