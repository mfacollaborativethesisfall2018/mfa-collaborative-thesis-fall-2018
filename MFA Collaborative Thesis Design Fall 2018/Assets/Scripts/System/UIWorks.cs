﻿using System.Collections;
using UnityEngine;

public class UIWorks {

	// Author: Glenn Storm
	// This class manipulates UI Data and UI HUD Data

	public	const	int 	totalHUDItems = 12;


	public	UIHUDData[]	ConfigureHUDItems( UIHUDData[] uiElements ) {

		UIHUDData[]	returnUIElements = new UIHUDData[ uiElements.Length ];

		for (int i=0; i<uiElements.Length; i++ ) {

			returnUIElements [i] = uiElements [i];
		}

		return returnUIElements;
	}


}
