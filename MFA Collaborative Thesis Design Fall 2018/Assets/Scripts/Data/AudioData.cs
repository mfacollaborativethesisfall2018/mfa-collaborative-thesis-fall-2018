﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class AudioData {

	// Author: Glenn Storm
	// This is the base class for audio data

	public	int						totalSounds = 15;

	public	string[]				soundName;
	public	AudioManager.aType[]	soundType;


	public	void	InitializeAudioData() {

		soundName = new string[ totalSounds ];
		soundType = new AudioManager.aType[ totalSounds ];

		ConfigureAudioData();
	}

	void ConfigureAudioData() {

		int 				index = 0;
		int					lastIndex = 0;
		AudioManager.aType 	sType = AudioManager.aType.Default;

		// TEMPLATE USE ONLY
		// Default
		sType = AudioManager.aType.Default;
		soundName[index] = "";
		// index++;
		// ...
		for ( int i=lastIndex; i<index; i++ ) {
			soundType[i] = sType;
		}
		lastIndex = index;

		// SFX UI
		sType = AudioManager.aType.SFXUI;
		soundName[index] = "SFX Heavy Attack Impact";
		index++;
		soundName[index] = "SFX Heavy Swing";
		index++;
		soundName[index] = "SFX Light Attack 1";
		index++;
		soundName[index] = "SFX Light Attack 2";
		index++;
		soundName[index] = "SFX Light Attack 3";
		index++;
		soundName[index] = "SFX Light Impact";
		index++;
		soundName[index] = "SFX Perfect Block Parry";
		index++;
		soundName[index] = "SFX Perfect Dodge";
		index++;
		soundName[index] = "SFX Power Cell Charge";
		index++;
		soundName[index] = "SFX UI Button Forward";
		index++;
		for ( int i=lastIndex; i<index; i++ ) {
			soundType[i] = sType;
		}
		lastIndex = index;

		// SFX Game
		/*
		sType = AudioManager.aType.SFXGame;
		soundName[index] = "SFX_Character_fall1";
		index++;
		for ( int i=lastIndex; i<index; i++ ) {
			soundType[i] = sType;
		}
		lastIndex = index;
		*/

		// Music Loop
		sType = AudioManager.aType.MusicLoop;
		soundName[index] = "Music Loop Menu";
		index++;
		soundName[index] = "Music Loop Battle";
		index++;
		for ( int i=lastIndex; i<index; i++ ) {
			soundType[i] = sType;
		}
		lastIndex = index;

		// Music Sting
		sType = AudioManager.aType.MusicSting;
		soundName[index] = "Music Sting 02";
		index++;
		soundName[index] = "Music Sting 04";
		index++;
		soundName[index] = "Music Sting Splash";
		index++;
		for ( int i=lastIndex; i<index; i++ ) {
			soundType[i] = sType;
		}
		lastIndex = index;

		// Voice
		/*
		sType = AudioManager.aType.Voice;
		soundName[index] = "Voice Tag Amazon Queen";
		index++;
		for ( int i=lastIndex; i<index; i++ ) {
			soundType[i] = sType;
		}
		lastIndex = index;
		*/
	}
}
