﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameData {

	// Author: Glenn Storm
	// This is the base class for game data

	public	bool		gameSaved;
	public	bool		gameChanged;
	public	float		gameTimeTotal;
	public	OptionsData	gameOptions = new OptionsData();


	public	void InitializeGame() {

		gameSaved = false;
		gameChanged = false;
		gameTimeTotal = 0f;
		gameOptions.InitializeOptionsData ();
	}

	public	void ConfigureGame() {


	}
}
