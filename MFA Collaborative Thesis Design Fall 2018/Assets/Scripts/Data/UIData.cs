﻿using System.Collections;
using UnityEngine;

public class UIData {

	// Author: Glenn Storm
	// This is the base class for UI data


	public	string		uiName;
	public	float		uiXPos;
	public	float		uiYPos;
	public	float		uiWidth;
	public	float		uiHeight;

}
