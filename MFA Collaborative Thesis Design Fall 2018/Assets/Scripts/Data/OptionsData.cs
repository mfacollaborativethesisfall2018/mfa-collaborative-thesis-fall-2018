﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class OptionsData {

	// Author: Glenn Storm
	// This is the base class for game options data

	public	enum HintLevel {
		none,
		basic,
		tutorial
	}

	public	bool		gamePaused;
	public	float		musicVolume;
	public	float		sfxVolume;
	public	float		voiceVolume;
	public	HintLevel	hintSetting;


	public	void InitializeOptionsData() {

		gamePaused = false;
		musicVolume = 1.0f;
		sfxVolume = 1.0f;
		voiceVolume = 1.0f;
		hintSetting = HintLevel.tutorial;
	}

	public	void ConfigureOptionsData() {

	}
}
