﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Author: Hendi Xu
/// Editor: Kevin Caton-Largent
/// This script controls the camera reseting to a position behind and above the player
/// _Targeting is Kevin's special edition for targeting integration
/// Main difference i auto grabbing the necessary variables from the scene and that the reset button is middle mouse
/// </summary>
public class CameraReset_Targeting : MonoBehaviour {

	public Transform target;
	private new Transform camera;

	public float posSpeed = 1.0F;
	public float rotSpeed = 1.0F;

	public CameraController_Targeting cameraController;
	// Use this for initialization
	void Start () 
	{
        if (camera == null || camera != GetComponent<Transform>())
            camera = GetComponent<Transform> ();
        if (target == null || target != GameObject.Find("Character").transform.Find("Camera"))
            target = GameObject.Find("Character").transform.Find("Camera");
        if (cameraController == null || cameraController != this.GetComponent<CameraController_Targeting>())
            cameraController = this.GetComponent<CameraController_Targeting>();
    }

    // Update is called once per frame
    void Update () 
	{
		

		// rotation movement
		camera.rotation = Quaternion.Lerp (camera.rotation, target.rotation, (rotSpeed * Time.deltaTime));

		// position movement
		camera.position = Vector3.Lerp (camera.position, target.position, (posSpeed * Time.deltaTime));

		if (Input.GetKeyUp (KeyCode.Mouse2) && !this.GetComponent<Targeting>().enabled) {
			
            Debug.Log("Camera Reset");
			cameraController.enabled = true;
			this.enabled = false;
		}
	}
}