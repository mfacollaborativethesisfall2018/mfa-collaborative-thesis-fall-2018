﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Author: Kevin Caton-Largent
/// Script has the camera orienting the enemy to face the player for testing purposes
/// </summary>
public class EnemyCam : MonoBehaviour
{
    public Transform targetPoint, enemyTrans;
    public Camera cameraOBJ;
    public float smoothFactor;
    private void Start()
    {
        targetPoint = GameObject.Find("Character").transform;
    }
    private void LateUpdate()
    {
        cameraOBJ.transform.LookAt(targetPoint);
        Quaternion enemySmoothRotation = Quaternion.LookRotation(targetPoint.position - enemyTrans.position);
        enemyTrans.rotation = Quaternion.Slerp(enemyTrans.rotation, enemySmoothRotation, Time.deltaTime * smoothFactor);
    }
}


