﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Author: Hendi Xu
/// Editor: Kevin Caton-Largent
/// This script controls the camera orbit around the player and is responsible for the camera following the player
/// _V3 is Kevin's special edition for targeting integration
/// </summary>
public class CameraController_V3 : MonoBehaviour
{

    ///
    /// Kevin's code block
    ///

    public TargetingV2 targetingScript;
    public Transform beforeTargeting;
    ///
    /// End of Kevin's code block
    ///
    public bool HideAndShowCursor = true;
    public bool LockRotationWhenRightClick = false;
    public bool UseBlurEffect = true;
    public bool UseFogEffect = true;
    public Transform target;//要控制的对象

    public float targetHeight = 1.0f;
    public float distance = 5.0f;

    public float maxDistance = 20;
    public float minDistance = 0.6f;

    public float xSpeed = 250.0f;
    public float ySpeed = 120.0f;

    public int yMinLimit = -80;
    public int yMaxLimit = 80;

    public int zoomRate = 40;

    public float rotationDampening = 3.0f;
    public float zoomDampening = 10.0f;

    private float x = 0.0f;
    private float y = 0.0f;
    private float currentDistance;
    private float desiredDistance;
    private float correctedDistance;
    //private bool grounded = false;

    public Transform player; 
    public float speed = 5f; 
    //Vector3 distanceB;   



	public Transform targetCamera;
	//private new Transform camera;

	public float posSpeed = 1.0F;
	public float rotSpeed = 1.0F;

	public CameraReset_V2 cameraResetLocation;
    void Start()
    {
        ///
        /// Kevin's code block
        ///
        if (targetingScript == null || targetingScript != this.GetComponent<TargetingV2>())
            targetingScript = this.GetComponent<TargetingV2>();
        if (player == null || player != GameObject.Find("Character").transform)      
            player = GameObject.Find("Character").transform;
        if (target == null || target != GameObject.Find("Character").transform)
            target = GameObject.Find("Character").transform;
        if (cameraResetLocation == null || cameraResetLocation != this.GetComponent<CameraReset_V2>())
            cameraResetLocation = this.GetComponent<CameraReset_V2>();
        if (targetCamera == null || targetCamera != GameObject.Find("Character").transform.Find("Targeting Camera"))
            targetCamera = GameObject.Find("Character").transform.Find("Targeting Camera");
        targetingScript.targetReticleStart = GameObject.Find("Player HUD").transform.Find("Target Reticle");

        ///
        /// End of Kevin's code block
        /// 

        //Screen.lockCursor = true;
		Cursor.lockState = CursorLockMode.Locked;
        //Cursor.lockState = 
        Vector3 angles = transform.eulerAngles;
        x = angles.x;
        y = angles.y;

        currentDistance = distance;
        desiredDistance = distance;
        correctedDistance = distance - 0.2f;

        // Make the rigid body not change rotation
        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().freezeRotation = true;

        //distanceB = transform.position - player.position;

		//camera = GetComponent<Transform> ();
    }

    void LateUpdate()
    {
        // Don't do anything if target is not defined
        if (!target)
        {
            GameObject go = GameObject.Find("Player");
            target = go.transform;
            transform.LookAt(target);
            return;
        }
        // If either mouse buttons are down, let the mouse govern camera position
        if (LockRotationWhenRightClick == false)
        {
            x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
            y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
        }
        if (Input.GetMouseButton(0))
        {
            if (LockRotationWhenRightClick == false)
            {
                x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
                y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
            }
        }
        y = ClampAngle(y, yMinLimit, yMaxLimit);

		/*print ("X:"+ x);
		print ("y:"+ y);*/


        // set camera rotation
        Quaternion rotation = Quaternion.Euler(y, x, 0);

        // calculate the desired distance
        desiredDistance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * zoomRate * Mathf.Abs(desiredDistance);
        desiredDistance = Mathf.Clamp(desiredDistance, minDistance, maxDistance);
        correctedDistance = desiredDistance;

        // calculate desired camera position
        Vector3 position = target.position - (rotation * Vector3.forward * desiredDistance + new Vector3(0, -targetHeight, 0));

        // check for collision using the true target's desired registration point as set by user using height
        RaycastHit collisionHit;
        Vector3 trueTargetPosition = new Vector3(target.position.x, target.position.y + targetHeight, target.position.z);

        // if there was a collision, correct the camera position and calculate the corrected distance
        bool isCorrected = false;
        if (Physics.Linecast(trueTargetPosition, position, out collisionHit))
        {
            if (collisionHit.transform.name != target.name)
            {
                position = collisionHit.point;
                correctedDistance = Vector3.Distance(trueTargetPosition, position);
                isCorrected = true;
            }
        }

        // For smoothing, lerp distance only if either distance wasn't corrected, or correctedDistance is more than currentDistance
        currentDistance = !isCorrected || correctedDistance > currentDistance ? Mathf.Lerp(currentDistance, correctedDistance, Time.deltaTime * zoomDampening) : correctedDistance;


       //Vector3 targetCamPos = player.position + distanceB;
       // transform.position = Vector3.Lerp(transform.position, targetCamPos, speed * Time.deltaTime);

        transform.rotation = rotation;
        // recalculate position based on the new currentDistance
         position = target.position - (rotation * Vector3.forward * currentDistance + new Vector3(0, -targetHeight - 0.05f, 0));
       // transform.position = position;
        transform.position = Vector3.Lerp(transform.position, position, speed * Time.deltaTime);

        /// Q is the the targeting button, middle mouse is the refocus while in free roam - Kevin
		if (Input.GetKeyDown (KeyCode.Mouse2) && !this.GetComponent<TargetingV2>().enabled || Input.GetKeyDown (KeyCode.Tab)  && !this.GetComponent<TargetingV2>().enabled) {
			/*// position movement
			camera.position = Vector3.Lerp (camera.position, targetCamera.position, (posSpeed * Time.deltaTime));
			position = targetCamera.position;

			// rotation movement
			camera.rotation = Quaternion.Lerp (camera.rotation, targetCamera.rotation, (rotSpeed * Time.deltaTime));
			rotation = targetCamera.rotation;*/
			cameraResetLocation.enabled = true;

            this.enabled = false;
		}
        else if (Input.GetKeyDown (KeyCode.Q)  && !this.GetComponent<TargetingV2>().enabled)
        {
            player.GetComponent<CubeMovement_V2>().rotatePlayer = !player.GetComponent<CubeMovement_V2>().rotatePlayer;
            beforeTargeting = target;
            target = targetCamera;
            targetingScript.enabled = true;
            GameObject.Find("Player HUD").transform.Find("Enemy Health").gameObject.SetActive(true);
            this.enabled = false;
        }
    }


    private static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }


}


