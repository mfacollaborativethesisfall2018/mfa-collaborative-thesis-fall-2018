﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Author: Hendi Xu
/// Editor: Kevin Caton-Largent
/// This script is Hendi's temporary movement script to give the player movement while player controls are developed
/// _Targeting is Kevin's special edition for targeting integration
/// </summary>
public class CubeMovement_Targeting : MonoBehaviour {

    public bool rotatePlayer;
    public float horizontalSpeed = 1f;
    //float verticalSpeed = 2.0f;
	public float moveSpeed;
    /*public float health;
    public bool death;*/
    // Use this for initialization
    void Start () {
        //death = false;
       /* if (health <= 0f)
            health = 25f;*/
        rotatePlayer = true;
	}
	
	// Update is called once per frame
	void Update () {
        float h = horizontalSpeed * Input.GetAxis("Mouse X");
        // float v = verticalSpeed * Input.GetAxis("Mouse Y");
        if (rotatePlayer)
            transform.Rotate(0, h, 0);


		if(Input.GetKey(KeyCode.A))
		{
			transform.position -= transform.right * Time.deltaTime * moveSpeed;
		}


		if (Input.GetKey(KeyCode.D))
		{
			transform.position += transform.right * Time.deltaTime * moveSpeed;
		}


		if (Input.GetKey(KeyCode.W))
		{
			transform.position += transform.forward * Time.deltaTime * moveSpeed;
		}


		if (Input.GetKey(KeyCode.S))
		{
			transform.position -= transform.forward * Time.deltaTime * moveSpeed;
		}
    }
}
