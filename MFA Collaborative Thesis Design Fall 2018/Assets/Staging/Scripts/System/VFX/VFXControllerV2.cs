﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Author: Kevin Caton-Largent
/// VFX controller controls when vfx appears (wip)
/// </summary>
public class VFXControllerV2 : MonoBehaviour {
    public List<ParticleSystem> dodgeEffect;
    public ParticleSystem blockEffect;
    public GameObject playerHitEffect;
    public GameObject playerHeavyTrail;
    public ParticleSystem deathEffect;
    public float dodgeTimer;
    public float playerHitTimer;
    public float playerHeavyAttackTimer;
    public float defaultPlayerHitInterval;
    public float defaultHeavyAttackInterval;
    public float defaultDodgeInterval;
    public bool dodgeActivated;
    public bool heavyAttackActivated;
    public bool playerHitActivated;
    public bool enemyInRange;
    public Transform target;
    /// <summary>
    /// Helper function to reset player heavy attack timers
    /// </summary>
    /// <param name="duration">new duration for player heavy attack timers</param>
    public void ResetPlayerHeavyTimer(float duration)
    {
        defaultHeavyAttackInterval = duration;
        playerHeavyAttackTimer = defaultHeavyAttackInterval;
    }

    public void ActivateHeavyTrail()
    {
        heavyAttackActivated = !heavyAttackActivated;
        if (heavyAttackActivated)
        {
            Debug.Log("Player heavy attack");
            playerHeavyTrail.SetActive(true);
        }
        else
        {
            Debug.Log("Player has stopped heavy attack");
            playerHeavyTrail.SetActive(false);
        }
    }

    /// <summary>
    /// Helper function to reset player hit timers
    /// </summary>
    /// <param name="duration">new duration for player hit timers</param>
    public void ResetPlayerHitTimer(float duration)
    {
        defaultPlayerHitInterval = duration;
        playerHitTimer = defaultPlayerHitInterval;
    }
   
    public void ActivatePlayerHit()
    {
        playerHitActivated = !playerHitActivated;
        if (playerHitActivated)
        {
            Debug.Log("Player has been hit");
            //playerHitEffect.Play();
            playerHitEffect.SetActive(true);
        }
        else
        {
            Debug.Log("Player hit vfx has been stopped");
            //playerHitEffect.Stop();
            playerHitEffect.SetActive(false);
        }
        //hitEffect.Play();

    }

     public void ActivateBlock()
    {
        Debug.Log("Player Block");
        if (!blockEffect.isPlaying)
            blockEffect.Play();
    }
    /// <summary>
    /// Helper function to reset dodge timers
    /// </summary>
    /// <param name="duration">new duration for dodge timers</param>
    public void ResetDodgeTimer(float duration)
    {
        defaultDodgeInterval = duration;
        dodgeTimer = defaultDodgeInterval;
    }
    
    public void ToggleDodge()
    {
        
        dodgeActivated = !dodgeActivated;
        Debug.Log("Player Dodge");
        foreach (ParticleSystem current in dodgeEffect)
        {
            if (dodgeActivated)
                current.Play();

            else
                current.Stop();
        }



    }
    public void ActivateDeath()
    {
        Debug.Log("Player Death");
        if (!deathEffect.isPlaying)
            deathEffect.Play();

    }
    private void Start()
    {
        dodgeActivated = false;
        if (defaultDodgeInterval <= 0f)
            defaultDodgeInterval = 3f;
        if (defaultPlayerHitInterval <= 0f)
            defaultPlayerHitInterval = 5f;
        if (defaultHeavyAttackInterval <= 0f)
            defaultHeavyAttackInterval = 3f;
    }
    private void Update()
    {
        if (dodgeActivated)
        {
            dodgeTimer -= Time.deltaTime;
            if (dodgeTimer <= 0f)
            {
                dodgeTimer = defaultDodgeInterval;
                ToggleDodge();
            }
        }
        if (playerHitActivated)
        {
            playerHitTimer -= Time.deltaTime;
            if (playerHitTimer <= 0f)
            {
                playerHitTimer = defaultPlayerHitInterval;
                ActivatePlayerHit();
            }
        }
        if (heavyAttackActivated)
        {
            playerHeavyAttackTimer -= Time.deltaTime;
            if (playerHeavyAttackTimer <= 0f)
            {
                playerHeavyAttackTimer = defaultHeavyAttackInterval;
                ActivateHeavyTrail();
            }
        }
        /*
        if (hitActivated)
        {
            hitTimer -= Time.deltaTime;
            if (hitTimer <= 0f)
            {
                hitTimer = defaultHitInterval;
                hitActivated = false;
            }
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            ActivateHit();
            hitTimer = defaultHitInterval;
        }
        else if (Input.GetKeyUp(KeyCode.E))
        {
            ActivateBlock();
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            if (!dodgeActivated)
            {
                ToggleDodge();
                timer = defaultDodgeInterval;
            }
        }*/

    }

}
