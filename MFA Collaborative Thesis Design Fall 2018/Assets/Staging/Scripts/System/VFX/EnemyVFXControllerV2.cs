﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Author: Kevin Caton-Largent
/// VFX controller controls when vfx appears (wip)
/// </summary>
public class EnemyVFXControllerV2 : MonoBehaviour {
    public List<ParticleSystem> dodgeEffect;
    public ParticleSystem blockEffect;
    public GameObject enemyHitEffect;
    public ParticleSystem deathEffect;
    public float dodgeTimer;
    public float defaultDodgeInterval;
    public float enemyHitTimer;
    public float defaultEnemyHitInterval;
    public bool dodgeActivated;
    public bool enemyHitActivated;

    /// <summary>
    /// Helper function to reset enemy hit timers
    /// </summary>
    /// <param name="duration">new duration for enemy hit timers</param>
    public void ResetEnemyHitTimer(float duration)
    {
        defaultEnemyHitInterval = duration;
        enemyHitTimer = defaultEnemyHitInterval;
    }


    public void ActivateEnemyHit()
    {
        enemyHitActivated = !enemyHitActivated;
        if (enemyHitActivated)
        {
            // Debug.Log("Enemy has been hit");
            //playerHitEffect.Play();
            enemyHitEffect.SetActive(true);

        }
        else
        {
            // Debug.Log("Enemy hit vfx has been stopped");
            //playerHitEffect.Stop();
            enemyHitEffect.SetActive(false);
        }
        //hitEffect.Play();

    }


    public void ActivateBlock()
    {
        // Debug.Log("Enemy Parry");
        if (!blockEffect.isPlaying)
            blockEffect.Play();
    }
    /// <summary>
    /// Helper function to reset dodge timers
    /// </summary>
    /// <param name="duration">new duration for dodge timers</param>
    public void ResetDodgeTimer(float duration)
    {
        defaultDodgeInterval = duration;
        dodgeTimer = defaultDodgeInterval;
    }

    public void ToggleDodge()
    {

        dodgeActivated = !dodgeActivated;
        // Debug.Log("Dodge");
        foreach (ParticleSystem current in dodgeEffect)
        {
            if (dodgeActivated)
                current.Play();

            else
                current.Stop();
        }



    }
    public void ActivateDeath()
    {
        // Debug.Log("Enemy Death");
        if (!deathEffect.isPlaying)
            deathEffect.Play();

    }
    private void Start()
    {
        dodgeActivated = false;
        if (defaultDodgeInterval <= 0f)
            defaultDodgeInterval = 5f;
    }

    private void Update()
    {
        if (dodgeActivated)
        {
            dodgeTimer -= Time.deltaTime;
            if (dodgeTimer <= 0f)
            {
                dodgeTimer = defaultDodgeInterval;
                ToggleDodge();
            }
        }
        if (enemyHitActivated)
        {
            enemyHitTimer -= Time.deltaTime;
            if (enemyHitTimer <= 0f)
            {
                enemyHitTimer = defaultEnemyHitInterval;
                ActivateEnemyHit();
            }
        }
    }

}
