﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Author: Kevin Caton-Largent
/// This script is attached to the eyesight collider object and informs the enemy if the player has entered eyesight
/// </summary>
public class EnemyCombatRange : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Player has entered range");
        if (other.GetComponent<CubeMovement_V2>() != null)
        {
            this.GetComponentInParent<EnemyController>().playerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("Player has exited range");
        if (other.GetComponent<CubeMovement_V2>() != null)
        {
           this.GetComponentInParent<EnemyController>().playerInRange = false;
        }
    }
}
