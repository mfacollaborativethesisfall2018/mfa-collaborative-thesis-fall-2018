﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Author: Kevin Caton-Largent
/// Controls the eyesight of the enemy
/// </summary>
public class EnemyEyesight : MonoBehaviour {
    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CubeMovement_Targeting>() != null)
        {
            
            Debug.Log("Player Sighted!");
            this.GetComponentInParent<EnemyController>().playerInSight = true;
        }
    }

    /*void OnTriggerExit(Collider other)
    {
        Debug.Log("Trigger Exit");
        if (other.GetComponent<CubeMovement_Targeting>() != null)
        {
           this.GetComponentInParent<EnemyController>().playerInSight = false;
        }
    }*/

}
