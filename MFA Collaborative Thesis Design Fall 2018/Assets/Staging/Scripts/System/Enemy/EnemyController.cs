﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
/// <summary>
/// Author: Kevin Caton-Largent
/// This dictates the enemy behavior and is responsible for the control of the enemy
/// </summary>
public class EnemyController : MonoBehaviour
{
    public enum EnableEnemyCam
    {
        enable,
        disable
    }
    public enum EnableAI
    {
        enable,
        disable
    }
    public bool playerInSight;
    public bool playerInRange;
    public bool blockActivated;
    public bool parryActivated;
    public bool dodgeActivated;
    public Vector3 distanceFromPlayer;
    public Transform player;
    public NavMeshAgent navAgent;
    public EnableEnemyCam camStatus;
    private EnableEnemyCam prevCamStatus;
    public EnableAI aiStatus;
    private EnableAI prevAIStatus;
    private Camera enemyCam;
    private EnemyData myStats;
    private EnemyVFXController myVFX;
    public VFXController playerAttacking;
    public PlayerHUD playerHP;
    // Use this for initialization
    void Start()
    {

        blockActivated = false;
        parryActivated = false;
        if (myStats == null || myStats != this.GetComponent<EnemyData>())
            myStats = this.GetComponent<EnemyData>();

        if (myVFX == null || myVFX != this.GetComponent<EnemyVFXController>())
            myVFX = this.GetComponent<EnemyVFXController>();

        if (playerAttacking == null || playerAttacking != GameObject.Find("Character").GetComponent<VFXController>())
            playerAttacking = GameObject.Find("Character").GetComponent<VFXController>();

        if (playerHP == null || playerHP != GameObject.Find("Player HUD").GetComponent<PlayerHUD>())
            playerHP = GameObject.Find("Player HUD").GetComponent<PlayerHUD>();

        prevCamStatus = camStatus;
        enemyCam = this.transform.Find("Enemy Cam").GetComponent<Camera>();
        if (camStatus == EnableEnemyCam.disable)
            ToggleCamera();
        else if (camStatus == EnableEnemyCam.enable)
            ToggleCamera();

        playerInSight = false;
        playerInRange = false;
        if (player == null || player!=GameObject.Find("Character").transform)
            player = GameObject.Find("Character").transform;
        if (navAgent == null || navAgent != this.GetComponent<NavMeshAgent>())
            navAgent = this.GetComponent<NavMeshAgent>();

        if (aiStatus == EnableAI.disable)
            ToggleAI();
        else if (aiStatus == EnableAI.enable)
            ToggleAI();

    }
    

    // Update is called once per frame
    void Update()
    {
        if (prevCamStatus != camStatus)
            ToggleCamera();
        
        if (prevAIStatus != aiStatus)            
            ToggleAI();
        
        if (playerInSight && navAgent.enabled)
        {
            navAgent.SetDestination(player.position + distanceFromPlayer);
            if (myStats.delay != 0f)
                myStats.delay -= Time.deltaTime;
            if (myStats.currentType == EnemyData.EnemyType.fast && playerInRange)
            {
                if (myStats.delay <= 0f)
                {
                    EnemyFastBehavior();
                    myStats.delay = myStats.defFastDelay;
                }
            }
            else if (myStats.currentType == EnemyData.EnemyType.heavy && playerInRange)
            {
                if (myStats.delay <= 0f)
                {
                    EnemyHeavyBehavior();
                    myStats.delay = myStats.defHeavyDelay; 
                }
            }
            if (myStats.health <= 0f)
            {
                myVFX.ActivateDeath();
            }

        }
    }
    /// <summary>
    /// Toggles the test camera
    /// </summary>
    private void ToggleCamera()
    {
        prevCamStatus = camStatus;
        if (camStatus == EnableEnemyCam.disable)
            enemyCam.gameObject.SetActive(false);
        else if (camStatus == EnableEnemyCam.enable)
            enemyCam.gameObject.SetActive(true);
    }
    /// <summary>
    /// Toggles the main ai, in the case you want a dummy stand in
    /// </summary>
    private void ToggleAI()
    {
        prevAIStatus = aiStatus;
        if (aiStatus == EnableAI.disable)       
            navAgent.enabled = false;
       
        else if (aiStatus == EnableAI.enable)       
            navAgent.enabled = true;    
    }
    /// <summary>
    /// Cause the enemy to dodge
    /// </summary>
    private void Dodge()
    {
        if (!myVFX.dodgeActivated)
        {
            myVFX.ToggleDodge();
            myVFX.timer = myVFX.defaultDodgeInterval;
        }
    }

    /// <summary>
    /// Cause the enemy to Block
    /// </summary>
    private void Block()
    {
        // Debug.Log("Enemy Blocked your attack");
        blockActivated = false;
    }

    /// <summary>
    /// Cause the enemy to Parry
    /// </summary>
    private void Parry()
    {
        myVFX.ActivateParry();
        parryActivated = false;
    }

    /// <summary>
    /// Cause the enemy to Light Attack
    /// </summary>
    private void LightAttack()
    {
        playerHP.Reduce(myStats.lightAttackDMG);
        //myVFX.ActivateHit();
    }

    /// <summary>
    /// Cause the enemy to Heavy Attack
    /// </summary>
    private void HeavyAttack()
    {
        playerHP.Reduce(myStats.heavyAttackDMG);
        // Debug.Log("Enemy Heavy Attack");
        //myVFX.ActivateHeavyHit();
    }

    /// <summary>
    /// The main ai behavior for a fast enemy
    /// </summary>
    private void EnemyFastBehavior()
    {
        // seen by player?
        // attack player if can't be seen otherwise \/


        // what is the state of attack   start < x < middle, x is current player attack state
        // dodge if x is in this range
        // attack if x is outside this range
        if ((playerAttacking.hitTimer > 0f) && (playerAttacking.hitTimer <= (playerAttacking.defaultHitInterval / 2f)))
        {
            Dodge();
        }
        else
            LightAttack();
        // check death state

    }

    

    /// <summary>
    /// The main ai behavior for a heavy enemy
    /// </summary>
    private void EnemyHeavyBehavior()
    {
        // seen by player?
        // heavy attack if can't be seen otherwise \/
        // what is the state of the attack
        // start or middle = block
        // finish = parry
        if ((playerAttacking.hitTimer > 0f) && (playerAttacking.hitTimer <= (playerAttacking.defaultHitInterval / 2f)))
        {
            blockActivated = true;
            Block();
            if (!blockActivated)
                LightAttack();
        }
        else if ((playerAttacking.hitTimer > (playerAttacking.defaultHitInterval / 2f)) && (playerAttacking.hitTimer <= (playerAttacking.defaultHitInterval - (playerAttacking.defaultHitInterval-1f))) )
        {
            parryActivated = true;
            Parry();
            if (!parryActivated)
                HeavyAttack();
        }

        // after blocks, attack player
        // after parries, heavy attack player
        // check death state
    }

    
    
}
