﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Author: Kevin Caton-Largent
/// This script controls the targeting behavior of the player camera
/// </summary>
public class Targeting : MonoBehaviour {
        
    public Transform targetPoint, playerTrans;
	public Camera cameraOBJ;
    public Camera targetingCamera;
	public float smoothTime;
    /*public List<GameObject> targetReticle;
    public List<Transform> targetReticleStartPoint;*/
    public Transform targetReticle;
    public Transform targetReticleStart;
    public Transform enemyHealth;
	//private Vector3 velocity = Vector3.zero;
	private void Start()
	{
      
		if (cameraOBJ == null || cameraOBJ != GameObject.FindObjectOfType<CameraController_Targeting>().GetComponent<Camera>())
            cameraOBJ = GameObject.FindObjectOfType<CameraController_Targeting>().GetComponent<Camera>();

        if (targetingCamera == null || targetingCamera != GameObject.Find("Character").transform.Find("Targeting Camera").GetComponent<Camera>())
            targetingCamera = GameObject.Find("Character").transform.Find("Targeting Camera").GetComponent<Camera>();

        if (playerTrans == null || playerTrans != GameObject.Find("Character").transform)
            playerTrans = GameObject.Find("Character").transform;

        if (targetReticle == null || targetReticle != GameObject.Find("Player HUD").transform.Find("Target Reticle"))
            targetReticle = GameObject.Find("Player HUD").transform.Find("Target Reticle");

        if (enemyHealth == null || enemyHealth != GameObject.Find("Player HUD").transform.Find("Enemy Health"))
            enemyHealth = GameObject.Find("Player HUD").transform.Find("Enemy Health");

        //enemyHealth.transform.gameObject.SetActive(true);
        DetermineNearestEnemy();

               
    }

    public void DetermineNearestEnemy()
	{
        float closestDistance = Mathf.Infinity;
        EnemyData[] enemies = GameObject.FindObjectsOfType<EnemyData>();
        foreach (EnemyData current in enemies)
        {
            float distance = (current.transform.Find("Target Point").position - playerTrans.position).sqrMagnitude;
            Debug.Log("Current Enemy: " + current.GetComponent<EnemyData>().enemyID + ", distance: " + distance);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                targetPoint = current.transform.Find("Target Point");
            }
        }
        Debug.DrawLine(playerTrans.position, targetPoint.position, Color.red);
        enemyHealth.transform.position = cameraOBJ.WorldToScreenPoint(targetPoint.position);

        //Transform[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
    }
    public void SelectNextEnemy()
    {
        EnemyData[] enemies = GameObject.FindObjectsOfType<EnemyData>();
        foreach (EnemyData current in enemies)
        {
            
            if (this.targetPoint.GetComponentInParent<EnemyData>().enemyID != current.GetComponent<EnemyData>().enemyID )
            {
                targetPoint = current.transform.Find("Target Point");
                break;
            }
        }
        Debug.DrawLine(playerTrans.position, targetPoint.position, Color.red);

        //Transform[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
    }

    /// <summary>
    /// Player controls for managing target selection
    /// </summary>
    public void TargetingControls()
	{
		if (Input.GetKeyDown(KeyCode.Q))
		{

            targetReticle.transform.position = cameraOBJ.WorldToScreenPoint(targetReticleStart.position);
            enemyHealth.transform.gameObject.SetActive(false);
            playerTrans.GetComponent<CubeMovement_Targeting>().rotatePlayer = !playerTrans.GetComponent<CubeMovement_Targeting>().rotatePlayer;
            //cameraOBJ.transform.position = cameraOBJ.GetComponent<CameraController_Targeting>().beforeTargeting.position;
            this.transform.GetComponent<CameraController_Targeting>().enabled = true;
            this.enabled = false;
        }
        else if (Input.GetKeyUp(KeyCode.Mouse2))
		{            
			Debug.Log ("Switching Target");
            SelectNextEnemy();
        }
    }
	private void LateUpdate()
	{
        //Debug.Log("Time.deltatime: " + Time.deltaTime);
       
        TargetingControls();
        targetReticle.transform.position = cameraOBJ.WorldToScreenPoint(targetPoint.position);
        enemyHealth.transform.position = cameraOBJ.WorldToScreenPoint(targetPoint.position);

        cameraOBJ.transform.position = targetingCamera.transform.position;
        cameraOBJ.transform.LookAt(targetPoint);
        Quaternion playerSmoothRotation = Quaternion.LookRotation(targetPoint.position - playerTrans.position);
        playerTrans.rotation = Quaternion.Slerp(playerTrans.rotation, playerSmoothRotation, 1); // substitute time.deltaTime with 1

    }




}
