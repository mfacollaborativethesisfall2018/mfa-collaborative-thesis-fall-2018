﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController1 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		lockCamera = true;
	}
    public float RotateSpeed = 1;
    public GameObject thirdPersonPlayer;    //角色
    public GameObject FollowCamera;         //跟随相机
    public float CameraSmoothTime = 0;
    private Vector3 velocity = Vector3.zero;
	public Vector3 cameraLocation;

	public bool lockCamera=true;
    void Update()
    {
       
        float x = RotateSpeed * Input.GetAxis("Mouse X");
		float y = RotateSpeed * Input.GetAxis("Mouse Y");
     
        FollowCamera.transform.rotation = Quaternion.Euler(
            FollowCamera.transform.rotation.eulerAngles +
            Quaternion.AngleAxis(x, Vector3.up).eulerAngles
        );

		FollowCamera.transform.rotation = Quaternion.Euler(
			FollowCamera.transform.rotation.eulerAngles +
			Quaternion.AngleAxis(y, Vector3.left).eulerAngles
		);

		if (!lockCamera) {
			thirdPersonPlayer.transform.rotation = Quaternion.Euler (
				thirdPersonPlayer.transform.rotation.eulerAngles +
				Quaternion.AngleAxis (x, Vector3.up).eulerAngles
			);
		}
		// Camera relative location compared to player location
       // Vector3 TargetCameraPosition = thirdPersonPlayer.transform.TransformPoint(new Vector3(0, 0.02f, -0.05f));
		Vector3 TargetCameraPosition = thirdPersonPlayer.transform.TransformPoint(cameraLocation);

        FollowCamera.transform.position = Vector3.SmoothDamp(
            FollowCamera.transform.position,
            TargetCameraPosition,
            ref velocity,
            CameraSmoothTime, 
            Mathf.Infinity,
            Time.deltaTime
        );
    }

}
