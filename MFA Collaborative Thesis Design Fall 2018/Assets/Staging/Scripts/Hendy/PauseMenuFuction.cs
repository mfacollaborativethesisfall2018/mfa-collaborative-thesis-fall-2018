﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuFuction : MonoBehaviour {


    public bool isPaused;
    public GameObject pauseMenu;
    


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                pauseMenu.SetActive(false);
                isPaused = false;
                Time.timeScale = 1;
            }
            else
            {
                pauseMenu.SetActive(true);
                isPaused = true;
                //Time.timeScale = 0;
            }
        }
    }

    public void RenturnToMenu()
    {
        SceneManager.LoadScene("playerPrototypeMenu");
    }

    public void HidePauseMenu()
    {
        pauseMenu.SetActive(false);
        isPaused = false;
        //Time.timeScale = 1;
    }

}
