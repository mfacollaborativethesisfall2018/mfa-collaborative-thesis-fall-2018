﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {


    public GameObject main;
    public GameObject level;
    public GameObject options;
    public GameObject credit;
    public GameObject exit;
    public GameObject creditReturn;


    public GameObject level1Picture;
    public GameObject level2Picture;

    public GameObject enterGameUI;
    public int flagForLevelChoose;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
 

    public void LevelShowAndOff()
    {
        options.SetActive(false);
       // credit.SetActive(false);
        if(level.activeSelf )
        {
            level.SetActive(false);
            level2Picture.SetActive(false);
            level1Picture.SetActive(false);
        }
        else
        {
            level.SetActive(true);
        }
    }


    public void OptionsShowAndOff()
    {
        level.SetActive(false);
        level2Picture.SetActive(false);
        level1Picture.SetActive(false);
        //credit.SetActive(false);
        if (options.activeSelf)
        {
            options.SetActive(false);
        }
        else
        {
            options.SetActive(true);
        }
    }

    public void CreditShow()
    {
        main.SetActive(false);
        level.SetActive(false);
        level2Picture.SetActive(false);
        level1Picture.SetActive(false);
        options.SetActive(false);
        credit.SetActive(true);
    }

    public void CreditReturn()
    {
        main.SetActive(true);
        credit.SetActive(false);
    }

    public void ShowLevel1()
    {
        level1Picture.SetActive(true);
        level2Picture.SetActive(false);
        flagForLevelChoose = 1;
    }
    public void ShowLevel2()
    {
        level2Picture.SetActive(true);
        level1Picture.SetActive(false);
        flagForLevelChoose = 2;
    }

    public void ShouEnterGameUI()
    {
        main.SetActive(false);
        level.SetActive(false);
        level2Picture.SetActive(false);
        level1Picture.SetActive(false);
        enterGameUI.SetActive(true);
    }

    public void Comfirm()
    {
        if(flagForLevelChoose==1)
        {

        }
        else if(flagForLevelChoose==2)
        {

        }
    }

    public void ReturnToMain()
    {
        main.SetActive(true);
        enterGameUI.SetActive(false);
    }
}
