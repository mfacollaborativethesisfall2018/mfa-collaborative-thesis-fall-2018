﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraReset : MonoBehaviour {

	public Transform target;
	private new Transform camera;

	public float posSpeed = 1.0F;
	public float rotSpeed = 1.0F;

	public CameraController2 cameraController;
	// Use this for initialization
	void Start () 
	{
		camera = GetComponent<Transform> ();
	}

	// Update is called once per frame
	void Update () 
	{
		

		// rotation movement
		camera.rotation = Quaternion.Lerp (camera.rotation, target.rotation, (rotSpeed * Time.deltaTime));

		// position movement
		camera.position = Vector3.Lerp (camera.position, target.position, (posSpeed * Time.deltaTime));

		if (Input.GetKeyUp (KeyCode.Q)) {
			

			cameraController.enabled = true;
			this.enabled = false;
		}
	}
}