﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour {

    public float horizontalSpeed = 1f;
    //float verticalSpeed = 2.0f;
	public float moveSpeed;
    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
        /*if(Input.GetKey(KeyCode.A))
            {
            Vector3 newMove = this.gameObject.transform.position;
			newMove.x -= moveSpeed;
            this.gameObject.transform.position = newMove;
        }


        if (Input.GetKey(KeyCode.D))
        {
            Vector3 newMove = this.gameObject.transform.position;
			newMove.x += moveSpeed;
            this.gameObject.transform.position = newMove;
        }


        if (Input.GetKey(KeyCode.W))
        {
            Vector3 newMove = this.gameObject.transform.position;
			newMove.z += moveSpeed;
            this.gameObject.transform.position = newMove;
        }


        if (Input.GetKey(KeyCode.S))
        {
            Vector3 newMove = this.gameObject.transform.position;
			newMove.z -= moveSpeed;
            this.gameObject.transform.position = newMove;
        }*/

        /*if (Input.GetKeyDown(KeyCode.A))
        {
            Vector3 newMove = this.gameObject.transform.position;
            newMove.x -= moveSpeed;
            this.gameObject.transform.position = newMove;
        }


        if (Input.GetKeyDown(KeyCode.D))
        {
            Vector3 newMove = this.gameObject.transform.position;
            newMove.x += moveSpeed;
            this.gameObject.transform.position = newMove;
        }


        if (Input.GetKeyDown(KeyCode.W))
        {
            Vector3 newMove = this.gameObject.transform.position;
            newMove.z += moveSpeed;
            this.gameObject.transform.position = newMove;
        }


        if (Input.GetKeyDown(KeyCode.S))
        {
            Vector3 newMove = this.gameObject.transform.position;
            newMove.z -= moveSpeed;
            this.gameObject.transform.position = newMove;
        }*/
         float h = horizontalSpeed * Input.GetAxis("Mouse X");
        // float v = verticalSpeed * Input.GetAxis("Mouse Y");
         transform.Rotate(0, h, 0);


		if(Input.GetKey(KeyCode.A))
		{
			transform.position -= transform.right * Time.deltaTime * moveSpeed;
		}


		if (Input.GetKey(KeyCode.D))
		{
			transform.position += transform.right * Time.deltaTime * moveSpeed;
		}


		if (Input.GetKey(KeyCode.W))
		{
			transform.position += transform.forward * Time.deltaTime * moveSpeed;
		}


		if (Input.GetKey(KeyCode.S))
		{
			transform.position -= transform.forward * Time.deltaTime * moveSpeed;
		}
    }
}
