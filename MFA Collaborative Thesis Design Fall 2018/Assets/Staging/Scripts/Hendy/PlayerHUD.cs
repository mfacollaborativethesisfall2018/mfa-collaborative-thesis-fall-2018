﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour {

    public GameObject PlayerHP;
    public GameObject playerCells;
    public VFXController vfxMaster;
    public float currentHP;
    public float targetHP;
    public float maxHP;

    public float currentCell;
    public float targetCell;
    public float maxCell;
    public bool death;

	public float speedOfBarProcess;

    /// <summary>
    /// Set of enemy specific hud elements
    /// </summary>
    public GameObject EnemyHP;
    public TargetingV2 tScript;
    public int oldID;
    public int currentID;
    public EnemyVFXController enemyVFX;
    public float enemyCurrentHP;
    public float enemyTargetHP;
    public float enemyMaxHP;
    /// <summary>
    /// End of enemy specific hud elements
    /// </summary>

    // Use this for initialization
    void Start () {
        death = false;
        if (vfxMaster == null || vfxMaster != GameObject.Find("Character").GetComponent<VFXController>())
            vfxMaster = GameObject.Find("Character").GetComponent<VFXController>();
        if (EnemyHP == null || EnemyHP != GameObject.Find("Player HUD").transform.Find("Enemy Health").transform.Find("EnemyInnerRing").gameObject)
            EnemyHP = GameObject.Find("Player HUD").transform.Find("Enemy Health").transform.Find("EnemyInnerRing").gameObject;
        if (tScript == null || tScript != GameObject.Find("Targeting Camera").GetComponent<TargetingV2>())
            tScript = GameObject.Find("Targeting Camera").GetComponent<TargetingV2>();
    }
    public void SetEnemy()
    {
        oldID = tScript.targetPoint.GetComponentInParent<EnemyData>().enemyID;
        enemyVFX = tScript.targetPoint.GetComponentInParent<EnemyVFXController>();
    }
    public void InitializeValues()
    {
        enemyCurrentHP = tScript.targetPoint.GetComponentInParent<EnemyData>().health;
        enemyTargetHP = tScript.targetPoint.GetComponentInParent<EnemyData>().health;
        enemyMaxHP = tScript.targetPoint.GetComponentInParent<EnemyData>().maxHealth;           
    }
    public void UpdateEnemyValues()
    {
        Debug.Log("Current ID " + tScript.targetPoint.GetComponentInParent<EnemyData>().enemyID);
        if (oldID == 0)
        {
            SetEnemy();
            InitializeValues();
        }
        if (tScript.targetPoint.GetComponentInParent<EnemyData>() != null && tScript.enabled)
        {
            if (tScript.targetPoint.GetComponentInParent<EnemyData>().enemyID != oldID)
            {
                SetEnemy();
                InitializeValues();
            }
            if (enemyTargetHP <= 0f && !death)
            {
                death = true;
                enemyVFX.ActivateDeath();
            }
            if (Input.GetKeyDown(KeyCode.I))
            {
                EnemyReduce(50);
            }


            EnemyHP.GetComponent<Image>().fillAmount = enemyCurrentHP / enemyMaxHP;
            enemyCurrentHP = Mathf.Lerp(enemyCurrentHP, enemyTargetHP, speedOfBarProcess * Time.deltaTime);
        }
    }
    // Update is called once per frame
    void Update () {
        if (tScript.enabled)
        {
            UpdateEnemyValues();
        }
        if(targetHP <= 0f && !death)
        {
            death = true;
            vfxMaster.ActivateDeath();
        }
		if(Input.GetKeyDown(KeyCode.B))
        {
            Reduce(50);
           
        }

        if(Input.GetKeyDown(KeyCode.Mouse1))
        {
            ReduceAllCells();

        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            IncreaseCell();

        }

        PlayerHP.GetComponent<Image>().fillAmount = currentHP / maxHP;
		currentHP = Mathf.Lerp(currentHP, targetHP, speedOfBarProcess*Time.deltaTime);

        playerCells.GetComponent<Image>().fillAmount = currentCell / maxCell;
		currentCell = Mathf.Lerp(currentCell, targetCell, speedOfBarProcess*Time.deltaTime);
    }

    public void Reduce(float damage)
    {

        targetHP  -= damage;
		if (targetHP <= 0) {
			targetHP = 0;
			print ("Player is dead");
		}
        
    }

    public void Increase(float damage)
    {
        targetHP = currentHP + damage;
        if (targetHP >= maxHP)
            targetHP = maxHP;
    }

    public void EnemyReduce(float damage)
    {
        tScript.targetPoint.GetComponentInParent<EnemyData>().health -= damage;
        enemyTargetHP -= damage;
        if (enemyTargetHP <= 0)
        {
            enemyTargetHP = 0;
            print("Enemy is dead");
        }

    }

    public void EnemyIncrease(float damage)
    {
        enemyTargetHP = enemyCurrentHP + damage;
        if (enemyTargetHP >= enemyMaxHP)
            enemyTargetHP = enemyMaxHP;
    }

    public void ReduceAllCells()
    {
        targetCell = 0;
    }

    public void IncreaseCell()
    {
        targetCell += 60;
        if (targetCell >= maxCell)
            targetCell = maxCell;
    }
}
