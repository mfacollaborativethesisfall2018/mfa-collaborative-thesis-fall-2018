﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Author: Kevin Caton-Largent
/// Used to store Enemy attribute information
/// </summary>
public class EnemyData : MonoBehaviour {

    public  enum EnemyType
    {
        fast,
        heavy
    }
    public enum Difficulty
    {
        easy,
        hard
    }

    public EnemyType    currentType;    
    public Difficulty   difficultyLevel;
    public int          enemyID;

    public float          health;
    public float          maxHealth;
    public float          maxHeavyHealth;
    public float          maxFastHealth;

    public float          lightAttackDMG;
    public float          defLAD;
    public float          heavyAttackDMG;
    public float          defHAD;

    public float        movementSpeed;
    public float        defMovementSpeed;
    public float        dodgeDistance;
    public float        defDodgeDistance;

    public bool         stunned;

    public float        delay;
    public float        defFastDelay;
    public float        defHeavyDelay;

    void Start()
    {
        stunned = false;

        if (maxFastHealth <= 0f)
            maxFastHealth = 300f;
        if (maxHeavyHealth <= 0f)
            maxHeavyHealth = 600f;

        if (defLAD <= 0f)
            defLAD = 50f;
        if (defHAD <= 0f)
            defHAD = 100f;

        if (defMovementSpeed <= 0f)
            defMovementSpeed = 10f;
        if (defDodgeDistance <= 0f)
            defDodgeDistance = 10f;

        if (defFastDelay <= 0f)
            defFastDelay = 3f;
        if (defHeavyDelay <= 0f)
            defHeavyDelay = 5f;
        string parseNumber = this.gameObject.name;
        string[] stringSplit = parseNumber.Split(' ');
        int.TryParse(stringSplit[1], out enemyID);
        InitializeStats();



    }
    /// <summary>
    ///  Initialize enemy stat values
    /// </summary>
    private void InitializeStats()
    {
        if (currentType == EnemyType.fast)
        {
            maxHealth = maxFastHealth;
            health = maxHealth;

            delay = 0;
            movementSpeed = defMovementSpeed;
            dodgeDistance = defDodgeDistance;

            lightAttackDMG = defLAD;
            heavyAttackDMG = defHAD;
        }
        else if (currentType == EnemyType.heavy)
        {
            maxHealth = maxHeavyHealth;
            health = maxHealth;

            delay = 0;
            movementSpeed = defMovementSpeed;
            dodgeDistance = defDodgeDistance;

            lightAttackDMG = defLAD;
            heavyAttackDMG = defHAD;
        }
    }
   
    
    
}
