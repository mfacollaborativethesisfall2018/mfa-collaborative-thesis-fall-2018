﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierController : MonoBehaviour {
    public Renderer barrierRenderer;
	// Use this for initialization
	void Start () {
        barrierRenderer = this.GetComponent<Renderer>();
        barrierRenderer.enabled = false;
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains("Avatar"))
        {
            Debug.Log("Barrier Enabled");
            barrierRenderer.enabled = true;
        }

    }
    public void OnTriggerExit(Collider other)
    {
        if (other.name.Contains("Avatar"))
        {
            Debug.Log("Barrier Disabled");
            barrierRenderer.enabled = false;
        }
    }
}
