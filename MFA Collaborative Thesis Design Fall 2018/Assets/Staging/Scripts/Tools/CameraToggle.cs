﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Author: Kevin Caton-Largent
/// This script is a tool meant to be able to cycle between active cameras
/// </summary>
public class CameraToggle : MonoBehaviour {

    // a list of cameras in the scene to switch between
    public List<Camera> cameras;
    public int currentCam;
    public int nextCam;
    public int toggleCount;
    public int maxToggleCount;
    public string toggleKey;
    /// <summary>
    /// Finds the active cameras in the scenes and updates the index of current camera
    /// </summary>
    private void FindEnabledCameras()
    {
        int index = 0;
        foreach (Camera currentCamera in cameras)
        {
            if (currentCamera.gameObject.activeSelf)
            {
                currentCam = index;
                Debug.Log("Current Cam Index: " + currentCam);
                break;
            }
            index++;
        }
    }
    private void ToggleCamera()
    {
        toggleCount++;

        if (toggleCount == 5)
        {
            FindEnabledCameras();
            toggleCount = 0;
        }
        cameras[currentCam].gameObject.SetActive(false);
        if (currentCam + 1 > cameras.Count - 1)
        {
            nextCam = 0;
            currentCam = 0;
        }
        else
        {
            nextCam = currentCam + 1;
            currentCam++;
        }
        cameras[nextCam].gameObject.SetActive(true);

        
    }
    void Start()
    {
        toggleCount = 0;
        if (maxToggleCount <= 0)
            maxToggleCount = 5;
        if (toggleKey == "")
            toggleKey = "t";
        FindEnabledCameras();
    }
    void Update()
    {
       if (Input.GetKeyUp(toggleKey))
        {
            ToggleCamera();
        }
    }
}
